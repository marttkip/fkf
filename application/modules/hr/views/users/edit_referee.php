<?php
//referee data
$row = $referee->row();

$referee_onames = $row->referee_onames;
$referee_fname = $row->referee_fname;
$referee_dob = $row->referee_dob;
$referee_email = $row->referee_email;
$referee_phone = $row->referee_phone;
$referee_address = $row->referee_address;
$civil_status_id = $row->civilstatus_id;
$referee_locality = $row->referee_locality;
$title_id = $row->title_id;
$gender_id = $row->gender_id;
$referee_city = $row->referee_city;
$referee_number = $row->referee_number;
$referee_post_code = $row->referee_post_code;
$referee_national_id_number = $row->referee_national_id_number;
$bank_account_number = $row->bank_account_number;
$bank_branch_code = $row->bank_branch_code;
$educational_background = $row->educational_background;
$year_of_graduation_from = $row->year_of_graduation_from;
$accolades = $row->accolades;
$computer_literacy = $row->computer_literacy;
$occupation = $row->occupation;
$year_of_graduation_to = $row->year_of_graduation_to;
$year_qualified = $row->year_qualified;
$training_town = $row->training_town;
$training_county = $row->training_county;
$training_referee = $row->training_referee;
$futuro_instructor = $row->futuro_instructor;
$caf_instructor = $row->caf_instructor;
$fifa_instructor = $row->fifa_instructor;
$grades_attained = $row->grades_attained;
$referee_type = $row->referee_type;
$current_status = $row->current_status;
//repopulate data if validation errors occur
$validation_error = validation_errors();
				
if(!empty($validation_error))
{
	$referee_onames = set_value('referee_onames');
	$referee_type = set_value('referee_type');
	$referee_fname = set_value('referee_fname');
	$referee_dob = set_value('referee_dob');
	$referee_email = set_value('referee_email');
	$referee_phone = set_value('referee_phone');
	$referee_address = set_value('referee_address');
	$civil_status_id = set_value('civil_status_id');
	$referee_locality = set_value('referee_locality');
	$title_id = set_value('title_id');
	$gender_id = set_value('gender_id');
	$referee_city = set_value('referee_city');
	$referee_number = set_value('referee_number');
	$referee_post_code = set_value('referee_post_code');
	$team_id = set_value('team_id');
	$referee_nssf_number = set_value('referee_nssf_number');
	$referee_kra_pin = set_value('referee_kra_pin');
	$referee_national_id_number = set_value('referee_national_id_number');
	$referee_nhif_number = set_value('referee_nhif_number');
	$referee_type_id2 = set_value('referee_type_id');
	$bank_team_id2 = set_value('bank_team_id');
	$bank_account_number = set_value('bank_account_number');
	$bank_branch_code = set_value('bank_branch_code');
	$educational_background = set_value('educational_background');
	$year_of_graduation_from = set_value('year_of_graduation_from');
	$accolades = set_value('accolades');
	$computer_literacy = set_value('computer_literacy');
	$occupation = set_value('occupation');
	$year_of_graduation_to = set_value('year_of_graduation_to');
	$year_qualified = set_value('year_qualified');
	$training_town = set_value('training_town');
	$training_county = set_value('training_county');
	$training_referee = set_value('training_referee');
	$futuro_instructor = set_value('futuro_instructor');
	$caf_instructor = set_value('caf_instructor');
	$fifa_instructor = set_value('fifa_instructor');
	$grades_attained = set_value('grades_attained');
	$referee_type = set_value('referee_type');
	$current_status = set_value('current_status');
}

$primary = $secondary = $college = $university = '';

if($educational_background == 1)
{
	$primary = 'checked';
}

else if($educational_background == 2)
{
	$secondary = 'checked';
}

else if($educational_background == 3)
{
	$college = 'checked';
}

else if($educational_background == 4)
{
	$university = 'checked';
}

$certificate = $diploma = $degree = '';

if($accolades == 1)
{
	$certificate = 'checked';
}

else if($accolades == 2)
{
	$diploma = 'checked';
}

else if($accolades == 3)
{
	$degree = 'checked';
}

$nil = $semi = $literate = '';

if($computer_literacy == 1)
{
	$nil = 'checked';
}

else if($computer_literacy == 2)
{
	$semi = 'checked';
}

else if($computer_literacy == 3)
{
	$literate = 'checked';
}

$self = $employed = '';

if($occupation == 1)
{
	$employed = 'checked';
}

else if($occupation == 2)
{
	$self = 'checked';
}

$twenty = $forty = $sixty = $eighty = $hundred = '';

if($grades_attained == 1)
{
	$twenty = 'checked';
}

else if($grades_attained == 2)
{
	$forty = 'checked';
}

else if($grades_attained == 3)
{
	$sixty = 'checked';
}

else if($grades_attained == 4)
{
	$eighty = 'checked';
}

else if($grades_attained == 5)
{
	$hundred = 'checked';
}

$active_referee = $match_commissioner = '';

if($referee_type == 1)
{
	$active_referee = 'checked';
}

else if($referee_type == 2)
{
	$match_commissioner = 'checked';
}

$international1 = $international2 = $national1 = $national2 = '';

if($current_status == 1)
{
	$international1 = 'checked';
}

else if($current_status == 2)
{
	$international2 = 'checked';
}

else if($current_status == 3)
{
	$national1 = 'checked';
}

else if($current_status == 4)
{
	$national2 = 'checked';
}

?>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">About <?php echo $referee_onames.' '.$referee_fname;?></h2>
    </header>
    <div class="panel-body">
    <!-- Adding Errors -->
    <?php
    if(isset($error)){
        echo '<div class="alert alert-danger"> Oh snap! Change a few things up and try submitting again. </div>';
    }
    if(!empty($validation_errors))
    {
        echo '<div class="alert alert-danger"> Oh snap! '.$validation_errors.' </div>';
    }

    ?>
            
<?php echo form_open_multipart(''.site_url().'soccer-management/edit-referee/'.$referee_id.'', array("class" => "form-horizontal", "role" => "form"));?>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label class="col-lg-5 control-label">Title: </label>
            
            <div class="col-lg-7">
            	<select class="form-control" name="title_id">
                	<?php
                    	if($titles->num_rows() > 0)
						{
							$title = $titles->result();
							
							foreach($title as $res)
							{
								$db_title_id = $res->title_id;
								$title_name = $res->title_name;
								
								if($db_title_id == $title_id)
								{
									echo '<option value="'.$db_title_id.'" selected>'.$title_name.'</option>';
								}
								
								else
								{
									echo '<option value="'.$db_title_id.'">'.$title_name.'</option>';
								}
							}
						}
					?>
                </select>
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-lg-5 control-label">Other Names: </label>
            
            <div class="col-lg-7">
            	<input type="text" class="form-control" name="referee_onames" placeholder="Other Names" value="<?php echo $referee_onames;?>">
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-lg-5 control-label">First Name: </label>
            
            <div class="col-lg-7">
            	<input type="text" class="form-control" name="referee_fname" placeholder="First Name" value="<?php echo $referee_fname;?>">
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg-5 control-label">Referee number: </label>
            
            <div class="col-lg-7">
            	<input type="text" class="form-control" name="referee_number" placeholder="Referee number" value="<?php echo $referee_number;?>">
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-lg-5 control-label">Date of Birth: </label>
            
            <div class="col-lg-7">
            	<div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </span>
                    <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="referee_dob" placeholder="Date of Birth" value="<?php echo $referee_dob;?>">
                </div>
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-lg-5 control-label">ID number: </label>
            
            <div class="col-lg-7">
            	<input type="text" class="form-control" name="referee_national_id_number" placeholder="ID number" value="<?php echo $referee_national_id_number;?>">
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-lg-5 control-label">Gender: </label>
            
            <div class="col-lg-7">
            	<select class="form-control" name="gender_id">
                	<?php
                    	if($genders->num_rows() > 0)
						{
							$gender = $genders->result();
							
							foreach($gender as $res)
							{
								$db_gender_id = $res->gender_id;
								$gender_name = $res->gender_name;
								
								if($db_gender_id == $gender_id)
								{
									echo '<option value="'.$db_gender_id.'" selected>'.$gender_name.'</option>';
								}
								
								else
								{
									echo '<option value="'.$db_gender_id.'">'.$gender_name.'</option>';
								}
							}
						}
					?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg-5 control-label">Bank account number: </label>
            
            <div class="col-lg-7">
            	<input type="text" class="form-control" name="bank_account_number" placeholder="Bank account number" value="<?php echo $bank_account_number;?>">
            </div>
        </div>
	</div>
    
    <div class="col-md-6">
    	<div class="form-group">
            <label class="col-lg-5 control-label">Bank branch code: </label>
            
            <div class="col-lg-7">
            	<input type="text" class="form-control" name="bank_branch_code" placeholder="Bank branch code" value="<?php echo $bank_branch_code;?>">
            </div>
        </div>
    	 <div class="form-group">
		
            <label class="col-lg-5 control-label">Civil Status: </label>
            
            <div class="col-lg-7">
            	<select class="form-control" name="civil_status_id">
                	<?php
                    	if($civil_statuses->num_rows() > 0)
						{
							$status = $civil_statuses->result();
							
							foreach($status as $res)
							{
								$status_id = $res->civil_status_id;
								$status_name = $res->civil_status_name;
								
								if($status_id == $civil_status_id)
								{
									echo '<option value="'.$status_id.'" selected>'.$status_name.'</option>';
								}
								
								else
								{
									echo '<option value="'.$status_id.'">'.$status_name.'</option>';
								}
							}
						}
					?>
                </select>
            </div>
        </div>
		 <div class="form-group">
            <label class="col-lg-5 control-label">Email Address: </label>
            
            <div class="col-lg-7">
            	<input type="text" class="form-control" name="referee_email" placeholder="Email Address" value="<?php echo $referee_email;?>">
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg-5 control-label">Phone: </label>
            
            <div class="col-lg-7">
            	<input type="text" class="form-control" name="referee_phone" placeholder="Phone" value="<?php echo $referee_phone;?>">
            </div>
        </div>
        
         <div class="form-group">
            <label class="col-lg-5 control-label">Town: </label>
            
            <div class="col-lg-7">
            	<input type="text" class="form-control" name="referee_locality" placeholder="Residence" value="<?php echo $referee_locality;?>">
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-lg-5 control-label">County: </label>
            
            <div class="col-lg-7">
            	<input type="text" class="form-control" name="referee_address" placeholder="Address" value="<?php echo $referee_address;?>">
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-lg-5 control-label">FKF Branch: </label>
            
            <div class="col-lg-7">
            	<input type="text" class="form-control" name="referee_city" placeholder="City" value="<?php echo $referee_locality;?>">
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-lg-5 control-label">FKF Sub Branch: </label>
            
            <div class="col-lg-7">
            	<input type="text" class="form-control" name="referee_post_code" placeholder="Post code" value="<?php echo $referee_post_code;?>">
            </div>
        </div>
	</div>
</div>

<div class="row" style="margin-top:10px;">
	<div class="col-md-12">
		<h3>Educational Background</h3>
	</div>
</div>

<div class="row" style="margin-top:10px;">
	<div class="col-md-12">
        
		<div class="form-group">
            <div class="col-lg-3">
				<div class="radio">
					<label>
						<input type="radio" name="educational_background" id="optionsRadios1" value="1" <?php echo $primary;?>>
						Primary
					</label>
				</div>
			</div>
            <div class="col-lg-3">
				<div class="radio">
					<label>
						<input type="radio" name="educational_background" id="optionsRadios2" value="2" <?php echo $secondary;?>>
						Secondary
					</label>
				</div>
			</div>
            <div class="col-lg-3">
				<div class="radio">
					<label>
						<input type="radio" name="educational_background" id="optionsRadios3" value="3" <?php echo $college;?>>
						College/ Polytechnic
					</label>
				</div>
			</div>
            <div class="col-lg-3">
				<div class="radio">
					<label>
						<input type="radio" name="educational_background" id="optionsRadios4" value="4" <?php echo $university;?>>
						University
					</label>
				</div>
			</div>
        </div>
        
        <div class="form-group">
            <label class="col-lg-4 control-label">Year of Graduation: </label>
            
            <div class="col-lg-4">
            	<input type="text" class="form-control" name="year_of_graduation_from" placeholder="From" value="<?php echo $year_of_graduation_from;?>">
            </div>
            
            <div class="col-lg-4">
            	<input type="text" class="form-control" name="year_of_graduation_to" placeholder="To" value="<?php echo $year_of_graduation_to;?>">
            </div>
        </div>
        
		<div class="form-group">
            <label class="col-lg-3 control-label">Accolades: </label>
            
            <div class="col-lg-3">
				<div class="radio">
					<label>
						<input type="radio" name="accolades" id="optionsRadios1" value="1" <?php echo $certificate;?>>
						Certificate
					</label>
				</div>
			</div>
            <div class="col-lg-3">
				<div class="radio">
					<label>
						<input type="radio" name="accolades" id="optionsRadios2" value="2" <?php echo $diploma;?>>
						Diploma
					</label>
				</div>
			</div>
            <div class="col-lg-3">
				<div class="radio">
					<label>
						<input type="radio" name="accolades" id="optionsRadios3" value="3" <?php echo $degree;?>>
						Degree
					</label>
				</div>
			</div>
        </div>
        
		<div class="form-group">
            <label class="col-lg-3 control-label">Computer Literacy: </label>
            
            <div class="col-lg-3">
				<div class="radio">
					<label>
						<input type="radio" name="computer_literacy" id="optionsRadios1" value="1" <?php echo $nil;?>>
						Nil
					</label>
				</div>
			</div>
            <div class="col-lg-3">
				<div class="radio">
					<label>
						<input type="radio" name="computer_literacy" id="optionsRadios2" value="2" <?php echo $semi;?>>
						Semi Literate
					</label>
				</div>
			</div>
            <div class="col-lg-3">
				<div class="radio">
					<label>
						<input type="radio" name="computer_literacy" id="optionsRadios3" value="3" <?php echo $literate;?>>
						Literate
					</label>
				</div>
			</div>
        </div>
        
		<div class="form-group">
            <div class="col-lg-3">
				<div class="radio">
					<label>
						<input type="radio" name="occupation" id="optionsRadios1" value="1" <?php echo $employed;?>>
						Occupation
					</label>
				</div>
			</div>
            <div class="col-lg-3">
				<div class="radio">
					<label>
						<input type="radio" name="occupation" id="optionsRadios2" value="2" <?php echo $self;?>>
						Self Employed
					</label>
				</div>
			</div>
        </div>
		
    </div>
</div>

<div class="row" style="margin-top:10px;">
	<div class="col-md-12">
		<h3>Refereeing Career</h3>
	</div>
</div>
        
<div class="row" style="margin-top:10px;">
	<div class="col-md-12">
        <div class="form-group">
            <label class="col-lg-4 control-label">Year Qualified: </label>
            
            <div class="col-lg-8">
            	<input type="text" class="form-control" name="year_qualified" placeholder="Year Qualified" value="<?php echo $year_qualified;?>">
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-lg-4 control-label">Trained Where: </label>
            
            <div class="col-lg-4">
            	<input type="text" class="form-control" name="training_town" placeholder="Town" value="<?php echo $training_town;?>">
            </div>
            
            <div class="col-lg-4">
            	<input type="text" class="form-control" name="training_county" placeholder="County" value="<?php echo $training_county;?>">
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-lg-4 control-label">Trained By: </label>
            
            <div class="col-lg-4">
            	<input type="text" class="form-control" name="training_referee" placeholder="Referee" value="<?php echo $training_referee;?>">
            </div>
            
            <div class="col-lg-4">
            	<input type="text" class="form-control" name="futuro_instructor" placeholder="Futuro Instructor" value="<?php echo $futuro_instructor;?>">
            </div>
            
            <div class="col-lg-4 col-lg-offset-4">
            	<input type="text" class="form-control" name="caf_instructor" placeholder="CAF Instructor" value="<?php echo $caf_instructor;?>">
            </div>
            
            <div class="col-lg-4">
            	<input type="text" class="form-control" name="fifa_instructor" placeholder="FIFA Instructor" value="<?php echo $fifa_instructor;?>">
            </div>
        </div>
		
		<div class="form-group">
            <label class="col-lg-4 control-label">Grades Attained: </label>
            <div class="col-lg-8">
				<div class="col-lg-3">
					<div class="radio">
						<label>
							<input type="radio" name="grades_attained" id="optionsRadios1" value="1" <?php echo $twenty;?>>
							0-20
						</label>
					</div>
				</div>
				<div class="col-lg-3">
					<div class="radio">
						<label>
							<input type="radio" name="grades_attained" id="optionsRadios2" value="2" <?php echo $forty;?>>
							21-40
						</label>
					</div>
				</div>
				<div class="col-lg-3">
					<div class="radio">
						<label>
							<input type="radio" name="grades_attained" id="optionsRadios3" value="3" <?php echo $sixty;?>>
							41-60
						</label>
					</div>
				</div>
				<div class="col-lg-3">
					<div class="radio">
						<label>
							<input type="radio" name="grades_attained" id="optionsRadios4" value="4" <?php echo $eighty;?>>
							61-80
						</label>
					</div>
				</div>
				<div class="col-lg-3">
					<div class="radio">
						<label>
							<input type="radio" name="grades_attained" id="optionsRadios5" value="5" <?php echo $hundred;?>>
							81-100
						</label>
					</div>
				</div>
			</div>
        </div>
        
		<div class="form-group">
            <label class="col-lg-4 control-label">Between the two where do you belong? </label>
            
            <div class="col-lg-4">
				<div class="radio">
					<label>
						<input type="radio" name="referee_type" id="optionsRadios1" value="1" <?php echo $active_referee;?>>
						Active Referee
					</label>
				</div>
			</div>
            <div class="col-lg-4">
				<div class="radio">
					<label>
						<input type="radio" name="referee_type" id="optionsRadios2" value="2" <?php echo $match_commissioner;?>>
						Match Commissioner
					</label>
				</div>
			</div>
        </div>
        
		<div class="form-group">
            <label class="col-lg-4 control-label">Current Status </label>
            
            <div class="col-lg-4">
				<div class="radio">
					<label>
						<input type="radio" name="current_status" id="optionsRadios1" value="1" <?php echo $international1;?>>
						International - Elite A FIFA
					</label>
				</div>
			</div>
            <div class="col-lg-4">
				<div class="radio">
					<label>
						<input type="radio" name="current_status" id="optionsRadios2" value="2" <?php echo $international2;?>>
						International - National division 1 & 2
					</label>
				</div>
			</div>
            <div class="col-lg-4 col-lg-offset-4">
				<div class="radio">
					<label>
						<input type="radio" name="current_status" id="optionsRadios2" value="3" <?php echo $national1;?>>
						National - Elite B KPL & NSL
					</label>
				</div>
			</div>
            <div class="col-lg-4">
				<div class="radio">
					<label>
						<input type="radio" name="current_status" id="optionsRadios2" value="4" <?php echo $national2;?>>
						National - Branch Level
					</label>
				</div>
			</div>
        </div>
        
	</div>
</div>

<div class="row" style="margin-top:10px;">
	<div class="col-md-12">
        <div class="form-actions center-align">
            <button class="submit btn btn-primary" type="submit">
                Edit referee
            </button>
        </div>
    </div>
</div>
            <?php echo form_close();?>
                </div>
            </section>