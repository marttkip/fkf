<?php
//personnel data
$personnel_onames = set_value('personnel_onames');
$personnel_fname = set_value('personnel_fname');
$bank_account_number = set_value('bank_account_number');
$personnel_dob = set_value('personnel_dob');
$personnel_email = set_value('personnel_email');
$personnel_phone = set_value('personnel_phone');
$personnel_address = set_value('personnel_address');
$civil_status_id = set_value('civil_status_id');
$personnel_locality = set_value('personnel_locality');
$title_id = set_value('title_id');
$gender_id = set_value('gender_id');
$personnel_city = set_value('personnel_city');
$personnel_number = set_value('personnel_number');
$personnel_post_code = set_value('personnel_post_code');
$branch_id = set_value('branch_id');
$personnel_type_id2 = set_value('personnel_type_id');
$bank_branch_id = set_value('bank_branch_id');
$personnel_national_id_number = set_value('personnel_national_id_number');
$personnel_kra_pin = set_value('personnel_kra_pin');
$cost_center = set_value('cost_center');
$engagement_date = set_value('engagement_date');



$bank_account_number = set_value('bank_account_number');
$bank_branch_code = set_value('bank_branch_code');
$educational_background = set_value('educational_background');
$year_of_graduation_from = set_value('year_of_graduation_from');
$accolades = set_value('accolades');
$computer_literacy = set_value('computer_literacy');
$occupation = set_value('occupation');
$year_of_graduation_to = set_value('year_of_graduation_to');
$year_qualified = set_value('year_qualified');
$training_town = set_value('training_town');
$training_county = set_value('training_county');
$training_referee = set_value('training_referee');
$futuro_instructor = set_value('futuro_instructor');
$caf_instructor = set_value('caf_instructor');
$fifa_instructor = set_value('fifa_instructor');
$grades_attained = set_value('grades_attained');
$referee_type = set_value('referee_type');
$current_status = set_value('current_status');

$primary = $secondary = $college = $university = '';

if($educational_background == 1)
{
    $primary = 'checked';
}

else if($educational_background == 2)
{
    $secondary = 'checked';
}

else if($educational_background == 3)
{
    $college = 'checked';
}

else if($educational_background == 4)
{
    $university = 'checked';
}

$certificate = $diploma = $degree = '';

if($accolades == 1)
{
    $certificate = 'checked';
}

else if($accolades == 2)
{
    $diploma = 'checked';
}

else if($accolades == 3)
{
    $degree = 'checked';
}

$nil = $semi = $literate = '';

if($computer_literacy == 1)
{
    $nil = 'checked';
}

else if($computer_literacy == 2)
{
    $semi = 'checked';
}

else if($computer_literacy == 3)
{
    $literate = 'checked';
}

$self = $employed = '';

if($occupation == 1)
{
    $employed = 'checked';
}

else if($occupation == 2)
{
    $self = 'checked';
}

$twenty = $forty = $sixty = $eighty = $hundred = '';

if($grades_attained == 1)
{
    $twenty = 'checked';
}

else if($grades_attained == 2)
{
    $forty = 'checked';
}

else if($grades_attained == 3)
{
    $sixty = 'checked';
}

else if($grades_attained == 4)
{
    $eighty = 'checked';
}

else if($grades_attained == 5)
{
    $hundred = 'checked';
}

$active_referee = $match_commissioner = '';

if($referee_type == 1)
{
    $active_referee = 'checked';
}

else if($referee_type == 2)
{
    $match_commissioner = 'checked';
}

$international1 = $international2 = $national1 = $national2 = '';

if($current_status == 1)
{
    $international1 = 'checked';
}

else if($current_status == 2)
{
    $international2 = 'checked';
}

else if($current_status == 3)
{
    $national1 = 'checked';
}

else if($current_status == 4)
{
    $national2 = 'checked';
}
?>          
          <section class="panel">
                <header class="panel-heading">
                    <h2 class="panel-title"><?php echo $title;?></h2>
                    <div class="row" style="margin-top:-25px;">
                        <div class="col-lg-12">
                            <?php
                                 if($module == 2)
                                 {
                                    ?>
                                         <a href="<?php echo site_url();?>human-resource/users" class="btn btn-sm btn-info pull-right">Back to users</a>
                                    <?php
                                 }
                                 else
                                 {
                                    ?>

                                         <a href="<?php echo site_url();?>human-resource/personnel" class="btn btn-sm btn-info pull-right">Back to personnel</a>
                                    <?php

                                 }
                            ?>
                        </div>
                    </div>
                </header>
                <div class="panel-body">
                    <div class="padd">
                	
                        
                    <!-- Adding Errors -->
                    <?php
						$success = $this->session->userdata('success_message');
						$error = $this->session->userdata('error_message');
						
						if(!empty($success))
						{
							echo '
								<div class="alert alert-success">'.$success.'</div>
							';
							
							$this->session->unset_userdata('success_message');
						}
						
						if(!empty($error))
						{
							echo '
								<div class="alert alert-danger">'.$error.'</div>
							';
							
							$this->session->unset_userdata('error_message');
						}
						$validation_errors = validation_errors();
						
						if(!empty($validation_errors))
						{
							echo '<div class="alert alert-danger"> Oh snap! '.$validation_errors.' </div>';
						}
                    ?>
                    
                    <?php echo form_open($this->uri->uri_string(), array("class" => "form-horizontal", "role" => "form"));?>
                    <div class="row">
                    	<div class="col-md-6">
                            <div class="form-group">
                                <label class="col-lg-5 control-label">Branch: </label>
                                
                                <div class="col-lg-7">
                                    <select class="form-control" name="branch_id">
                                    	<?php
                                        	if($branches->num_rows() > 0)
                    						{
                    							foreach($branches->result() as $res)
                    							{
                    								$branch_id2 = $res->branch_id;
                    								$branch_name = $res->branch_name;
                    								
                    								if($branch_id2 == $branch_id)
                    								{
                    									echo '<option value="'.$branch_id2.'" selected>'.$branch_name.'</option>';
                    								}
                    								
                    								else
                    								{
                    									echo '<option value="'.$branch_id2.'">'.$branch_name.'</option>';
                    								}
                    							}
                    						}
                    					?>
                                    </select>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-lg-5 control-label">Type: </label>
                                
                                <div class="col-lg-7">
                                    <select class="form-control" name="personnel_type_id">
                                    	<?php
                                        	if($personnel_types->num_rows() > 0)
                    						{
                    							$status = $personnel_types->result();
                    							
                    							foreach($status as $res)
                    							{
                    								$personnel_type_id = $res->personnel_type_id;
                    								$personnel_type_name = $res->personnel_type_name;
                    								if($personnel_type_id > 2 AND $module == 2)
                                                    {
                                                        if($personnel_type_id == $personnel_type_id2)
                                                        {
                                                            echo '<option value="'.$personnel_type_id.'" selected>'.$personnel_type_name.'</option>';
                                                        }
                                                        
                                                        else
                                                        {
                                                            echo '<option value="'.$personnel_type_id.'">'.$personnel_type_name.'</option>';
                                                        }
                                                    }

                                                    if($personnel_type_id < 3 AND $module != 2)
                                                    {
                                                        if($personnel_type_id == $personnel_type_id2)
                                                        {
                                                            echo '<option value="'.$personnel_type_id.'" selected>'.$personnel_type_name.'</option>';
                                                        }
                                                        
                                                        else
                                                        {
                                                            echo '<option value="'.$personnel_type_id.'">'.$personnel_type_name.'</option>';
                                                        }
                                                    }

                    							
                    							}
                    						}
                    					?>
                                    </select>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-lg-5 control-label">Title: </label>
                                
                                <div class="col-lg-7">
                                    <select class="form-control" name="title_id">
                                    	<?php
                                        	if($titles->num_rows() > 0)
                    						{
                    							$title = $titles->result();
                    							
                    							foreach($title as $res)
                    							{
                    								$db_title_id = $res->title_id;
                    								$title_name = $res->title_name;
                    								
                    								if($db_title_id == $title_id)
                    								{
                    									echo '<option value="'.$db_title_id.'" selected>'.$title_name.'</option>';
                    								}
                    								
                    								else
                    								{
                    									echo '<option value="'.$db_title_id.'">'.$title_name.'</option>';
                    								}
                    							}
                    						}
                    					?>
                                    </select>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-lg-5 control-label">Other Names: </label>
                                
                                <div class="col-lg-7">
                                	<input type="text" class="form-control" name="personnel_onames" placeholder="Other Names" value="<?php echo $personnel_onames;?>">
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-lg-5 control-label">First Name: </label>
                                
                                <div class="col-lg-7">
                                	<input type="text" class="form-control" name="personnel_fname" placeholder="First Name" value="<?php echo $personnel_fname;?>">
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-lg-5 control-label">Personnel number: </label>
                                
                                <div class="col-lg-7">
                                	<input type="text" class="form-control" name="personnel_number" placeholder="Personnel number" value="<?php echo $personnel_number;?>">
                                </div>
                            </div>
                             <div class="form-group">
                                <label class="col-lg-5 control-label">ID number: </label>
                                
                                <div class="col-lg-7">
                                	<input type="text" class="form-control" name="personnel_national_id_number" placeholder="ID number" value="<?php echo $personnel_national_id_number;?>">
                                </div>
                            </div>
                         
                            
                            <div class="form-group">
                                <label class="col-lg-5 control-label">Date of Birth: </label>
                                
                                <div class="col-lg-7">
                                	<div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </span>
                                        <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="personnel_dob" placeholder="Date of Birth" value="<?php echo $personnel_dob;?>">
                                    </div>
                                </div>
                            </div>
                               <?php
                            if($module != 2)
                            {
                                ?>
                                <div class="form-group">
                                    <label class="col-lg-5 control-label">KRA pin: </label>
                                    
                                    <div class="col-lg-7">
                                        <input type="text" class="form-control" name="personnel_kra_pin" placeholder="KRA pin" value="<?php echo $personnel_kra_pin;?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-5 control-label"> Bank Account number: </label>
                                    
                                    <div class="col-lg-7">
                                        <input type="text" class="form-control" name="bank_account_number" placeholder="Account number" value="<?php echo $bank_account_number;?>">
                                    </div>
                                </div>
                                <?php
                            }
                            ?>
                    		
                            
                    	</div>
                        
                        <div class="col-md-6">
                            
                            <div class="form-group">
                                <label class="col-lg-5 control-label">Gender: </label>
                                
                                <div class="col-lg-7">
                                    <select class="form-control" name="gender_id">
                                    	<?php
                                        	if($genders->num_rows() > 0)
                    						{
                    							$gender = $genders->result();
                    							
                    							foreach($gender as $res)
                    							{
                    								$db_gender_id = $res->gender_id;
                    								$gender_name = $res->gender_name;
                    								
                    								if($db_gender_id == $gender_id)
                    								{
                    									echo '<option value="'.$db_gender_id.'" selected>'.$gender_name.'</option>';
                    								}
                    								
                    								else
                    								{
                    									echo '<option value="'.$db_gender_id.'">'.$gender_name.'</option>';
                    								}
                    							}
                    						}
                    					?>
                                    </select>
                                </div>
                            </div>
                              <?php
                            if($module != 2)
                            {
                                ?>
                                <div class="form-group">
                                    <label class="col-lg-5 control-label">Civil Status: </label>
                                    
                                    <div class="col-lg-7">
                                        <select class="form-control" name="civil_status_id">
                                        	<?php
                                            	if($civil_statuses->num_rows() > 0)
                        						{
                        							$status = $civil_statuses->result();
                        							
                        							foreach($status as $res)
                        							{
                        								$status_id = $res->civil_status_id;
                        								$status_name = $res->civil_status_name;
                        								
                        								if($status_id == $civil_status_id)
                        								{
                        									echo '<option value="'.$status_id.'" selected>'.$status_name.'</option>';
                        								}
                        								
                        								else
                        								{
                        									echo '<option value="'.$status_id.'">'.$status_name.'</option>';
                        								}
                        							}
                        						}
                        					?>
                                        </select>
                                    </div>
                                </div>
                            <?php
                             }
                            ?>
                            
                            <div class="form-group">
                                <label class="col-lg-5 control-label">Email Address: </label>
                                
                                <div class="col-lg-7">
                                	<input type="text" class="form-control" name="personnel_email" placeholder="Email Address" value="<?php echo $personnel_email;?>">
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-lg-5 control-label">Phone: </label>
                                
                                <div class="col-lg-7">
                                	<input type="text" class="form-control" name="personnel_phone" placeholder="Phone" value="<?php echo $personnel_phone;?>">
                                </div>
                            </div>

                            <?php
                            if($module == 2)
                            {
                                ?>
                                 <div class="form-group">
                                    <label class="col-lg-5 control-label">Town: </label>
                                    
                                    <div class="col-lg-7">
                                        <input type="text" class="form-control" name="personnel_locality" placeholder="Town" value="<?php echo $personnel_locality;?>">
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-lg-5 control-label">County: </label>
                                    
                                    <div class="col-lg-7">
                                        <input type="text" class="form-control" name="personnel_address" placeholder="County" value="<?php echo $personnel_address;?>">
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-lg-5 control-label">FKF Branch: </label>
                                    
                                    <div class="col-lg-7">
                                        <input type="text" class="form-control" name="personnel_city" placeholder="FKF Branch" value="<?php echo $personnel_locality;?>">
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-lg-5 control-label">FKF Sub Branch: </label>
                                    
                                    <div class="col-lg-7">
                                        <input type="text" class="form-control" name="personnel_post_code" placeholder="FKF Sub branch" value="<?php echo $personnel_post_code;?>">
                                    </div>
                                </div>
                                <?php
                            }
                            else
                            {

                                ?>
                                   <div class="form-group">
                                        <label class="col-lg-5 control-label">Residence: </label>
                                        
                                        <div class="col-lg-7">
                                            <input type="text" class="form-control" name="personnel_locality" placeholder="Residence" value="<?php echo $personnel_locality;?>">
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-lg-5 control-label">Address: </label>
                                        
                                        <div class="col-lg-7">
                                            <input type="text" class="form-control" name="personnel_address" placeholder="Address" value="<?php echo $personnel_address;?>">
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-lg-5 control-label">City: </label>
                                        
                                        <div class="col-lg-7">
                                            <input type="text" class="form-control" name="personnel_city" placeholder="City" value="<?php echo $personnel_locality;?>">
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-lg-5 control-label">Post code: </label>
                                        
                                        <div class="col-lg-7">
                                            <input type="text" class="form-control" name="personnel_post_code" placeholder="Post code" value="<?php echo $personnel_post_code;?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-5 control-label">Bank Branch Code: </label>
                                        
                                        <div class="col-lg-7">
                                            <input type="text" class="form-control" name="bank_branch_id" placeholder="Bank Branch Code" value="<?php echo $bank_branch_id;?>">
                                        </div>
                                    </div>
                                <?php

                            }

                            ?>
                            
                         
                    		<div class="form-group">
                                <label class="col-lg-5 control-label">Engagement Date: </label>
                                
                                 <div class="col-lg-7">
                                	<div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </span>
                                        <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="engagement_date" placeholder="Engagement Date" value="<?php echo $engagement_date;?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php
                if($module == 2)
                {
                    ?>
                        

                        <div class="row" style="margin-top:10px;">
                            <div class="col-md-6">
                                <div class="row" style="margin-top:10px;">
                                    <div class="col-md-12">
                                        <h3>Educational Background</h3>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="educational_background" id="optionsRadios1" value="1" <?php echo $primary;?>>
                                                Primary
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="educational_background" id="optionsRadios2" value="2" <?php echo $secondary;?>>
                                                Secondary
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="educational_background" id="optionsRadios3" value="3" <?php echo $college;?>>
                                                College/ Polytechnic
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="educational_background" id="optionsRadios4" value="4" <?php echo $university;?>>
                                                University
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Year of Graduation: </label>
                                    
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" name="year_of_graduation_from" placeholder="From" value="<?php echo $year_of_graduation_from;?>">
                                    </div>
                                    
                                    <div class="col-sm-4">
                                        <input type="text" class="form-control" name="year_of_graduation_to" placeholder="To" value="<?php echo $year_of_graduation_to;?>">
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Accolades: </label>
                                    
                                    <div class="col-lg-3">
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="accolades" id="optionsRadios1" value="1" <?php echo $certificate;?>>
                                                Certificate
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="accolades" id="optionsRadios2" value="2" <?php echo $diploma;?>>
                                                Diploma
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="accolades" id="optionsRadios3" value="3" <?php echo $degree;?>>
                                                Degree
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-lg-3 control-label">Computer Literacy: </label>
                                    
                                    <div class="col-lg-3">
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="computer_literacy" id="optionsRadios1" value="1" <?php echo $nil;?>>
                                                Nil
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="computer_literacy" id="optionsRadios2" value="2" <?php echo $semi;?>>
                                                Semi Literate
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="computer_literacy" id="optionsRadios3" value="3" <?php echo $literate;?>>
                                                Literate
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <div class="col-lg-3">
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="occupation" id="optionsRadios1" value="1" <?php echo $employed;?>>
                                                Occupation
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="occupation" id="optionsRadios2" value="2" <?php echo $self;?>>
                                                Self Employed
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                            <div class="col-md-6">
                                <div class="row" style="margin-top:10px;">
                                    <div class="col-md-12">
                                        <h3>Refereeing Career</h3>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-4 control-label">Year Qualified: </label>
                                    
                                    <div class="col-lg-8">
                                        <input type="text" class="form-control" name="year_qualified" placeholder="Year Qualified" value="<?php echo $year_qualified;?>">
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-lg-4 control-label">Trained Where: </label>
                                    
                                    <div class="col-lg-4">
                                        <input type="text" class="form-control" name="training_town" placeholder="Town" value="<?php echo $training_town;?>">
                                    </div>
                                    
                                    <div class="col-lg-4">
                                        <input type="text" class="form-control" name="training_county" placeholder="County" value="<?php echo $training_county;?>">
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-lg-4 control-label">Trained By: </label>
                                    
                                    <div class="col-lg-4">
                                        <input type="text" class="form-control" name="training_referee" placeholder="Referee" value="<?php echo $training_referee;?>">
                                    </div>

                                    
                                    <div class="col-lg-4">
                                        <input type="text" class="form-control" name="futuro_instructor" placeholder="Futuro Instructor" value="<?php echo $futuro_instructor;?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    
                                    <div class="col-lg-4 col-lg-offset-4">
                                        <input type="text" class="form-control" name="caf_instructor" placeholder="CAF Instructor" value="<?php echo $caf_instructor;?>">
                                    </div>
                                    
                                    <div class="col-lg-4">
                                        <input type="text" class="form-control" name="fifa_instructor" placeholder="FIFA Instructor" value="<?php echo $fifa_instructor;?>">
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-lg-4 control-label">Grades Attained: </label>
                                    <div class="col-lg-8">
                                        <div class="col-lg-3">
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="grades_attained" id="optionsRadios1" value="1" <?php echo $twenty;?>>
                                                    0-20
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="grades_attained" id="optionsRadios2" value="2" <?php echo $forty;?>>
                                                    21-40
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="grades_attained" id="optionsRadios3" value="3" <?php echo $sixty;?>>
                                                    41-60
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="grades_attained" id="optionsRadios4" value="4" <?php echo $eighty;?>>
                                                    61-80
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="grades_attained" id="optionsRadios5" value="5" <?php echo $hundred;?>>
                                                    81-100
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                
                                <div class="form-group">
                                    <label class="col-lg-4 control-label">Current Status </label>
                                    
                                    <div class="col-lg-4">
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="current_status" id="optionsRadios1" value="1" <?php echo $international1;?>>
                                                International - Elite A FIFA
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="current_status" id="optionsRadios2" value="2" <?php echo $international2;?>>
                                                International - National division 1 & 2
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-lg-offset-4">
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="current_status" id="optionsRadios2" value="3" <?php echo $national1;?>>
                                                National - Elite B KPL & NSL
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="radio">
                                            <label>
                                                <input type="radio" name="current_status" id="optionsRadios2" value="4" <?php echo $national2;?>>
                                                National - Branch Level
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>

                    <?php

                }
                ?>


                    <div class="row" style="margin-top:10px;">
                    	<div class="col-md-12">
                            <div class="form-actions center-align">
                                <button class="submit btn btn-primary" type="submit">
                                    Add personnel
                                </button>
                            </div>
                        </div>
                    </div>
                    <?php echo form_close();?>
                </div>
            </div>
        </section>