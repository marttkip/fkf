<?php
//personnel data
$row = $personnel->row();

// var_dump($row) or die();
$personnel_onames = $row->personnel_onames;
$personnel_fname = $row->personnel_fname;
$personnel_dob = $row->personnel_dob;
$personnel_email = $row->personnel_email;
$personnel_phone = $row->personnel_phone;
$personnel_address = $row->personnel_address;
$civil_status_id = $row->civilstatus_id;
$personnel_locality = $row->personnel_locality;
$title_id = $row->title_id;
$gender_id = $row->gender_id;
$personnel_username = $row->personnel_username;
$personnel_kin_fname = $row->personnel_kin_fname;
$personnel_kin_onames = $row->personnel_kin_onames;
$personnel_kin_contact = $row->personnel_kin_contact;
$personnel_kin_address = $row->personnel_kin_address;
$kin_relationship_id = $row->kin_relationship_id;
$job_title_idd = $row->job_title_id;
$staff_id = $row->personnel_staff_id;
$bank_branch_id = $row->bank_branch_id;

$educational_background = $row->educational_background;
$year_of_graduation_from = $row->year_of_graduation_from;
$accolades = $row->accolades;
$computer_literacy = $row->computer_literacy;
$occupation = $row->occupation;
$year_of_graduation_to = $row->year_of_graduation_to;
$year_qualified = $row->year_qualified;
$training_town = $row->training_town;
$training_county = $row->training_county;
$training_referee = $row->training_referee;
$futuro_instructor = $row->futuro_instructor;
$caf_instructor = $row->caf_instructor;
$fifa_instructor = $row->fifa_instructor;
$grades_attained = $row->grades_attained;
$current_status = $row->current_status;


//echo $gender_id;
//repopulate data if validation errors occur
$validation_error = validation_errors();
				
if(!empty($validation_error))
{
	$personnel_onames =set_value('personnel_onames');
	$personnel_fname =set_value('personnel_fname');
	$personnel_dob =set_value('personnel_dob');
	$personnel_email =set_value('personnel_email');
	$personnel_phone =set_value('personnel_phone');
	$personnel_address =set_value('personnel_address');
	$civil_status_id =set_value('civil_status_id');
	$personnel_locality =set_value('personnel_locality');
	$title_id =set_value('title_id');
	$gender_id =set_value('gender_id');
	$personnel_username =set_value('personnel_username');
	$personnel_kin_fname =set_value('personnel_kin_fname');
	$personnel_kin_onames =set_value('personnel_kin_onames');
	$personnel_kin_contact =set_value('personnel_kin_contact');
	$personnel_kin_address =set_value('personnel_kin_address');
	$kin_relationship_id =set_value('kin_relationship_id');
	$job_title_id =set_value('job_title_id');
	$staff_id =set_value('staff_id');
	$bank_id2 = set_value('bank_id');
	$bank_branch_id2 = set_value('bank_branch_id');

	$educational_background = set_value('educational_background');
	$year_of_graduation_from = set_value('year_of_graduation_from');
	$accolades = set_value('accolades');
	$computer_literacy = set_value('computer_literacy');
	$occupation = set_value('occupation');
	$year_of_graduation_to = set_value('year_of_graduation_to');
	$year_qualified = set_value('year_qualified');
	$training_town = set_value('training_town');
	$training_county = set_value('training_county');
	$training_referee = set_value('training_referee');
	$futuro_instructor = set_value('futuro_instructor');
	$caf_instructor = set_value('caf_instructor');
	$fifa_instructor = set_value('fifa_instructor');
	$grades_attained = set_value('grades_attained');
	$current_status = set_value('current_status');
	
}

$primary = $secondary = $college = $university = '';

if($educational_background == 1)
{
    $primary = 'checked';
}

else if($educational_background == 2)
{
    $secondary = 'checked';
}

else if($educational_background == 3)
{
    $college = 'checked';
}

else if($educational_background == 4)
{
    $university = 'checked';
}

$certificate = $diploma = $degree = '';

if($accolades == 1)
{
    $certificate = 'checked';
}

else if($accolades == 2)
{
    $diploma = 'checked';
}

else if($accolades == 3)
{
    $degree = 'checked';
}

$nil = $semi = $literate = '';

if($computer_literacy == 1)
{
    $nil = 'checked';
}

else if($computer_literacy == 2)
{
    $semi = 'checked';
}

else if($computer_literacy == 3)
{
    $literate = 'checked';
}

$self = $employed = '';

if($occupation == 1)
{
    $employed = 'checked';
}

else if($occupation == 2)
{
    $self = 'checked';
}

$twenty = $forty = $sixty = $eighty = $hundred = '';

if($grades_attained == 1)
{
    $twenty = 'checked';
}

else if($grades_attained == 2)
{
    $forty = 'checked';
}

else if($grades_attained == 3)
{
    $sixty = 'checked';
}

else if($grades_attained == 4)
{
    $eighty = 'checked';
}

else if($grades_attained == 5)
{
    $hundred = 'checked';
}

$active_referee = $match_commissioner = '';


$international1 = $international2 = $national1 = $national2 = '';

if($current_status == 1)
{
    $international1 = 'checked';
}

else if($current_status == 2)
{
    $international2 = 'checked';
}

else if($current_status == 3)
{
    $national1 = 'checked';
}

else if($current_status == 4)
{
    $national2 = 'checked';
}
?>
		<div class="row">
        	<!-- <div class="col-md-2">
            	<img src="<?php echo base_url().'assets/img/avatar.jpg';?>" class="img-responsive img-thumbnail" />
            </div> -->
            
            <div class="col-md-9">
            	<!-- <h2><?php echo $personnel_fname.' '.$personnel_onames;?></h2>
                <p>
                    <i class="fa fa-phone"/></i>
                    <span id="mobile_phone"><?php echo $personnel_phone;?></span>
                    <i class="fa fa-envelope"/></i>
                    <span id="work_email"><?php echo $personnel_email;?></span>
                </p> -->
            </div>
            
        	<div class="col-md-3">
            
            </div>
        </div>
      	<div class="row">
        
          <section class="panel">

                <header class="panel-heading">
                	<div class="row">
	                	<div class="col-md-6">
		                    <h2 class="panel-title"><?php echo $title;?> <?php echo $personnel_fname.' '.$personnel_onames;?> Details</h2>
		                    <i class="fa fa-phone"/></i>
		                    <span id="mobile_phone"><?php echo $personnel_phone;?></span>
		                    <i class="fa fa-envelope"/></i>
		                    <span id="work_email"><?php echo $personnel_email;?></span>
		                </div>
		                <div class="col-md-6">
		                	<?php
		                	if($module == 2)
		                	{
		                		?>
		                		<a href="<?php echo site_url();?>human-resource/users" class="btn btn-sm btn-info pull-right fa fa-arrow-left"> Back to users</a>

		                		<?php
		                	}
		                	else
		                	{
		                		?>
		                		<a href="<?php echo site_url();?>human-resource/personnel" class="btn btn-sm btn-info pull-right fa fa-arrow-left"> Back to personnel</a>

		                		<?php
		                	}
		                	?>
		                		
		                </div>
	                </div>
                </header>
                <div class="panel-body">
                    
                    <div class="row">
                    	<div class="col-md-12">
                        	<?php
                            	$success = $this->session->userdata('success_message');
                            	$error = $this->session->userdata('error_message');
								
								if(!empty($success))
								{
									echo '
										<div class="alert alert-success">'.$success.'</div>
									';
									
									$this->session->unset_userdata('success_message');
								}
								
								if(!empty($error))
								{
									echo '
										<div class="alert alert-danger">'.$error.'</div>
									';
									
									$this->session->unset_userdata('error_message');
								}
								
							?>
                        	<div class="tabs">
								<ul class="nav nav-tabs nav-justified">
									<li class="active">
										<a class="text-center" data-toggle="tab" href="#general"><i class="fa fa-user"></i> General details</a>
									</li>
									<?php
									if($module != 2)
									{
										?>
										<li>
											<a class="text-center" data-toggle="tab" href="#uploads">Uploads</a>
										</li>
										<?php
									}
									?>
									<li>
										<a class="text-center" data-toggle="tab" href="#account"><i class="fa fa-lock"></i> Account details</a>
									</li>
									<?php
									if($module != 2)
									{
										?>
											<li>
												<a class="text-center" data-toggle="tab" href="#emergency">Emergency contacts</a>
											</li>
											<li>
												<a class="text-center" data-toggle="tab" href="#dependants">Dependants</a>
											</li>
									<?php
									}
									?>
									<li>
										<a class="text-center" data-toggle="tab" href="#job">Job</a>
									</li>
									<?php
									if($module != 2)
									{
										?>
									<li>
										<a class="text-center" data-toggle="tab" href="#leave"><i class="fa fa-calendar-check-o"></i> Leave</a>
									</li>
									<?php
									}
									?>
								</ul>
								<div class="tab-content">
									<div class="tab-pane active" id="general">
										<?php echo $this->load->view('edit/about', '', TRUE);?>
									</div>
									<div class="tab-pane" id="uploads">
										<?php echo $this->load->view('edit/uploads', '', TRUE);?>
									</div>
									<div class="tab-pane" id="account">
										<?php echo $this->load->view('edit/account', '', TRUE);?>
									</div>
									<div class="tab-pane" id="emergency">
										<?php echo $this->load->view('edit/emergency', '', TRUE);?>
									</div>
									<div class="tab-pane" id="dependants">
										<?php echo $this->load->view('edit/dependants', '', TRUE);?>
									</div>
									<div class="tab-pane" id="job">
										<?php echo $this->load->view('edit/jobs', '', TRUE);?>
									</div>
									<div class="tab-pane" id="leave">
										<?php echo $this->load->view('edit/leave', '', TRUE);?>
									</div>
								</div>
							</div>
                        </div>
                    </div>
                </div>
            </section>
        </div>