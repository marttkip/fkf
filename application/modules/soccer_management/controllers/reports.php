<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once "./application/modules/soccer_management/controllers/soccer_management.php";

class Reports extends soccer_management 
{
	function __construct()
	{
		parent:: __construct();
	}
	
	public function tournament_payments()
	{
		//$where = 'tournament_fixture.tournament_fixture_id = tournament_fixture_referee.tournament_fixture_id AND tournament_fixture_referee.referee_id = tournament_fixture_referee_payment.referee_id AND tournament_fixture.tournament_fixture_id = tournament_fixture_referee_payment.tournament_fixture_id AND tournament_fixture.tournament_fixture_id = tournament_fixture_team.tournament_fixture_id AND tournament_fixture_team.tournament_team_id = tournament_team.tournament_team_id AND tournament_team.team_id = team.team_id AND tournament_fixture_referee_payment.referee_id = referee.referee_id';
		//$table = 'tournament_fixture, tournament_fixture_referee, tournament_fixture_referee_payment, tournament_team, team, tournament_fixture_team,referee';
		
		//$v_data['tournament_referee_payments'] = $this->reports_model->get_all_tournament_payments($table, $where);
		$v_data['tournament_referee_payments'] = $this->reports_model->referee_tournament_payements();
		$data['title'] = $v_data['title'] = 'Tournament Payments';
		$data['content'] = $this->load->view('reports/tournament_payments', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}


	public function match_report()
	{
		$week_date = $this->session->userdata('week_search');
		// var_dump($week_date); die();
		if(!empty($week_date))
		{

			$week =  date('W', strtotime($week_date));
			$explode = explode('-', $week_date);
			$year = $explode[0];
		}
		else
		{

			$week =  date('W', strtotime(date('Y-m-d')));	

			$year = date('Y');
		}
		
		$dates = $this->reports_model->getWeekDates($year,$week);
		// var_dump($dates); die();
		$title = $dates['from']. ' - '.$dates['to'].'';
		$v_data['tournament_referee_payments'] = $this->reports_model->referee_tournament_payements();
		$v_data['league_matches'] = $this->reports_model->get_league_matches($dates['from'],$dates['to']);
		$v_data['tournament_matches'] = $this->reports_model->get_tournament_matches($dates['from'],$dates['to']);
		$v_data['league_referees'] = $this->reports_model->get_league_matches_referee($dates['from'],$dates['to']);
		$v_data['tournament_referees'] = $this->reports_model->get_tournament_matches_referee($dates['from'],$dates['to']);
		$data['title'] = $v_data['title'] = $title;
		$data['content'] = $this->load->view('reports/match_report', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}

	public function search_match_report()
	{
		$search_title = '';
		//search league name
		if(!empty($_POST['week_date']))
		{
			$search_title .= ' Date of the week <strong>'.$_POST['week_date'].'</strong>';
			$week_date = $_POST['week_date'];	
			
		}
		
		else
		{
			$week_date = date('Y-m-d');
		}
		
		$search =$week_date;
		$this->session->set_userdata('week_search', $search);
		$this->session->set_userdata('league_search_title2', $search_title);
		redirect('reports/matches-report');
	}
	public function referees()
	{
		//league_referees
		$league_referees = $this->payments_model->get_league_payment_referees();
		$v_data['league_referees'] = $league_referees;
		$tournament_referees = $this->payments_model->get_tournament_payment_referees();
		$v_data['tournament_referees'] = $tournament_referees;
		$v_data['title'] = $data['title'] = 'Referee Payments';
		
		$data['content'] = $this->load->view('reports/payments', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}
}
?>