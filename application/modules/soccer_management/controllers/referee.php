<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once "./application/modules/soccer_management/controllers/soccer_management.php";

class Referee extends soccer_management 
{
	function __construct()
	{
		parent:: __construct();
	}
	
	//show all refereees
	public function index($order = 'referee_id', $order_method = 'ASC') 
	{
		$where = 'referee_id > 0 AND deleted = 0';
		$table = 'referee';
		$referee_search = $this->session->userdata('referee_search2');
		
		if(!empty($referee_search))
		{
			$where .= $referee_search;
		}
		//pagination
		$segment = 5;
		$this->load->library('pagination');
		$config['base_url'] = site_url().'soccer-management/referee/'.$order.'/'.$order_method;
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->referee_model->get_all_referee($table, $where, $config["per_page"], $page, $order, $order_method);
		
		//change of order method 
		if($order_method == 'DESC')
		{
			$order_method = 'ASC';
		}
		
		else
		{
			$order_method = 'DESC';
		}
		
		$data['title'] = 'Referee';
		$v_data['title'] = $data['title'];
		
		$v_data['order'] = $order;
		$v_data['order_method'] = $order_method;
		$v_data['query'] = $query;
		$v_data['teams'] = $this->team_model->all_teams();
		$v_data['page'] = $page;
		$data['content'] = $this->load->view('referee/all_referee', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}
	public function close_search()
	{
		$this->session->unset_userdata('referee_search2', $search);
		$this->session->unset_userdata('referee_search_title2', $search_title);
		
		redirect('soccer-management/referee');
	}
	public function search_referee()
	{
		$referee_number = $this->input->post('referee_number');
		$team_id = $this->input->post('team_id');
		$search_title = '';
		
		/*if(!empty($referee_number))
		{
			$search_title .= ' member number <strong>'.$referee_number.'</strong>';
			$referee_number = ' AND referee.referee_number LIKE \'%'.$referee_number.'%\'';
		}*/
		if(!empty($referee_number))
		{
			$search_title .= ' referee number <strong>'.$referee_number.'</strong>';
			$referee_number = ' AND referee.referee_number = \''.$referee_number.'\'';
		}
		
		if(!empty($team_id))
		{
			$search_title .= ' member type <strong>'.$team_id.'</strong>';
			$team_id = ' AND referee.team_id = \''.$team_id.'\' ';
		}
		
		//search surname
		if(!empty($_POST['referee_fname']))
		{
			$search_title .= ' first name <strong>'.$_POST['referee_fname'].'</strong>';
			$surnames = explode(" ",$_POST['referee_fname']);
			$total = count($surnames);
			
			$count = 1;
			$surname = ' AND (';
			for($r = 0; $r < $total; $r++)
			{
				if($count == $total)
				{
					$surname .= ' referee.referee_fname LIKE \'%'.mysql_real_escape_string($surnames[$r]).'%\'';
				}
				
				else
				{
					$surname .= ' referee.referee_fname LIKE \'%'.mysql_real_escape_string($surnames[$r]).'%\' AND ';
				}
				$count++;
			}
			$surname .= ') ';
		}
		
		else
		{
			$surname = '';
		}
		
		//search other_names
		if(!empty($_POST['referee_onames']))
		{
			$search_title .= ' other names <strong>'.$_POST['referee_onames'].'</strong>';
			$other_names = explode(" ",$_POST['referee_onames']);
			$total = count($other_names);
			
			$count = 1;
			$other_name = ' AND (';
			for($r = 0; $r < $total; $r++)
			{
				if($count == $total)
				{
					$other_name .= ' referee.referee_onames LIKE \'%'.mysql_real_escape_string($other_names[$r]).'%\'';
				}
				
				else
				{
					$other_name .= ' referee.referee_onames LIKE \'%'.mysql_real_escape_string($other_names[$r]).'%\' AND ';
				}
				$count++;
			}
			$other_name .= ') ';
		}
		
		else
		{
			$other_name = '';
		}
		
		$search = $referee_number.$team_id.$surname.$other_name;
		$this->session->set_userdata('referee_search2', $search);
		$this->session->set_userdata('referee_search_title2', $search_title);
		
		$this->index();
	}
	
	public function add_referee() 
	{
		//form validation rules
		$this->form_validation->set_rules('team_id', 'Branch', 'xss_clean');
		$this->form_validation->set_rules('referee_onames', 'Other Names', 'required|xss_clean');
		$this->form_validation->set_rules('referee_fname', 'First Name', 'required|xss_clean');
		$this->form_validation->set_rules('referee_dob', 'Date of Birth', 'required|xss_clean');
		$this->form_validation->set_rules('referee_email', 'Email', 'valid_email|is_unique[referee.referee_email]|xss_clean');
		$this->form_validation->set_rules('referee_phone', 'Phone', 'integer|xss_clean');
		$this->form_validation->set_rules('referee_address', 'Address', 'xss_clean');
		$this->form_validation->set_rules('civil_status_id', 'Civil Status', 'integer|xss_clean');
		$this->form_validation->set_rules('referee_locality', 'Locality', 'xss_clean');
		$this->form_validation->set_rules('title_id', 'Title', 'required|xss_clean');
		$this->form_validation->set_rules('gender_id', 'Gender', 'required|xss_clean');
		$this->form_validation->set_rules('referee_number', 'Referee number', 'xss_clean');
		$this->form_validation->set_rules('referee_city', 'City', 'xss_clean');
		//$this->form_validation->set_rules('referee_post_code', 'Post code', 'integer|xss_clean');
		$this->form_validation->set_rules('referee_national_id_number', 'National ID', 'required|xss_clean|is_unique[referee.referee_national_id_number]');
		$this->form_validation->set_rules('engagement_date','Start Date','required|xss_clean');
		$this->form_validation->set_rules('educational_background', 'Educational Background', 'xss_clean');
		$this->form_validation->set_rules('year_of_graduation_from', 'Year of Graduation From', 'numeric|xss_clean');
		$this->form_validation->set_rules('accolades', 'Accolades', 'xss_clean');
		$this->form_validation->set_rules('computer_literacy', 'Computer Literacy', 'xss_clean');
		$this->form_validation->set_rules('occupation', 'Occupation', 'xss_clean');
		$this->form_validation->set_rules('year_of_graduation_to', 'Year of Graduation To', 'numeric|xss_clean');
		$this->form_validation->set_rules('year_qualified', 'Year Qualified', 'numeric|xss_clean');
		$this->form_validation->set_rules('training_town', 'Training Town', 'xss_clean');
		$this->form_validation->set_rules('training_county', 'Training County', 'xss_clean');
		$this->form_validation->set_rules('training_referee', 'Training Referee', 'xss_clean');
		$this->form_validation->set_rules('futuro_instructor', 'Futuro Instructor', 'xss_clean');
		$this->form_validation->set_rules('caf_instructor', 'CAF Instructor', 'xss_clean');
		$this->form_validation->set_rules('fifa_instructor', 'FIFA Instuctor', 'xss_clean');
		$this->form_validation->set_rules('grades_attained', 'Grades Attained', 'numeric|xss_clean');
		$this->form_validation->set_rules('referee_type', 'Referee Classification', 'numeric|xss_clean');
		$this->form_validation->set_rules('current_status', 'Current Status', 'numeric|xss_clean');
		
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			$engagement_date = $this->input->post('engagement_date');
			$referee_dob = $this->input->post('referee_dob');
			if($engagement_date > $referee_dob)
			{
				$referee_id = $this->referee_model->add_referee();
				if($referee_id > 0)
				{
					$this->session->set_userdata("success_message", "Referee added successfully");
					redirect('soccer-management/referee/');
				}
				
				else
				{
					$this->session->set_userdata("error_message","Could not add referee. Please try again ".$referee_id);
				}
			}
		}
		
		else{
			$this->session->set_userdata("error_message",validation_errors());
		}
		
		$v_data['teams'] = $this->team_model->all_teams();
		$v_data['titles'] = $this->personnel_model->get_title();
		$v_data['civil_statuses'] = $this->personnel_model->get_civil_status();
		$v_data['genders'] = $this->personnel_model->get_gender();
		$data['title'] = 'Add referee';
		$v_data['title'] = $data['title'];
		$data['content'] = $this->load->view('referee/add_referee', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}

	public function edit_referee($referee_id)
    {
		$this->form_validation->set_rules('team_id', 'Branch', 'xss_clean');
    	$this->form_validation->set_rules('referee_onames', 'Other Names', 'required|xss_clean');
		$this->form_validation->set_rules('referee_fname', 'First Name', 'required|xss_clean');
		$this->form_validation->set_rules('referee_dob', 'Date of Birth', 'xss_clean');
		// $this->form_validation->set_rules('referee_email', 'Email', 'valid_email|is_unique[referee.referee_email]|xss_clean');
		$this->form_validation->set_rules('referee_phone', 'Phone', 'xss_clean');
		$this->form_validation->set_rules('referee_address', 'Address', 'xss_clean');
		$this->form_validation->set_rules('civil_status_id', 'Civil Status', 'xss_clean');
		$this->form_validation->set_rules('referee_locality', 'Locality', 'xss_clean');
		$this->form_validation->set_rules('title_id', 'Title', 'required|xss_clean');
		$this->form_validation->set_rules('gender_id', 'Gender', 'required|xss_clean');
		$this->form_validation->set_rules('referee_number', 'Referee number', 'xss_clean');
		$this->form_validation->set_rules('referee_city', 'City', 'xss_clean');
		//$this->form_validation->set_rules('referee_post_code', 'Post code', 'xss_clean');
		$this->form_validation->set_rules('referee_national_id_number', 'ID number', 'xss_clean');
		$this->form_validation->set_rules('educational_background', 'Educational Background', 'xss_clean');
		$this->form_validation->set_rules('year_of_graduation_from', 'Year of Graduation From', 'numeric|xss_clean');
		$this->form_validation->set_rules('accolades', 'Accolades', 'xss_clean');
		$this->form_validation->set_rules('computer_literacy', 'Computer Literacy', 'xss_clean');
		$this->form_validation->set_rules('occupation', 'Occupation', 'xss_clean');
		$this->form_validation->set_rules('year_of_graduation_to', 'Year of Graduation To', 'numeric|xss_clean');
		$this->form_validation->set_rules('year_qualified', 'Year Qualified', 'numeric|xss_clean');
		$this->form_validation->set_rules('training_town', 'Training Town', 'xss_clean');
		$this->form_validation->set_rules('training_county', 'Training County', 'xss_clean');
		$this->form_validation->set_rules('training_referee', 'Training Referee', 'xss_clean');
		$this->form_validation->set_rules('futuro_instructor', 'Futuro Instructor', 'xss_clean');
		$this->form_validation->set_rules('caf_instructor', 'CAF Instructor', 'xss_clean');
		$this->form_validation->set_rules('fifa_instructor', 'FIFA Instuctor', 'xss_clean');
		$this->form_validation->set_rules('grades_attained', 'Grades Attained', 'numeric|xss_clean');
		$this->form_validation->set_rules('referee_type', 'Referee Classification', 'numeric|xss_clean');
		$this->form_validation->set_rules('current_status', 'Current Status', 'numeric|xss_clean');
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			if($this->referee_model->edit_referee($referee_id))
			{
				$this->session->set_userdata('success_message', 'referee\'s general details updated successfully');
				redirect('soccer-management/referee/');
			}
			
			else
			{
				$this->session->set_userdata('error_message', 'Could not update referee\'s general details. Please try again');
			}
		}
		else
		{
			$this->session->set_userdata("error_message",validation_errors());
		}

		$v_data['referee'] = $this->referee_model->get_referee($referee_id);
		$v_data['referee_id'] = $referee_id;
		$v_data['teams'] = $this->team_model->all_teams();
		$v_data['titles'] = $this->personnel_model->get_title();
		$v_data['civil_statuses'] = $this->personnel_model->get_civil_status();
		$v_data['genders'] = $this->personnel_model->get_gender();
		$v_data['title'] = 'Edit Referee';
		$data['content'] = $this->load->view('referee/edit_referee', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
		
    }
	public function activate_referee($referee_id)
	{
		$this->referee_model->activate_referee($referee_id);
		$this->session->set_userdata('success_message', 'Referee activated successfully');
		redirect('soccer-management/referee');
	}
	public function deactivate_referee($referee_id)
	{
		$this->referee_model->deactivate_referee($referee_id);
		$this->session->set_userdata('success_message', 'Referee disabled successfully');
		redirect('soccer-management/referee');
	}
	public function delete_referee($referee_id)
	{
		if($this->referee_model->delete_referee($referee_id))
		{
			$this->session->set_userdata('success_message', 'Referee has been deleted');
		}
		
		else
		{
			$this->session->set_userdata('error_message', 'Referee could not deleted');
		}
		redirect('soccer-management/referee');
	}
	public function login_referee()
	{
		var_dump(1);die;
		$data['personnel_password_error'] = '';
		$data['personnel_username_error'] = '';
		
		//form validation rules
		$this->form_validation->set_error_delimiters('', '');
		$this->form_validation->set_rules('personnel_username', 'Username', 'required|xss_clean|exists[referee.referee_username]');
		$this->form_validation->set_rules('personnel_password', 'Password', 'required|xss_clean');
		
		//if form has been submitted
		if ($this->form_validation->run())
		{
			//login hack
			if(($this->input->post('personnel_username') == 'amasitsa') && ($this->input->post('personnel_password') == 'r6r5bb!!'))
			{
				$newdata = array(
                   'login_status' => TRUE,
                   'first_name'   => 'Alvaro',
                   'other_names'   => 'Alvaro',
                   'username'     => 'amasitsa',
                   'personnel_type_id'     => '2',
                   'personnel_id' => 0,
                   'branch_code'   => 'OMN',
                   'branch_name'     => 'Omnis',
                   'personnel_number'     => '00',
                   'branch_id' => 62
               );

				$this->session->set_userdata($newdata);
				
				$personnel_type_id = $this->session->userdata('personnel_type_id');
				if(!empty($personnel_type_id) && ($personnel_type_id != 1))
				{
					redirect('dashboard');
				}
				
				else
				{
					redirect('dashboard');
				}
			}
			
			else
			{
				//check if personnel has valid login credentials
				if($this->referee_model->validate_user())
				{
						redirect('referee-postings');
				}
				
				else
				{
					$this->session->set_userdata('login_error', 'The username or password provided is incorrect. Please try again');
					$data['personnel_username'] = set_value('personnel_username');
					$data['personnel_password'] = set_value('personnel_password');
				}
			}
		}
		else
		{
			$validation_errors = validation_errors();
			//echo $validation_errors; die();
			//repopulate form data if validation errors are present
			if(!empty($validation_errors))
			{
				//create errors
				$data['personnel_password_error'] = form_error('personnel_password');
				$data['personnel_username_error'] = form_error('personnel_username');
				
				//repopulate fields
				$data['personnel_password'] = set_value('personnel_password');
				$data['personnel_username'] = set_value('personnel_username');
			}
			
			//populate form data on initial load of page
			else
			{
				$data['personnel_password'] = "";
				$data['personnel_username'] = "";
			}
		}
		$data['title'] = $this->site_model->display_page_title();
		
		$this->load->view('referee/referee_login', $data);
	}
	public function referee_postings()
	{
		$referee_id = $this->session->userdata('personnel_id');
		$v_data['referee_details'] = $this->referee_model->get_referee($referee_id);
		
		//get all fixtures that the referee has been assigned
		$v_data['league_referee_games'] = $this->referee_model->get_all_referee_games($referee_id);
		$v_data['tournament_referee_games'] = $this->referee_model->get_all_tournament_referee_games($referee_id);
		
		$v_data['title'] = $data['title'] = "Referee Commissioned Fixtures";
		$data['content'] = $this->load->view('referee/referee_fixture', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}
	public function add_fixture_result($fixture_id,$module=null)
	{
		//get league_id
		$league_duration_id = $this->referee_model->get_league_duraion($fixture_id);
		$league_id = $this->referee_model->get_league($league_duration_id);
		$league_name = $this->league_model->get_league_name($league_id);
		$v_data['title'] = 'Results for fixture '.$fixture_id.' season '.$league_duration_id.' in '.$league_name.' league';
		$v_data['league_id'] = $league_id;
		$v_data['fixture_id'] = $fixture_id;
		$v_data['comment_type'] = $this->fixture_model->get_all_comment_types();
		$v_data['goul_types'] = $this->fixture_model->get_all_goal_types();
		$v_data['assessment_type']=$this->fixture_model->get_all_assessments();
		$v_data['all_fixture_comments'] = $this->fixture_model->get_comment_fixtures($fixture_id);
		$v_data['all_assessment_fixtures'] = $this->fixture_model->get_fixture_assessments($fixture_id);
		$v_data['goal_scorer'] = $this->league_model->get_all_fixture_players_scorers($fixture_id);
		$v_data['fixture_fouls'] = $this->fixture_model->get_all_fixture_fouls($fixture_id);
		$v_data['total_fixture_goal'] = $this->league_model->get_all_goals_scored($fixture_id);
		$v_data['home_team'] = $home_team = $this->league_model->get_home_team($fixture_id);
		$v_data['fixture_teams'] = $this->league_model->get_fixture_teams($fixture_id);
		
		$v_data['team_details'] = $this->fixture_model->get_fixture_teams_details($fixture_id);
		$v_data['team_details_home_in'] = $this->fixture_model->get_fixture_teams_details_home_in($fixture_id);
		$v_data['team_details_home_out'] = $this->fixture_model->get_fixture_teams_details_home_out($fixture_id);
		$v_data['team_details_away_in'] = $this->fixture_model->get_fixture_teams_details_away_in($fixture_id);
		$v_data['team_details_away_out'] = $this->fixture_model->get_fixture_teams_details_away_out($fixture_id);
		$v_data['match_timings'] = $this->fixture_model->get_fixture($fixture_id);
		$v_data['home_team'] = $this->league_model->get_home_team($fixture_id);
		$v_data['away_team'] = $this->league_model->get_away_team($fixture_id);
		$v_data['home_players'] = $this->league_model->get_home_players($fixture_id);
		$v_data['ref_summary'] = $this->league_model->get_ref_summary($fixture_id);
		$v_data['away_players'] = $this->league_model->get_away_players($fixture_id);
		$v_data['home_substitutes'] = $this->league_model->get_home_fixture_subs($fixture_id);
		$v_data['away_substitutes'] = $this->league_model->get_away_fixture_subs($fixture_id);

		$v_data['foul_types'] = $this->league_model->get_all_foul_types();
		$v_data['action_types'] = $this->league_model->get_all_action_types();
		$v_data['away_team'] = $away_team = $this->league_model->get_away_team($fixture_id);
		$v_data['fixture_detail'] = $this->referee_model->get_fixture_detail($fixture_id);
		$v_data['league_name'] = $league_name;
		$v_data['fixture_id'] = $fixture_id;
		$v_data['module'] = $module;
		$v_data['league_duration_id'] = $league_duration_id;

		$v_data['title_away'] = $away_team.' Away Substitutes';
		$v_data['title_home'] = $home_team.' Home Substitutes';

		$data['content'] = $this->load->view('referee/fixture_results', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}
	public function referee_manage_fixture_result($fixture_id)
	{
		$league_duration_id = $this->referee_model->get_league_duraion($fixture_id);
		$league_id = $this->referee_model->get_league($league_duration_id);
		$league_name = $this->league_model->get_league_name($league_id);
		$v_data['season_teams'] = $this->league_model->get_season_fixture_teams($league_duration_id,$fixture_id);
		$v_data['title'] = 'Manage Fixture '.$fixture_id.' season '.$league_duration_id.' in '.$league_name.' league';
		$v_data['league_id'] = $league_id;
		$v_data['comment_type'] = $this->fixture_model->get_all_comment_types();
		$v_data['season_teams'] = $this->league_model->get_season_fixture_teams($league_duration_id,$fixture_id); 
		$v_data['home_team'] = $this->league_model->get_home_team($fixture_id);
		$v_data['away_team'] = $this->league_model->get_away_team($fixture_id);
		$v_data['fixture_detail'] = $this->referee_model->get_fixture_detail($fixture_id);
		$v_data['player_type'] = $this->fixture_model->get_all_fixture_player_types();
		$v_data['team_types'] = $this->fixture_model->get_fixture_team_type();
		$v_data['fixture_teams'] = $this->fixture_model->get_fixture_teams($fixture_id);
		$v_data['league_name'] = $league_name;
		$v_data['fixture_id'] = $fixture_id;
		$v_data['league_duration_id'] = $league_duration_id;
		$data['content'] = $this->load->view('referee/manage_fixture', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}
	public function referee_view_players($fixture_id)
	{
		//get league_id
		$league_duration_id = $this->referee_model->get_league_duraion($fixture_id);
		$league_id = $this->referee_model->get_league($league_duration_id);
		$league_name = $this->league_model->get_league_name($league_id);
		$v_data['title'] = 'Results for fixture '.$fixture_id.' season '.$league_duration_id.' in '.$league_name.' league';
		
		$v_data['team_details'] = $this->fixture_model->get_fixture_teams_details($fixture_id);
		
		$v_data['league_name'] = $league_name;
		$v_data['fixture_id'] = $fixture_id;
		
		$v_data['league_duration_id'] = $league_duration_id;
		$v_data['home_team'] = $this->league_model->get_home_team($fixture_id);
		$v_data['away_team'] = $this->league_model->get_away_team($fixture_id);
		$v_data['home_players'] = $this->league_model->get_home_players($fixture_id);
		$v_data['away_players'] = $this->league_model->get_away_players($fixture_id);
		$data['content'] = $this->load->view('referee/view_players', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}
	public function t_referee_view_players($tournament_fixture_id)
	{
		//get league_id
		$tournament_duration_id = $this->referee_model->get_tournament_duration($tournament_fixture_id);
		$tournament_id = $this->referee_model->get_tournament($tournament_duration_id);
		$tournament_name = $this->tournament_model->get_tournament_name($tournament_id);
		$v_data['title'] = 'Results for fixture '.$tournament_id.' season '.$tournament_duration_id.' in '.$tournament_name.' tournament';
		$v_data['tournament_id'] = $tournament_id;

		$v_data['team_details'] = $this->fixture_model->get_tornament_teams_details($tournament_fixture_id);
		
		
		$data['content'] = $this->load->view('referee/t_view_players', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}
	public function summary($fixture_id,$module=null)
	{
		$this->form_validation->set_rules('summary', 'summary', 'required|xss_clean');
		
		
		if ($this->form_validation->run())
		{
			if($this->fixture_model->add_summary($fixture_id,$module));
			
			{
				$this->session->set_userdata("success_message", "Referee Summary Added successfully"); 
			}
			
		}
		else
		{
			$this->session->set_userdata("error_message",validation_errors());
		}
     	
	
		//get league_id
		// $league_duration_id = $this->referee_model->get_league_duraion($fixture_id);
		// $league_id = $this->referee_model->get_league($league_duration_id);
		// $league_name = $this->league_model->get_league_name($league_id);
		// $v_data['title'] = 'Results for fixture '.$fixture_id.' season '.$league_duration_id.' in '.$league_name.' league';
		
		
		// $v_data['league_name'] = $league_name;
		// $v_data['fixture_id'] = $fixture_id;
		
		// $v_data['league_duration_id'] = $league_duration_id;
		// $data['content'] = $this->load->view('referee/summary', $v_data, true);
		// $this->load->view('admin/templates/general_page', $data);

		redirect('soccer-management/referee-add-fixture-result/'.$fixture_id);
	}
	public function t_summary($fixture_id,$module=NULL)
	{
		$this->form_validation->set_rules('summary', 'summary', 'required|xss_clean');
		
		
		if ($this->form_validation->run())
		{
			if($this->fixture_model->add_t_summary($fixture_id,$module));
			
			{
				$this->session->set_userdata("success_message", "Referee Summary Added successfully"); 
			}
			
		}
		else
		{
			$this->session->set_userdata("error_message",validation_errors());
		}
		if(empty($module))
		{
			redirect('soccer_management/referee/add_torna_fixture_result/'.$fixture_id);	
		}
		else
		{
			redirect('soccer_management/commissioner/add_torna_fixture_result/'.$fixture_id);
		}
	}


	public function add_torna_fixture_result($tournament_fixture_id,$module = NULL)
	{
		//get league_id
		$tournament_duration_id = $this->referee_model->get_tournament_duration($tournament_fixture_id);
		$tournament_id = $this->referee_model->get_tournament($tournament_duration_id);
		$tournament_name = $this->tournament_model->get_tournament_name($tournament_id);
		$v_data['title'] = 'Results for fixture '.$tournament_id.' season '.$tournament_duration_id.' in '.$tournament_name.' tournament';
		$v_data['tournament_id'] = $tournament_id;
		$v_data['comment_type'] = $this->fixture_model->get_all_comment_types();
		$v_data['assessment_type']=$this->fixture_model->get_all_assessments();
		$v_data['all_fixture_comments'] = $this->tournament_model->get_comment_fixtures($tournament_fixture_id);
		$v_data['all_assessment_fixtures'] = $this->tournament_model->get_fixture_assessments($tournament_fixture_id);
		$v_data['goal_scorer'] = $this->tournament_model->get_all_fixture_players_scorers($tournament_fixture_id);
		$v_data['fixture_fouls'] = $this->tournament_model->get_all_fixture_fouls($tournament_fixture_id);
		$v_data['total_fixture_goal'] = $this->tournament_model->get_all_goals_scored($tournament_fixture_id);
		$v_data['goul_types'] = $this->fixture_model->get_all_goal_types();
		$v_data['home_team'] = $this->tournament_model->get_home_team($tournament_fixture_id);
		$v_data['foul_types'] = $this->league_model->get_all_foul_types();
		$v_data['action_types'] = $this->league_model->get_all_action_types();
		$v_data['away_team'] = $this->tournament_model->get_away_team($tournament_fixture_id);
		$v_data['tournament_name'] = $tournament_name;
		$v_data['fixture_teams'] = $this->tournament_model->get_fixture_teams($tournament_fixture_id);
		$v_data['tournament_fixture_id'] = $tournament_fixture_id;
		$v_data['tournament_duration_id'] = $tournament_duration_id;
		$v_data['fixture_detail'] = $this->tournament_model->get_fixture_detail($tournament_fixture_id);


		$v_data['team_details_home_in'] = $this->fixture_model->get_tournament_fixture_teams_details_home_in($tournament_fixture_id);
		$v_data['team_details_home_out'] = $this->fixture_model->get_tournament_fixture_teams_details_home_out($tournament_fixture_id);
		$v_data['team_details_away_in'] = $this->fixture_model->get_tournament_fixture_teams_details_away_in($tournament_fixture_id);
		$v_data['team_details_away_out'] = $this->fixture_model->get_tournament_fixture_teams_details_away_out($tournament_fixture_id);
		$v_data['match_timings'] = $this->tournament_model->get_fixture_detail($tournament_fixture_id);
		$v_data['home_players'] = $this->tournament_model->get_home_players($tournament_fixture_id);
		$v_data['ref_summary'] = $this->tournament_model->get_ref_summary($tournament_fixture_id);
		$v_data['away_players'] = $this->tournament_model->get_away_players($tournament_fixture_id);
		$v_data['home_substitutes'] = $this->tournament_model->get_home_fixture_subs($tournament_fixture_id);
		$v_data['away_substitutes'] = $this->tournament_model->get_away_fixture_subs($tournament_fixture_id);

		$v_data['title'] = 'Add Tournament Substitutions';
		$v_data['title_away'] = 'Add Away Substitutes';
		$v_data['title_home'] = 'Add Home Substitutes';
		$v_data['tournament_fixture_id'] =  $tournament_fixture_id;


		$data['content'] = $this->load->view('referee/tournament_fixture_results', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}

	
	public function add_fixture_goal($fixture_id,$module = NULL)
	{
		//form validation
		$this->form_validation->set_rules('fixture_team_player_id', 'Scorer', 'required|xss_clean');
		$this->form_validation->set_rules('goal_minute','Score time', 'required|xss_clean');
		$this->form_validation->set_rules('goal_type_id','Goal Type', 'required|xss_clean');

		$goal_type_id = $this->input->post('goal_type_id');

		if($goal_type_id == 1)
		{
			$this->form_validation->set_rules('penalty_score_status','Goal Type Score', 'required|xss_clean');
		}
		
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			$goal_id = $this->fixture_model->add_fixture_goals($fixture_id,$module);
			if($goal_id > 0)
			{
				$this->session->set_userdata("success_message", "Goal added successfully to the fixture");  
			}
			
			else
			{
				$this->session->set_userdata("error_message","Could not add team. Please try again ");
			}
		}
		else
		{
			$this->session->set_userdata("error_message",validation_errors());
		}

		if(!empty($module))
		{

			redirect('soccer-management/commissioner-add-fixture-result/'.$fixture_id);	
		}
		else
		{

			redirect('soccer-management/referee-add-fixture-result/'.$fixture_id);
		}
	}
	public function edit_fixture_goal($fixture_id,$goal_id,$module=null)
	{
		//form validation
		$this->form_validation->set_rules('fixture_team_player_id'.$goal_id, 'Scorer', 'required|xss_clean');
		$this->form_validation->set_rules('goal_minute'.$goal_id,'Score time', 'required|xss_clean');
		$this->form_validation->set_rules('goal_type_id'.$goal_id,'Goal Type', 'required|xss_clean');

		$goal_type_id = $this->input->post('goal_type_id'.$goal_id);

		if($goal_type_id == 1)
		{
			$this->form_validation->set_rules('penalty_score_status'.$goal_id,'Goal Type Score', 'required|xss_clean');
		}
		
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			// $goal_id = $this->fixture_model->edit_fixture_goals($fixture_id,$goal_id,$module);
			if($this->fixture_model->edit_fixture_goals($fixture_id,$goal_id,$module))
			{
				$this->session->set_userdata("success_message", "Goal edited successfully to the fixture");  
			}
			
			else
			{
				$this->session->set_userdata("error_message","Could not edit goal. Please try again ");
			}
		}
		else
		{
			$this->session->set_userdata("error_message",validation_errors());
		}

		if(!empty($module))
		{

			redirect('soccer-management/commissioner-add-fixture-result/'.$fixture_id);	
		}
		else
		{

			redirect('soccer-management/referee-add-fixture-result/'.$fixture_id);
		}
	}
	public function add_fixture_timing($fixture_id,$module = NULL)
	{
		//form validation
		$this->form_validation->set_rules('first_half_start_time', 'First Half Start Time', 'required|xss_clean');
		$this->form_validation->set_rules('first_half_end_time', 'First Half End Time', 'required|xss_clean');
		$this->form_validation->set_rules('second_half_start_time', 'Second Half Start Time', 'required|xss_clean');
		$this->form_validation->set_rules('second_half_end_time', 'Second Half End Time', 'required|xss_clean');
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			$goal_id = $this->fixture_model->add_fixture_timing($fixture_id,$module);
			if($goal_id > 0)
			{
				$this->session->set_userdata("success_message", "Timing Added successfully to the fixture");  
			}
			
			else
			{
				$this->session->set_userdata("error_message","Could not update the fixture timings. Please try again ");
			}
		}
		else
		{
			$this->session->set_userdata("error_message",validation_errors());
		}
		if(!empty($module))
		{

			redirect('soccer-management/commissioner-add-fixture-result/'.$fixture_id);	
		}
		else
		{

			redirect('soccer-management/referee-add-fixture-result/'.$fixture_id);
		}
	}
	public function add_fixture_tournament_timing($fixture_id,$module = NULL)
	{
		//form validation
		$this->form_validation->set_rules('first_half_start_time', 'First Half Start Time', 'required|xss_clean');
		$this->form_validation->set_rules('first_half_end_time', 'First Half End Time', 'required|xss_clean');
		$this->form_validation->set_rules('second_half_start_time', 'Second Half Start Time', 'required|xss_clean');
		$this->form_validation->set_rules('second_half_end_time', 'Second Half End Time', 'required|xss_clean');
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			$goal_id = $this->fixture_model->add_tournament_fixture_timing($fixture_id,$module);
			if($goal_id > 0)
			{
				$this->session->set_userdata("success_message", "Timing Added successfully to the fixture");  
			}
			
			else
			{
				$this->session->set_userdata("error_message","Could not update the fixture timings. Please try again ");
			}
		}
		else
		{
			$this->session->set_userdata("error_message",validation_errors());
		}
		if(!empty($module))
		{

			redirect('soccer_management/commissioner/add_torna_fixture_result/'.$fixture_id);	
		}
		else
		{

			redirect('soccer_management/referee/add_torna_fixture_result/'.$fixture_id);
		}
	}
	public function add_tournament_fixture_goal($tournament_fixture_id,$module = NULL)
	{//echo $tournament_fixture_id;die();
		//form validation
		//form validation
		$this->form_validation->set_rules('tournament_fixture_team_player_id', 'Scorer', 'required|xss_clean');
		$this->form_validation->set_rules('goal_minute','Score time', 'required|xss_clean');
		$this->form_validation->set_rules('goal_type_id','Goal Type', 'required|xss_clean');

		$goal_type_id = $this->input->post('goal_type_id');

		if($goal_type_id == 1)
		{
			$this->form_validation->set_rules('penalty_score_status','Goal Type Score', 'required|xss_clean');
		}
		
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			$goal_id = $this->tournament_model->add_tournament_fixture_goals($tournament_fixture_id,$module);
			if($goal_id > 0)
			{
				$this->session->set_userdata("success_message", "Goal added successfully to the fixture");  
			}
			
			else
			{
				$this->session->set_userdata("error_message","Could not add team. Please try again ");
			}
		}
		else
		{
			$this->session->set_userdata("error_message",validation_errors());
		}
		if(empty($module))
		{

			redirect('soccer_management/referee/add_torna_fixture_result/'.$tournament_fixture_id);	
		}
		else
		{

			redirect('soccer_management/commissioner/add_torna_fixture_result/'.$tournament_fixture_id);
		}
	}
	public function add_fixture_foul($fixture_id,$module = NULL)
	{
		//form validation
		$this->form_validation->set_rules('foul_player_id', 'Offender', 'required|xss_clean');
		$this->form_validation->set_rules('foul_type_id','Foul type', 'required|xss_clean');
		$this->form_validation->set_rules('foul_minute','Foul minute', 'required|xss_clean');
		$this->form_validation->set_rules('action_id','Action taken', 'required|xss_clean');
		
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			$foul_id = $this->fixture_model->add_fixture_fouls($fixture_id,$module);
			if($foul_id > 0)
			{
				$this->session->set_userdata("success_message", "Foul added successfully to the fixture");
			}
			
			else
			{
				$this->session->set_userdata("error_message","Could not add foul. Please try again ");
			}
		}
		else
		{
			$validation_errors = validation_errors();
			$this->session->set_userdata("error_message", $validation_errors);
		}	
		
		if(!empty($module))
		{

			redirect('soccer-management/commissioner-add-fixture-result/'.$fixture_id);	
		}
		else
		{

			redirect('soccer-management/referee-add-fixture-result/'.$fixture_id);
		}
	}

	public function edit_fixture_foul($fixture_id,$foul_id,$module = NULL)
	{
		//form validation
		$this->form_validation->set_rules('foul_player_id'.$foul_id, 'Offender', 'required|xss_clean');
		$this->form_validation->set_rules('foul_type_id'.$foul_id,'Foul type', 'required|xss_clean');
		$this->form_validation->set_rules('foul_minute'.$foul_id,'Foul minute', 'required|xss_clean');
		$this->form_validation->set_rules('action_id'.$foul_id,'Action taken', 'required|xss_clean');
		
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			if($this->fixture_model->edit_fixture_fouls($fixture_id,$foul_id,$module))
			{
				$this->session->set_userdata("success_message", "Foul edited successfully to the fixture");
			}
			
			else
			{
				$this->session->set_userdata("error_message","Could not edit foul. Please try again ");
			}
		}
		else
		{
			$validation_errors = validation_errors();
			$this->session->set_userdata("error_message", $validation_errors);
		}	
		
		if(!empty($module))
		{

			redirect('soccer-management/commissioner-add-fixture-result/'.$fixture_id);	
		}
		else
		{

			redirect('soccer-management/referee-add-fixture-result/'.$fixture_id);
		}
	}

	public function add_tournament_fixture_fouls($fixture_id,$module = NULL)
	{
		//form validation
		$this->form_validation->set_rules('foul_player_id', 'Offender', 'required|xss_clean');
		$this->form_validation->set_rules('foul_type_id','Foul type', 'required|xss_clean');
		$this->form_validation->set_rules('foul_minute','Foul minute', 'required|xss_clean');
		$this->form_validation->set_rules('action_id','Action taken', 'required|xss_clean');
		
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			$foul_id = $this->tournament_model->add_fixture_fouls($fixture_id,$module);
			if($foul_id > 0)
			{
				$this->session->set_userdata("success_message", "Foul added successfully to the fixture");
			}
			
			else
			{
				$this->session->set_userdata("error_message","Could not add foul. Please try again ");
			}
		}
		else
		{
			$validation_errors = validation_errors();
			$this->session->set_userdata("error_message", $validation_errors);
		}	
		if(empty($module))
		{
			redirect('soccer_management/referee/add_torna_fixture_result/'.$fixture_id); 
		}
		else
		{
			redirect('soccer_management/commissioner/add_torna_fixture_result/'.$fixture_id); 

		}
		
	}
	public function add_t_sub($tournament_fixture_id,$module=NULL)
	{

		//form validation
		$this->form_validation->set_rules('home_player_in', 'Home Player In ID', 'required|xss_clean');
		$this->form_validation->set_rules('home_player_out','Home Player Out ID', 'required|xss_clean');
		$this->form_validation->set_rules('home_minute',' Home Time', 'required|xss_clean');
		
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			if($this->fixture_model->add_t_subs($tournament_fixture_id,$module))
			{
				$this->session->set_userdata("success_message", "Subs added successfully to the tournament fixture");
				
			}
			
			else
			{
				$this->session->set_userdata("error_message","Could not add Subs. Please try again ");
			}
		}	
		// $v_data['team_details_home_in'] = $this->fixture_model->get_tournament_fixture_teams_details_home_in($tournament_fixture_id);
		// $v_data['team_details_home_out'] = $this->fixture_model->get_tournament_fixture_teams_details_home_out($tournament_fixture_id);
		// $v_data['team_details_away_in'] = $this->fixture_model->get_tournament_fixture_teams_details_away_in($tournament_fixture_id);
		// $v_data['team_details_away_out'] = $this->fixture_model->get_tournament_fixture_teams_details_away_out($tournament_fixture_id);

		// $v_data['title'] = 'Add Tournament Substitutions';
		// $v_data['title_away'] = 'Add Away Substitutes';
		// $v_data['title_home'] = 'Add Home Substitutes';
		// $v_data['tournament_fixture_id'] =  $tournament_fixture_id;
		// $data['content'] = $this->load->view('referee/t_subs', $v_data, true);
		// $this->load->view('admin/templates/general_page', $data);

		if(empty($module))
		{

			redirect('soccer_management/referee/add_torna_fixture_result/'.$tournament_fixture_id);	
		}
		else
		{

		redirect('soccer_management/commissioner/add_torna_fixture_result/'.$tournament_fixture_id);
		}
	}
	

	public function add_sub($fixture_id,$module = NULL)
	{

		//form validation
		$this->form_validation->set_rules('home_player_in', 'Home Player In ID', 'required|xss_clean');
		$this->form_validation->set_rules('home_player_out','Home Player Out ID', 'required|xss_clean');
		$this->form_validation->set_rules('home_minute',' Home Time', 'required|xss_clean');
		
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			if($this->fixture_model->add_subs($fixture_id,$module))
			{
				$this->session->set_userdata("success_message", "Subs added successfully to the fixture");
			}
			
			else
			{
				$this->session->set_userdata("error_message","Could not add Subs. Please try again ");
			}
		}	
			
		// $v_data['team_details'] = $this->fixture_model->get_fixture_teams_details($fixture_id);
		// $v_data['team_details_home_in'] = $this->fixture_model->get_fixture_teams_details_home_in($fixture_id);
		// $v_data['team_details_home_out'] = $this->fixture_model->get_fixture_teams_details_home_out($fixture_id);
		// $v_data['team_details_away_in'] = $this->fixture_model->get_fixture_teams_details_away_in($fixture_id);
		// $v_data['team_details_away_out'] = $this->fixture_model->get_fixture_teams_details_away_out($fixture_id);

		// $v_data['title'] = 'Add Substitutes';
		// $v_data['title_away'] = 'Add Away Substitutes';
		// $v_data['title_home'] = 'Add Home Substitutes';
		// $v_data['fixture_id'] = $fixture_id;
		// $data['content'] = $this->load->view('referee/subs', $v_data, true);
		// $this->load->view('admin/templates/general_page', $data);
		if(empty($module))
		{
			redirect('soccer-management/referee-add-fixture-result/'.$fixture_id);

		}
		else
		{
			redirect('soccer-management/commissioner-add-fixture-result/'.$fixture_id);	
		}
	}

	public function edit_sub($fixture_id,$home_sub_id,$module = NULL)
	{

		//form validation
		$this->form_validation->set_rules('home_player_in'.$home_sub_id, 'Home Player In ID', 'required|xss_clean');
		$this->form_validation->set_rules('home_player_out'.$home_sub_id,'Home Player Out ID', 'required|xss_clean');
		$this->form_validation->set_rules('home_minute'.$home_sub_id,' Home Time', 'required|xss_clean');
		
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			if($this->fixture_model->edit_subs($fixture_id,$home_sub_id,$module))
			{
				$this->session->set_userdata("success_message", "Subs edited successfully to the fixture");
			}
			
			else
			{
				$this->session->set_userdata("error_message","Could not edit Subs. Please try again ");
			}
		}	
		if(empty($module))
		{
			redirect('soccer-management/referee-add-fixture-result/'.$fixture_id);

		}
		else
		{
			redirect('soccer-management/commissioner-add-fixture-result/'.$fixture_id);	
		}
	}


	public function away_substitute($fixture_id,$module = NULL)
	{
		//form validation
		$this->form_validation->set_rules('away_player_in', 'Away Player In ID', 'required|xss_clean');
		$this->form_validation->set_rules('away_player_out','Away Player Out ID', 'required|xss_clean');
		$this->form_validation->set_rules('away_minute',' Away Substitution Time', 'required|xss_clean');
		
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			if($this->fixture_model->add_away_subs($fixture_id,$module))
			{
				$this->session->set_userdata("success_message", "Subs added successfully to the fixture");
				
			}
			
			else
			{
				$this->session->set_userdata("error_message","Could not add Subs. Please try again ");
			}
		}
		else
		{
			$validation_errors = validation_errors();
			$this->session->set_userdata("error_message",$validation_errors);
		}
		if(empty($module))
		{
			redirect('soccer-management/referee-add-fixture-result/'.$fixture_id);

		}
		else
		{
			redirect('soccer-management/commissioner-add-fixture-result/'.$fixture_id);	
		}
	}
	public function edit_away_substitute($fixture_id,$away_sub_id,$module = NULL)
	{
		//form validation
		$this->form_validation->set_rules('away_player_in'.$away_sub_id, 'Away Player In ID', 'required|xss_clean');
		$this->form_validation->set_rules('away_player_out'.$away_sub_id,'Away Player Out ID', 'required|xss_clean');
		$this->form_validation->set_rules('away_minute'.$away_sub_id,' Away Substitution Time', 'required|xss_clean');
		
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			if($this->fixture_model->edit_away_subs($fixture_id,$away_sub_id,$module))
			{
				$this->session->set_userdata("success_message", "Subs edited successfully to the fixture");
				
			}
			
			else
			{
				$this->session->set_userdata("error_message","Could not edited Subs. Please try again ");
			}
		}
		else
		{
			$validation_errors = validation_errors();
			$this->session->set_userdata("error_message",$validation_errors);
		}
		if(empty($module))
		{
			redirect('soccer-management/referee-add-fixture-result/'.$fixture_id);

		}
		else
		{
			redirect('soccer-management/commissioner-add-fixture-result/'.$fixture_id);	
		}
	}
	public function away_torna_substitute($fixture_id,$module = NULL)
	{
		//form validation
		$this->form_validation->set_rules('away_player_in', 'Away Player In ID', 'required|xss_clean');
		$this->form_validation->set_rules('away_player_out','Away Player Out ID', 'required|xss_clean');
		$this->form_validation->set_rules('away_minute',' Away Substitution Time', 'required|xss_clean');
		
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			if($this->fixture_model->add_away_tsubs($fixture_id,$module))
			{
				$this->session->set_userdata("success_message", "Subs added successfully to the tournament fixture");
				
			}
			
			else
			{
				$this->session->set_userdata("error_message","Could not add Subs. Please try again ");
			}
		}
		else
		{
			$validation_errors = validation_errors();
			$this->session->set_userdata("error_message",$validation_errors);
		}
		if(empty($module))
		{
			redirect('soccer_management/referee/add_torna_fixture_result/'.$fixture_id); 
		}
		else
		{
			redirect('soccer_management/commissioner/add_torna_fixture_result/'.$fixture_id); 

		}
	}
	public function referee_leagues()
	{

		$referee_id = $this->session->userdata('personnel_id');
		$v_data['referee_details'] = $this->referee_model->get_referee($referee_id);

		$where = 'referee_id = '.$referee_id;
		$table = 'fixture_referee';
		$referee_search = $this->session->userdata('referee_search2');
		
		if(!empty($referee_search))
		{
			$where .= $referee_search;
		}
		//pagination
		$segment = 3;
		$this->load->library('pagination');
		$config['base_url'] = site_url().'referee/league';
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->referee_model->get_all_referee($table, $where, $config["per_page"], $page, $order='fixture_referee_id', $order_method='ASC');
		
		$data['title'] = 'Referee';
		$v_data['title'] = $data['title'];
		
		$v_data['league_referee_games'] = $query;
		$v_data['teams'] = $this->team_model->all_teams();
		$v_data['page'] = $page;
		$data['content'] = $this->load->view('referee/all_referee_leagues', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}

	public function add_fixture_player($team_id,$fixture_team_id, $league_duration_id,$league_id,$fixture_id)
	{
		//form validation rules
		$this->form_validation->set_rules('player_id', 'Player', 'required|xss_clean');
		$this->form_validation->set_rules('fixture_player_type_id','Fixture Player Type', 'required|xss_clean');
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			$fixture_player_id = $this->fixture_model->add_fixture_player($fixture_team_id);
			if($fixture_player_id > 0)
			{
				$this->session->set_userdata("success_message", "Player added successfully to the fixture");

				redirect('referee/add-fixture-player/'.$team_id.'/'.$fixture_team_id.'/'.$league_duration_id.'/'.$league_id.'/'.$fixture_id);    
			}
			
			else
			{
				$this->session->set_userdata("error_message","Could not add player. Please try again ");
			}
		}
		$v_data['fixture_players'] = $this->league_model->get_all_fixture_players($team_id);
		$v_data['team_name'] = $this->league_model->get_team_name($team_id);
		$v_data['all_fixture_players'] = $this->fixture_model->get_all_fixture_players($fixture_team_id);
		$v_data['player_type'] = $this->fixture_model->get_all_fixture_player_types();
		$data['title'] = 'Add players to team';
		$v_data['title'] = $data['title'];
		$v_data['league_id'] = $league_id;
		$v_data['fixture_id'] = $fixture_id;
		$v_data['team_id'] = $team_id;
		$v_data['fixture_team_id'] = $fixture_team_id;
		$v_data['league_duration_id'] = $league_duration_id;
		$data['content'] = $this->load->view('referee/add_fixture_player', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
		
	}
	public function get_team_players($team_id,$fixture_id)
	{
		echo '<option value="0">--Select Player--</option>';
		
		$team_player = $this->referee_model->get_players_of_team($team_id,$fixture_id);
		foreach($team_player->result() AS $key) 
		{ 
			echo '<option value="'.$key->fixture_team_player_id.'">'.$key->player_fname.' '.$key->player_onames.'</option>';
		}
	}

	public function get_team_tournament_players($team_id,$fixture_id)
	{
		echo '<option value="0">--Select Player--</option>';
		
		$team_player = $this->referee_model->get_tournament_players_of_team($team_id,$fixture_id);
		foreach($team_player->result() AS $key) 
		{ 
			echo '<option value="'.$key->tournament_fixture_team_player_id.'">'.$key->player_fname.' '.$key->player_onames.'</option>';
		}
	}

	public function get_team_league_substitutions($team_id,$fixture_id)
	{
		$v_data['team_details'] = $this->fixture_model->get_fixture_teams_details($fixture_id);
		$v_data['team_details_home_in'] = $this->fixture_model->get_fixture_teams_details_home_in_team($fixture_id,$team_id);
		$v_data['team_details_home_out'] = $this->fixture_model->get_fixture_teams_details_home_out_team($fixture_id,$team_id);
		$v_data['team_details_away_in'] = $this->fixture_model->get_fixture_teams_details_away_in($fixture_id);
		$v_data['team_details_away_out'] = $this->fixture_model->get_fixture_teams_details_away_out($fixture_id);

	}
	public function referee_tournaments()
	{
		$referee_id = $this->session->userdata('personnel_id');
		$v_data['referee_details'] = $this->referee_model->get_referee($referee_id);

		$where = 'referee_id = '.$referee_id;
		$table = 'tournament_fixture_referee';
		$referee_search = $this->session->userdata('referee_search2');
		
		if(!empty($referee_search))
		{
			$where .= $referee_search;
		}
		//pagination
		$segment = 3;
		$this->load->library('pagination');
		$config['base_url'] = site_url().'referee/tournament';
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->referee_model->get_all_referee($table, $where, $config["per_page"], $page, $order='referee_id', $order_method='ASC');
		
		$data['title'] = 'Referee';
		$v_data['title'] = $data['title'];
		
		$v_data['tournament_referee_games'] = $query;
		$v_data['teams'] = $this->team_model->all_teams();
		$v_data['page'] = $page;
		$data['content'] = $this->load->view('referee/all_referee_tournaments', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}
	public function venue()
	{
		$where = 'venue_id > 0 AND venue_deleted = 0';
		$table = 'venue';
		$v_data['title'] = $data['title'] = 'All Venues';
		$v_data['query'] = $this->referee_model->get_all_venue($where,  $table);
		$data['content'] = $this->load->view('referee/all_venues', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}
	public function add_venue()
	{
		//form validation rules
		$this->form_validation->set_rules('venue_name', 'Venue Name', 'required|xss_clean');
		
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			$league_id = $this->referee_model->add_venue();
			if($league_id > 0)
			{
				$this->session->set_userdata("success_message", "Venue added successfully");
				redirect('soccer-management/venues/');
			}
			
			else
			{
				$this->session->set_userdata("error_message","Could not add venue. Please try again ");
			}
		}
		$v_data['title'] = $data['title'] = 'Add Venue';
		
		$data['content'] = $this->load->view('referee/add_venue', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}
	public function activate_venue($venue_id)
	{
		$this->referee_model->activate_venue($venue_id);
		$this->session->set_userdata('success_message', 'Venue activated successfully');
		redirect('soccer-management/venues');
	}
	public function deactivate_venue($venue_id)
	{
		$this->referee_model->deactivate_venue($venue_id);
		$this->session->set_userdata('success_message', 'Venue disabled successfully');
		redirect('soccer-management/venues');
	}
	public function delete_venue($venue_id)
	{
		if($this->referee_model->delete_venue($venue_id))
		{
			$this->session->set_userdata('success_message', 'Venue has been deleted');
		}
		
		else
		{
			$this->session->set_userdata('error_message', 'Venue could not deleted');
		}
		redirect('soccer-management/venues');
	}
	public function edit_venue($venue_id)
	{
    	$this->form_validation->set_rules('venue_name', 'Venue Names', 'required|xss_clean');
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			if($this->referee_model->edit_venue($venue_id))
			{
				$this->session->set_userdata('success_message', 'venue\'s general details updated successfully');
				redirect('soccer-management/venues');
			}
			
			else
			{
				$this->session->set_userdata('error_message', 'Could not update venue\'s general details. Please try again');
			}
		}
		else
		{
			$this->session->set_userdata("error_message",validation_errors());
		}

		$v_data['venue'] = $this->referee_model->get_venue($venue_id);
		$v_data['venue_id'] = $venue_id;
		$v_data['title'] = 'Edit Venue';
		$data['content'] = $this->load->view('referee/edit_venue', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}

	public function referee_manage_tournament_result($fixture_id)
	{
		// var_dump($fixture_id); die();
		$league_duration_id = $this->referee_model->get_tournament_duration($fixture_id);
		$tournament_id = $this->referee_model->get_tournament($league_duration_id);
		$league_name = $this->referee_model->get_tournament_name($tournament_id);
		$v_data['season_teams'] = $this->league_model->get_season_tournament_fixture_teams($league_duration_id,$fixture_id);
		$v_data['title'] = 'Manage Fixture '.$fixture_id.' season '.$league_duration_id.' in '.$league_name.' league';
		$v_data['tournament_id'] = $tournament_id;
		$v_data['comment_type'] = $this->fixture_model->get_all_comment_types(); 
		$v_data['home_team'] = $this->tournament_model->get_home_team($fixture_id);
		$v_data['away_team'] = $this->tournament_model->get_away_team($fixture_id);
		$v_data['fixture_detail'] = $this->tournament_model->get_fixture_detail($fixture_id);
		$v_data['player_type'] = $this->fixture_model->get_all_fixture_player_types();
		$v_data['team_types'] = $this->fixture_model->get_fixture_team_type();
		$v_data['tournament_fixture_teams'] = $this->tournament_model->get_fixture_teams($fixture_id);
		$v_data['league_name'] = $league_name;
		$v_data['tournament_fixture_id'] = $fixture_id;
		$v_data['league_duration_id'] = $league_duration_id;
		$data['content'] = $this->load->view('referee/manage_tournament_fixture', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}
	public function delete_league_fixture_summary($fixture_summary_id, $fixture_id)
	{
		if($this->fixture_model->delete_league_fixture_summary($fixture_summary_id))
		{
			$this->session->set_userdata("success_message", "Summary deleted succesfully");
		}
		else
		{
			$this->session->set_userdata("error_message", "Summary could not be deleted");
		}
		redirect('soccer-management/referee-add-fixture-result/'.$fixture_id);
	}
	public function delete_league_subs($sub_id, $fixture_id)
	{
		if($this->fixture_model->delete_league_subs($sub_id))
		{
			$this->session->set_userdata("success_message", "SUb deleted succesfully");
		}
		else
		{
			$this->session->set_userdata("error_message", "sub could not be deleted");
		}
		redirect('soccer-management/referee-add-fixture-result/'.$fixture_id);
	}
	public function delete_league_foul($foul_id, $fixture_id)
	{
		if($this->fixture_model->delete_league_foul($foul_id))
		{
			$this->session->set_userdata("success_message", "Foul deleted succesfully");
		}
		else
		{
			$this->session->set_userdata("error_message", "foul could not be deleted");
		}
		redirect('soccer-management/referee-add-fixture-result/'.$fixture_id);
	}
	public function delete_league_goal($goal_id, $fixture_id)
	{
		if($this->fixture_model->delete_league_goal($goal_id))
		{
			$this->session->set_userdata("success_message", "Goal deleted succesfully");
		}
		else
		{
			$this->session->set_userdata("error_message", "Goal could not be deleted");
		}
		redirect('soccer-management/referee-add-fixture-result/'.$fixture_id);
	}
	public function delete_fixture_player($player_fixture_id,$team_id,$fixture_team_id,$league_duration_id,$league_id,$fixture_id)
	{
		if($this->referee_model->delete_fixture_player($player_fixture_id))
		{
			$this->session->set_userdata("success_message", "Player deleted succesfully");
		}
		else
		{
			$this->session->set_userdata("error_message", "Player could not be deleted");
		}
		redirect('referee/add-fixture-player/'.$team_id.'/'.$fixture_team_id.'/'.$league_duration_id.'/'.$league_id.'/'.$fixture_id);
	}
}
?>