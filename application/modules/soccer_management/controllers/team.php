<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once "./application/modules/soccer_management/controllers/soccer_management.php";

class Team extends soccer_management 
{
	var $team_logo_path;
	var $team_logo_location;
	var $csv_path;
	function __construct()
	{
		parent:: __construct();
		$this->team_logo_path = realpath(APPPATH . '../assets/team_logo');
		$this->team_logo_location = base_url().'assets/team_logo/';
		$this->csv_path = realpath(APPPATH . '../assets/csv');
	}
	
	//show all teames
	public function index($order = 'team.team_id', $order_method = 'ASC') 
	{
		$table = '';
		$where = 'team.team_id > 0 AND team.team_deleted = 0';
		$table .= 'team';
		$team_search = $this->session->userdata('team_search2');
		
		if(!empty($team_search))
		{
			$where .= $team_search;
			$table .=',team_coach';
		}
		//pagination
		$segment = 5;
		$this->load->library('pagination');
		$config['base_url'] = site_url().'soccer-management/team/'.$order.'/'.$order_method;
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->team_model->get_all_team($table, $where, $config["per_page"], $page, $order, $order_method);
		
		//change of order method 
		if($order_method == 'DESC')
		{
			$order_method = 'ASC';
		}
		
		else
		{
			$order_method = 'DESC';
		}
		
		$data['title'] = 'Team';
		$v_data['title'] = $data['title'];
		
		$v_data['order'] = $order;
		$v_data['order_method'] = $order_method;
		$v_data['query'] = $query;
		$v_data['coaches'] = $this->coach_model->all_coach();
		$v_data['page'] = $page;
		$data['content'] = $this->load->view('team/all_team', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}
	public function close_search()
	{
		$this->session->unset_userdata('team_search2', $search);
		$this->session->unset_userdata('team_search_title2', $search_title);
		$youth_teams = $this->session->userdata('youth_teams');
		if(!empty($youth_teams))
		{
			redirect('soccer-management/youth-teams');
		}
		else
		{
			redirect('soccer-management/team');
		}
	}
	public function search_team()
	{
		$team_number = $this->input->post('team_number');
		$coach_id = $this->input->post('coach_id');
		$search_title = '';
		if(!empty($team_number))
		{
			$search_title .= ' team number <strong>'.$team_number.'</strong>';
			$team_number = ' AND team.team_number = \''.$team_number.'\'';
		}
		
		if(!empty($coach_id))
		{
			$search_title .= ' coach id <strong>'.$coach_id.'</strong>';
			$coach_id = ' AND team.team_id = team_coach.team_id AND team_coach.coach_id = '.$coach_id;
		}
		
		//search venue
		if(!empty($_POST['team_venue']))
		{
			$search_title .= ' venue name <strong>'.$_POST['team_venue'].'</strong>';
			$venues = explode(" ",$_POST['team_venue']);
			$total = count($venues);
			
			$count = 1;
			$venue = ' AND (';
			for($r = 0; $r < $total; $r++)
			{
				if($count == $total)
				{
					$venue .= ' team.team_venue LIKE \'%'.mysql_real_escape_string($venues[$r]).'%\'';
				}
				
				else
				{
					$venue .= ' team.team_venue LIKE \'%'.mysql_real_escape_string($venues[$r]).'%\' AND ';
				}
				$count++;
			}
			$venue .= ') ';
		}
		
		else
		{
			$venue = '';
		}
		//team name
		if(!empty($_POST['team_name']))
		{
			$search_title .= ' team name <strong>'.$_POST['team_name'].'</strong>';
			$names = explode(" ",$_POST['team_name']);
			$total = count($names);
			
			$count = 1;
			$teamname = ' AND (';
			for($r = 0; $r < $total; $r++)
			{
				if($count == $total)
				{
					$teamname .= ' team.team_venue LIKE \'%'.mysql_real_escape_string($names[$r]).'%\'';
				}
				
				else
				{
					$teamname .= ' team.team_venue LIKE \'%'.mysql_real_escape_string($names[$r]).'%\' AND ';
				}
				$count++;
			}
			$teamname .= ') ';
		}
		
		else
		{
			$teamname = '';
		}

		
		$search = $team_number.$coach_id.$venue.$teamname;
		$this->session->set_userdata('team_search2', $search);
		$this->session->set_userdata('team_search_title2', $search_title);
		
		$youth_teams = $this->session->userdata('youth_teams');
		if(!empty($youth_teams))
		{
			redirect('soccer-management/youth-teams');
		}
		else
		{
			$this->index();
		}
	}
	
	public function add_team() 
	{
		$v_data['team_logo_location'] = 'http://placehold.it/500x500';
		$this->session->unset_userdata('logo_error_message');
		//upload image if it has been selected
		$response = $this->team_model->upload_team_logo($this->team_logo_path);
		if($response)
		{
			$v_data['team_logo_location'] = $this->team_logo_location.$this->session->userdata('team_logo_file_name');
		}
		
		//case of upload error
		else
		{
			$v_data['logo_error'] = $this->session->userdata('logo_error_message');
		}
		
		//form validation rules
		$this->form_validation->set_rules('team_name', 'Team Names', 'required|xss_clean');
		$this->form_validation->set_rules('team_venue', 'Team venue', 'required|xss_clean');
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			if(empty($upload_error))
			{
				$data = array(
					'team_name'=>ucwords(strtolower($this->input->post('team_name'))),
					'team_logo'=>$this->session->userdata('team_logo_name'),
					'team_venue'=>ucwords(strtolower($this->input->post('team_venue'))),
					'created_by'=>$this->session->userdata('personnel_id'),
					'modified_by'=>$this->session->userdata('personnel_id'),
					'created' =>date('Y-m-d H:i:s'),
				);
				
				if($this->db->insert('team', $data))
				{
					$this->session->set_userdata('success_message', 'team added successfully');
					redirect('soccer-management/team/');
				}
				else{
					$this->session->set_userdata('success_message', 'team not added');
					redirect('soccer-management/add-team/');
				}
			}
		}
		$v_data['team_logo_name'] = $this->session->userdata('team_logo_name');
		$v_data['team_logo_location'] = $this->team_logo_location;
		$v_data['teams'] = $this->team_model->all_teams();
		$v_data['titles'] = $this->personnel_model->get_title();
		$v_data['civil_statuses'] = $this->personnel_model->get_civil_status();
		$v_data['genders'] = $this->personnel_model->get_gender();
		$data['title'] = 'Add team';
		$v_data['title'] = $data['title'];
		$data['content'] = $this->load->view('team/add_team', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}

	public function edit_team($team_id)
    {
    	$this->form_validation->set_rules('team_name', 'Team Names', 'required|xss_clean');
		
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			if($this->team_model->edit_team($team_id))
			{
				$this->session->set_userdata('success_message', 'team\'s general details updated successfully');
				$youth_teams = $this->session->userdata('youth_teams');
				if(!empty($youth_teams))
				{
					redirect('soccer-management/youth-teams');
				}
				else
				{
					redirect('soccer-management/team');
				}
			}
			
			else
			{
				$this->session->set_userdata('error_message', 'Could not update team\'s general details. Please try again');
			}
		}
		else
		{
			$this->session->set_userdata("error_message","Could not add team. Please try again");
		}

		$v_data['team'] = $this->team_model->get_team($team_id);
		$v_data['team_id'] = $team_id;
		$v_data['teams'] = $this->team_model->all_teams();
		$v_data['title'] = 'Edit Team';
		$data['content'] = $this->load->view('team/edit_team', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
		
    }
	public function activate_team($team_id)
	{
		$this->team_model->activate_team($team_id);
		$this->session->set_userdata('success_message', 'Team activated successfully');
		$youth_teams = $this->session->userdata('youth_teams');
		if(!empty($youth_teams))
		{
			redirect('soccer-management/youth-teams');
		}
		else
		{
			redirect('soccer-management/team');
		}
	}
	public function deactivate_team($team_id)
	{
		$this->team_model->deactivate_team($team_id);
		$this->session->set_userdata('success_message', 'Team disabled successfully');
		$youth_teams = $this->session->userdata('youth_teams');
		if(!empty($youth_teams))
		{
			redirect('soccer-management/youth-teams');
		}
		else
		{
			redirect('soccer-management/team');
		}
	}
	public function delete_team($team_id)
	{
		if($this->team_model->delete_team($team_id))
		{
			$this->session->set_userdata('success_message', 'Team has been deleted');
		}
		
		else
		{
			$this->session->set_userdata('error_message', 'Team could not deleted');
		}
		$youth_teams = $this->session->userdata('youth_teams');
		if(!empty($youth_teams))
		{
			redirect('soccer-management/youth-teams');
		}
		else
		{
			redirect('soccer-management/team');
		}
	}
	public function import_teams()
	{
		$v_data['title'] = $data['title'] = $this->site_model->display_page_title();
		$data['content'] = $this->load->view('team/import_team', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}
	
	public function team_template()
	{
		$this->team_model->team_template();
	}
	
	public function do_team_import()
	{
		if(isset($_FILES['import_csv']))
		{
			if(is_uploaded_file($_FILES['import_csv']['tmp_name']))
			{
				//import products from excel 
				$response = $this->team_model->import_csv_teams($this->csv_path);
				
				if($response == FALSE)
				{
					$v_data['import_response_error'] = 'Something went wrong. Please try again.';
				}
				
				else
				{
					if($response['check'])
					{
						$v_data['import_response'] = $response['response'];
					}
					
					else
					{
						$v_data['import_response_error'] = $response['response'];
					}
				}
			}
			
			else
			{
				$v_data['import_response_error'] = 'Please select a file to import.';
			}
		}
		
		else
		{
			$v_data['import_response_error'] = 'Please select a file to import.';
		}
		
		$v_data['title'] = $data['title'] = $this->site_model->display_page_title();
		$data['content'] = $this->load->view('team/import_team', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	
	}
}
?>