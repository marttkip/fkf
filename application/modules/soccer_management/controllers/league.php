<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once "./application/modules/soccer_management/controllers/soccer_management.php";

class League extends soccer_management 
{
	function __construct()
	{
		parent:: __construct();
	}
	
	//show all leaguees
	public function index($order = 'league_id', $order_method = 'ASC') 
	{
		$where = 'league_id > 0 AND league_deleted = 0 AND league_type = 1';
		$table = 'league';
		$league_search = $this->session->userdata('league_search2');
		
		if(!empty($league_search))
		{
			$where .= $league_search;
		}
		//pagination
		$segment = 5;
		$this->load->library('pagination');
		$config['base_url'] = site_url().'soccer-management/league/'.$order.'/'.$order_method;
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->league_model->get_all_league($table, $where, $config["per_page"], $page, $order, $order_method);
		
		//change of order method 
		if($order_method == 'DESC')
		{
			$order_method = 'ASC';
		}
		
		else
		{
			$order_method = 'DESC';
		}
		
		$data['title'] = 'League';
		$v_data['title'] = $data['title'];
		
		$v_data['order'] = $order;
		$v_data['order_method'] = $order_method;
		$v_data['query'] = $query;
		$v_data['teams'] = $this->team_model->all_teams();
		$v_data['page'] = $page;
		$data['content'] = $this->load->view('league/all_league', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}
	public function close_search()
	{
		$this->session->unset_userdata('league_search2', $search);
		$this->session->unset_userdata('league_search_title2', $search_title);
		
		redirect('soccer-management/league');
	}
	public function search_league()
	{
		$search_title = '';
		//search league name
		if(!empty($_POST['league_name']))
		{
			$search_title .= ' league names <strong>'.$_POST['league_name'].'</strong>';
			$league_names = explode(" ",$_POST['league_name']);
			$total = count($league_names);
			
			$count = 1;
			$league_name = ' AND (';
			for($r = 0; $r < $total; $r++)
			{
				if($count == $total)
				{
					$league_name .= ' league.league_name LIKE \'%'.mysql_real_escape_string($league_names[$r]).'%\'';
				}
				
				else
				{
					$league_name .= ' league.league_name LIKE \'%'.mysql_real_escape_string($league_names[$r]).'%\' AND ';
				}
				$count++;
			}
			$league_name .= ') ';
		}
		
		else
		{
			$league_name = '';
		}
		
		$search =$league_name;
		$this->session->set_userdata('league_search2', $search);
		$this->session->set_userdata('league_search_title2', $search_title);
		
		$this->index();
	}
	
	public function add_league() 
	{
		//form validation rules
		$this->form_validation->set_rules('league_name', 'League Names', 'required|xss_clean');
		
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			$league_id = $this->league_model->add_league();
			if($league_id > 0)
			{
				$this->session->set_userdata("success_message", "League added successfully");
				redirect('soccer-management/league/');
			}
			
			else
			{
				$this->session->set_userdata("error_message","Could not add league. Please try again ".$league_id);
			}
		}
		$data['title'] = 'Add league';
		$v_data['title'] = $data['title'];
		$data['content'] = $this->load->view('league/add_league', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}

	public function edit_league($league_id)
    {
    	$this->form_validation->set_rules('league_name', 'League Names', 'required|xss_clean');
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			if($this->league_model->edit_league($league_id))
			{
				$this->session->set_userdata('success_message', 'league\'s general details updated successfully');
				redirect('soccer-management/league/');
			}
			
			else
			{
				$this->session->set_userdata('error_message', 'Could not update league\'s general details. Please try again');
			}
		}
		else
		{
			$this->session->set_userdata("error_message","Could not add league. Please try again");
		}

		$v_data['league'] = $this->league_model->get_league($league_id);
		$v_data['league_id'] = $league_id;
		$v_data['title'] = 'Edit League';
		$data['content'] = $this->load->view('league/edit_league', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
		
    }
	public function activate_league($league_id)
	{
		$this->league_model->activate_league($league_id);
		$this->session->set_userdata('success_message', 'League activated successfully');
		redirect('soccer-management/league');
	}
	public function deactivate_league($league_id)
	{
		$this->league_model->deactivate_league($league_id);
		$this->session->set_userdata('success_message', 'League disabled successfully');
		redirect('soccer-management/league');
	}
	public function delete_league($league_id)
	{
		if($this->league_model->delete_league($league_id))
		{
			$this->session->set_userdata('success_message', 'League has been deleted');
		}
		
		else
		{
			$this->session->set_userdata('error_message', 'League could not deleted');
		}
		redirect('soccer-management/league');
	}
	public function add_league_duration($league_id)
	{
		//form validation rules
		$this->form_validation->set_rules('league_duration_end_date', 'End date', 'required|xss_clean');
		$this->form_validation->set_rules('league_duration_start_date', 'Start date', 'required|xss_clean');
		
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			$league_duration_id = $this->league_model->add_league_duration($league_id);
			if($league_duration_id > 0)
			{
				$this->session->set_userdata("success_message", "League duration added successfully");
				redirect('soccer-management/add-league-duration/'.$league_id);
			}
			
			else
			{
				$this->session->set_userdata("error_message","Could not add league duration. Please try again ".$league_id);
			}
		}
		$data['title'] = 'Add league duartion';
		$v_data['title'] = $data['title'];
		$v_data['league_id'] = $league_id;
		$v_data['league_name'] = $this->league_model->get_league_name($league_id);
		$v_data['league_durations'] = $this->league_model->get_all_league_durations($league_id);
		$data['content'] = $this->load->view('league/add_league_duration', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}
	public function edit_league_duration($league_duration_id, $league_id)
	{
		$this->form_validation->set_rules('league_duration_start_date', 'Start Date', 'xss_clean');
    	$this->form_validation->set_rules('league_duration_end_date', 'End Date', 'required|xss_clean');
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			$league_type = $this->soccer_management_model->get_league_type($league_id);
			if($this->league_model->edit_league_duration($league_duration_id))
			{
				$this->session->set_userdata('success_message', 'season details updated successfully');
				if($league_type ==1)
				{
					redirect('soccer-management/add-league-duration/'.$league_id);
				}
				elseif($league_type ==2)
				{
					redirect('soccer-management/add-youth-league-duration/'.$league_id);
				}	
			}
			
			else
			{
				$this->session->set_userdata('error_message', 'Could not update season details. Please try again');
			}
		}
		$v_data['league_duration'] = $this->league_model->get_league_duration($league_duration_id);
		$v_data['league_name'] = $this->league_model->get_league_name($league_id);
		$v_data['title'] = 'Edit Season';
		$v_data['league_duration_id'] = $league_duration_id;
		$data['content'] = $this->load->view('league/edit_league_duration', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}
	public function activate_league_duration($league_duration_id,$league_id)
	{
		$league_type = $this->soccer_management_model->get_league_type($league_id);
		
		$this->league_model->activate_league_duration($league_duration_id);
		$this->session->set_userdata('success_message', 'Season activated successfully');
		
		if($league_type ==1)
		{
			redirect('soccer-management/add-league-duration/'.$league_id);
		}
		elseif($league_type ==2)
		{
			redirect('soccer-management/add-youth-league-duration/'.$league_id);
		}
	}
	public function deactivate_league_duration($league_duration_id,$league_id)
	{
		$league_type = $this->soccer_management_model->get_league_type($league_id);
		
		$this->league_model->deactivate_league_duration($league_duration_id);
		$this->session->set_userdata('success_message', 'Season disabled successfully');
		
		if($league_type ==1)
		{
			redirect('soccer-management/add-league-duration/'.$league_id);
		}
		elseif($league_type ==2)
		{
			redirect('soccer-management/add-youth-league-duration/'.$league_id);
		}
	}
	public function delete_league_duration($league_duration_id,$league_id)
	{
		$league_type = $this->soccer_management_model->get_league_type($league_id);
		
		if($this->league_model->delete_league_duration($league_duration_id))
		{
			$this->session->set_userdata('success_message', 'Season has been deleted');
		}
		
		else
		{
			$this->session->set_userdata('error_message', 'Season could not deleted');
		}
		if($league_type ==1)
		{
			redirect('soccer-management/add-league-duration/'.$league_id);
		}
		elseif($league_type ==2)
		{
			redirect('soccer-management/add-youth-league-duration/'.$league_id);
		}
	}
	public function add_league_duration_team($league_duration_id,$league_id)
	{
		//form validation rules
		$this->form_validation->set_rules('team_id', 'Team', 'required|xss_clean');
		
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			$league_team_id = $this->league_model->add_league_duration_team($league_duration_id);
			if($league_team_id > 0)
			{
				$this->session->set_userdata("success_message", "Team added successfully");
				redirect('soccer-management/add-league-duration/'.$league_id);
			}
			
			else
			{
				$this->session->set_userdata("error_message","Could not add team. Please try again ");
			}
		}
		$league_name = $this->league_model->get_league_name($league_id);
		$data['title'] = 'Add Team to '.$league_name.' season '.$league_duration_id;
		$v_data['title'] = $data['title'];
		$v_data['league_id'] = $league_id;
		$v_data['league_duration_id'] = $league_duration_id;
		$v_data['league_teams'] = $this->league_model->get_league_teams($league_duration_id);
		$v_data['teams'] = $this->league_model->all_league_teams($league_duration_id);
		$v_data['league_name'] = $league_name; 
		$data['content'] = $this->load->view('league/add_league_team', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}
	public function remove_league_team($team_id,$league_id, $league_duration_id)
	{
		if($this->league_model->remove_league_team($team_id,$league_duration_id))
		{
			$this->session->set_userdata('success_message', 'Team removed');
		}
		
		else
		{
			$this->session->set_userdata('error_message', 'Team could not removed');
		}
		redirect('soccer-management/add-league-duration-team/'.$league_duration_id.'/'.$league_id);
	}
	public function add_league_duration_fixture($league_duration_id,$league_id)
	{
		//form validation rules
		$this->form_validation->set_rules('fixture_date', 'Date', 'required|xss_clean');
		$this->form_validation->set_rules('fixture_time', 'Time', 'required|xss_clean');
		$this->form_validation->set_rules('venue_id', 'Venue', 'required|xss_clean');
		
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			$fixture_id = $this->fixture_model->add_fixture($league_duration_id);
			if($fixture_id > 0)
			{
				$this->session->set_userdata("success_message", "Fixture added successfully");
				redirect('soccer-management/add-league-duration/'.$league_id);
			}
			
			else
			{
				$this->session->set_userdata("error_message","Could not add fixture. Please try again ");
			}
		}
		$league_name = $this->league_model->get_league_name($league_id);
		$v_data['league_fixtures'] = $this->fixture_model->get_season_fixtures($league_duration_id);
		$data['title'] = 'Add fixture to '.$league_name.' season '.$league_duration_id;
		$v_data['title'] = $data['title'];
		$v_data['league_id'] = $league_id;
		$v_data['league_duration_id'] = $league_duration_id;
		$v_data['venues'] = $this->fixture_model->get_all_venues();
		$data['content'] = $this->load->view('fixture/add_fixture', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}
	public function activate_league_duration_fixture($fixture_id, $league_id)
	{
		$this->fixture_model->activate_league_duration_fixture($fixture_id);
		$this->session->set_userdata('success_message', 'Fixture activated successfully');
		redirect('soccer-management/add-league-duration/'.$league_id);
	}
	public function deactivate_league_duration_fixture($fixture_id, $league_id)
	{
		$this->fixture_model->deactivate_league_duration_fixture($fixture_id);
		$this->session->set_userdata('success_message', 'Fixture deactivated successfully');
		redirect('soccer-management/add-league-duration/'.$league_id);
	}
	public function add_team_fixture($fixture_id, $league_duration_id,$league_id)
	{
		//form validation rules
		$this->form_validation->set_rules('league_team_id', 'Team', 'required|xss_clean');
		$this->form_validation->set_rules('fixture_team_type_id', 'Team Type', 'required|xss_clean');
		
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			$leage_fixture_id = $this->fixture_model->add_team_fixture($fixture_id);
			if($leage_fixture_id > 0)
			{
				$this->session->set_userdata("success_message", "Team added successfully to the fixture");
				if(!empty($this->session->userdata('referee_is_logged_in')))
				{
					redirect('referee-postings');
				}
				else
				{
					redirect('soccer-management/manage-fixture/'.$fixture_id.'/'.$league_duration_id.'/'.$league_id);
				}
			}
			
			else
			{
				$this->session->set_userdata("error_message","Could not add team. Please try again ");
			}
		}
		$data['title'] = 'Add team to fixture '.$fixture_id.' season '.$league_duration_id;
		$v_data['title'] = $data['title'];
		$v_data['league_id'] = $league_id;
		$v_data['fixture_id'] = $fixture_id;
		$v_data['league_duration_id'] = $league_duration_id;
		$data['content'] = $this->load->view('league/fixture_management', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}
	public function add_fixture_referee($fixture_id, $league_duration_id,$league_id)
	{
		//form validation rules
		$this->form_validation->set_rules('referee_id', 'Referee', 'required|xss_clean');
		
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			$fixture_referee_id = $this->fixture_model->add_fixture_referee($fixture_id);
			if($fixture_referee_id > 0)
			{
				$this->session->set_userdata("success_message", "Referee added successfully to the fixture");
				redirect('soccer-management/manage-fixture/'.$fixture_id.'/'.$league_duration_id.'/'.$league_id);  
			}
			
			else
			{
				$this->session->set_userdata("error_message","Could not add team. Please try again ");
			}
		}
		else
		{
			$validation_errors = validation_errors();
			$this->session->set_userdata("error_message", $validation_errors);
			redirect('soccer-management/manage-fixture/'.$fixture_id.'/'.$league_duration_id.'/'.$league_id);  
		}
		$v_data['league_id'] = $league_id;
		$v_data['fixture_id'] = $fixture_id;
		$v_data['league_duration_id'] = $league_duration_id;
		$data['content'] = $this->load->view('league/fixture_management', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}
	public function add_fixture_player($team_id,$fixture_team_id, $league_duration_id,$league_id)
	{
		//form validation rules
		$this->form_validation->set_rules('player_id', 'Player', 'required|xss_clean');
		$this->form_validation->set_rules('fixture_player_type_id','Fixture Player Type', 'required|xss_clean');
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			$fixture_player_id = $this->fixture_model->add_fixture_player($fixture_team_id);
			if($fixture_player_id > 0)
			{
				$this->session->set_userdata("success_message", "Player added successfully to the fixture");
				redirect('soccer-management/add-league-duration-fixture/'.$league_duration_id.'/'.$league_id);    
			}
			
			else
			{
				$this->session->set_userdata("error_message","Could not add player. Please try again ");
			}
		}
		$v_data['fixture_players'] = $this->league_model->get_all_fixture_players($team_id);
		$v_data['team_name'] = $this->league_model->get_team_name($team_id);
		$v_data['all_fixture_players'] = $this->fixture_model->get_all_fixture_players($fixture_team_id);
		$v_data['player_type'] = $this->fixture_model->get_all_fixture_player_types();
		$data['title'] = 'Add players to team';
		$v_data['title'] = $data['title'];
		$v_data['league_id'] = $league_id;
		$v_data['league_duration_id'] = $league_duration_id;
		$data['content'] = $this->load->view('league/add_fixture_player', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
		
	}
	public function add_fixture_commissioner($fixture_id,$league_duration_id,$league_id)
	{
		//form validation rules
		$this->form_validation->set_rules('commissioner_id', 'Commission', 'required|xss_clean');
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			$fixture_commission_id = $this->fixture_model->add_fixture_commissioner($fixture_id);
			if($fixture_commission_id > 0)
			{
				$this->session->set_userdata("success_message", "Commissioner added successfully to the fixture");
				redirect('soccer-management/manage-fixture/'.$fixture_id.'/'.$league_duration_id.'/'.$league_id); 
			}
			
			else
			{
				$this->session->set_userdata("error_message","Could not add commissioner. Please try again ");
			}
		}
		$v_data['title'] = $data['title'];
		$v_data['league_id'] = $league_id;
		$v_data['fixture_id'] = $fixture_id;
		$v_data['league_duration_id'] = $league_duration_id;
		$data['content'] = $this->load->view('league/fixture_management', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}
	public function add_fixture_results($fixture_id,$league_duration_id,$league_id,$module = NULL)
	{
		$league_name = $this->league_model->get_league_name($league_id);
		$v_data['title'] = 'Results for fixture '.$fixture_id.' season '.$league_duration_id.' in '.$league_name.' league';
		$v_data['league_id'] = $league_id;
		$v_data['comment_type'] = $this->fixture_model->get_all_comment_types();
		$v_data['assessment_type']=$this->fixture_model->get_all_assessments();
		$v_data['all_fixture_comments'] = $this->fixture_model->get_comment_fixtures($fixture_id);
		$v_data['all_assessment_fixtures'] = $this->fixture_model->get_fixture_assessments($fixture_id);
		$v_data['goal_scorer'] = $this->league_model->get_all_fixture_players_scorers($fixture_id);
		$v_data['fixture_fouls'] = $this->fixture_model->get_all_fixture_fouls($fixture_id);
		$v_data['total_fixture_goal'] = $this->league_model->get_all_goals_scored($fixture_id);
		$v_data['home_team'] = $this->league_model->get_home_team($fixture_id);
		$v_data['goul_types'] = $this->fixture_model->get_all_goal_types();
		$v_data['foul_types'] = $this->league_model->get_all_foul_types();
		$v_data['action_types'] = $this->league_model->get_all_action_types();
		$v_data['away_team'] = $this->league_model->get_away_team($fixture_id);


		// changes
		$v_data['comment_type'] = $this->fixture_model->get_all_comment_types();
		$v_data['goul_types'] = $this->fixture_model->get_all_goal_types();
		$v_data['assessment_type']=$this->fixture_model->get_all_assessments();
		$v_data['all_fixture_comments'] = $this->fixture_model->get_comment_fixtures($fixture_id);
		$v_data['all_assessment_fixtures'] = $this->fixture_model->get_fixture_assessments($fixture_id);
		$v_data['goal_scorer'] = $this->league_model->get_all_fixture_players_scorers($fixture_id);
		$v_data['fixture_fouls'] = $this->fixture_model->get_all_fixture_fouls($fixture_id);
		$v_data['total_fixture_goal'] = $this->league_model->get_all_goals_scored($fixture_id);
		$v_data['home_team'] = $home_team = $this->league_model->get_home_team($fixture_id);
		$v_data['fixture_teams'] = $this->league_model->get_fixture_teams($fixture_id);


		$v_data['team_details'] = $this->fixture_model->get_fixture_teams_details($fixture_id);
		$v_data['team_details_home_in'] = $this->fixture_model->get_fixture_teams_details_home_in($fixture_id);
		$v_data['team_details_home_out'] = $this->fixture_model->get_fixture_teams_details_home_out($fixture_id);
		$v_data['team_details_away_in'] = $this->fixture_model->get_fixture_teams_details_away_in($fixture_id);
		$v_data['team_details_away_out'] = $this->fixture_model->get_fixture_teams_details_away_out($fixture_id);




		$v_data['foul_types'] = $this->league_model->get_all_foul_types();
		$v_data['action_types'] = $this->league_model->get_all_action_types();
		$v_data['away_team'] = $away_team = $this->league_model->get_away_team($fixture_id);
		$v_data['fixture_detail'] = $this->referee_model->get_fixture_detail($fixture_id);
		$v_data['league_name'] = $league_name;
		$v_data['fixture_id'] = $fixture_id;
		$v_data['module'] = $module;
		$v_data['league_duration_id'] = $league_duration_id;

		$v_data['title_away'] = $away_team.' Away Substitutes';
		$v_data['title_home'] = $home_team.' Home Substitutes';

		// end of changes
		
		$v_data['league_name'] = $league_name;
		$v_data['fixture_id'] = $fixture_id;
		$v_data['module'] = $module;
		$v_data['league_duration_id'] = $league_duration_id;
		$data['content'] = $this->load->view('league/fixture_results', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}
	public function add_fixture_scores($fixture_id,$league_duration_id,$league_id)
	{
	//form validation
		$this->form_validation->set_rules('fixture_team_player_id', 'Scorer', 'required|xss_clean');
		$this->form_validation->set_rules('goal_minute','Score time', 'required|xss_clean');
		$this->form_validation->set_rules('goal_type_id','Goal Type', 'required|xss_clean');
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			$goal_id = $this->fixture_model->add_fixture_goals($fixture_id);
			if($goal_id > 0)
			{
				$this->session->set_userdata("success_message", "Goal added successfully to the fixture");  
			}
			
			else
			{
				$this->session->set_userdata("error_message","Could not add team. Please try again ");
			}
		}
		else
		{
			$this->session->set_userdata("error_message",validation_errors());
		}
		redirect('soccer-management/add-fixture-result/'.$fixture_id.'/'.$league_duration_id.'/'.$league_id);
	}
	public function add_fixture_fouls($fixture_id,$league_duration_id,$league_id)
	{
		//form validation
		$this->form_validation->set_rules('foul_player_id', 'Offender', 'required|xss_clean');
		$this->form_validation->set_rules('foul_type_id','Foul type', 'required|xss_clean');
		$this->form_validation->set_rules('foul_minute','Foul minute', 'required|xss_clean');
		$this->form_validation->set_rules('action_id','Action taken', 'required|xss_clean');
		
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			$foul_id = $this->fixture_model->add_fixture_fouls($fixture_id);
			if($foul_id > 0)
			{
				$this->session->set_userdata("success_message", "Foul added successfully to the fixture");
				redirect('soccer-management/add-fixture-result/'.$fixture_id.'/'.$league_duration_id.'/'.$league_id);  
			}
			
			else
			{
				$this->session->set_userdata("error_message","Could not add foul. Please try again ");
			}
		}
		else
		{
			$validation_errors = validation_errors();
			$this->session->set_userdata("error_message", $validation_errors);
			redirect('soccer-management/add-fixture-result/'.$fixture_id.'/'.$league_duration_id.'/'.$league_id); 
		}	
	}
	public function add_fixture_comments($comment_id,$fixture_id,$league_duration_id,$league_id)
	{
		//form validation
		$this->form_validation->set_rules('comment_fixture_description', 'Comment', 'required|xss_clean');
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			//echo $comment_id;die();
			$fixture_comment_id = $this->fixture_model->add_fixture_comments($fixture_id,$comment_id);
			if($fixture_comment_id > 0)
			{
				$this->session->set_userdata("success_message", "Comment added successfully to the fixture");
				redirect('soccer-management/add-fixture-result/'.$fixture_id.'/'.$league_duration_id.'/'.$league_id);  
			}
			
			else
			{
				$this->session->set_userdata("error_message","Could not add comment. Please try again ");
			}
		}
		else
		{
			$validation_errors = validation_errors();
			$this->session->set_userdata("error_message", $validation_errors);
			redirect('soccer-management/add-fixture-result/'.$fixture_id.'/'.$league_duration_id.'/'.$league_id); 
		}	
	}
	public function add_fixture_assignments($assessment_id,$fixture_id,$league_duration_id,$league_id)
	{
		$this->form_validation->set_rules('assessment_fixture_description', 'Assessment', 'required|xss_clean');
		$this->form_validation->set_rules('assessment_name', 'Rating', 'required|xss_clean');
		//if form conatins invalid data
		if ($this->form_validation->run())
		{
			//echo $comment_id;die();
			$fixture_assessment_id = $this->fixture_model->add_fixture_assessmentss($fixture_id,$assessment_id);
			if($fixture_assessment_id > 0)
			{
				$this->session->set_userdata("success_message", "Assessment added successfully to the fixture");
				redirect('soccer-management/add-fixture-result/'.$fixture_id.'/'.$league_duration_id.'/'.$league_id);  
			}
			
			else
			{
				$this->session->set_userdata("error_message","Could not add assessment. Please try again ");
			}
		}
		else
		{
			$validation_errors = validation_errors();
			$this->session->set_userdata("error_message", $validation_errors);
			redirect('soccer-management/add-fixture-result/'.$fixture_id.'/'.$league_duration_id.'/'.$league_id); 
		}	
	}
	public function view_league_standings($league_duration_id,$league_id)
	{
		$v_data['title'] = $data['title'] = 'Standings';
		$v_data['teams'] = $this->league_model->get_league_teams($league_duration_id);
		$data['content'] = $this->load->view('league/league_table', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}
	public function manage_fixture($fixture_id,$league_duration_id,$league_id)
	{
		$v_data['title'] = $data['title'] = 'Fixture '.$fixture_id.' Management';
		$v_data['season_teams'] = $this->league_model->get_season_fixture_teams($league_duration_id,$fixture_id); 
		$v_data['fixture_teams'] = $this->fixture_model->get_fixture_teams($fixture_id);
		$v_data['team_types'] = $this->fixture_model->get_fixture_team_type();
		$v_data['referee_types'] = $this->fixture_model->get_fixture_referee_type();
		$v_data['referees'] = $this->referee_model->get_all_referees();
		$v_data['fixture_referees'] = $this->fixture_model->get_fixture_referee($fixture_id);
		$v_data['fixture_commissioners'] = $this->fixture_model->get_all_fixture_commissioners($fixture_id);
		$v_data['personnel_commissioners'] = $this->fixture_model->get_all_personnel();
		$v_data['player_type'] = $this->fixture_model->get_all_fixture_player_types();
		$v_data['fixture_id'] = $fixture_id;
		$v_data['league_duration_id'] = $league_duration_id;
		$v_data['league_id'] = $league_id;
		$data['content'] = $this->load->view('league/fixture_management', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}
	public function add_fixture_payments($fixture_id,$league_duration_id,$league_id)
	{
		$v_data['title'] = $data['title'] = 'League Payments';
		$v_data['league_duration_id'] = $league_duration_id;
		$v_data['fixture_id'] = $fixture_id;
		$v_data['league_id'] = $league_id;
		$v_data['fixture_referees'] = $this->league_model->get_fixture_referee($fixture_id);
		$v_data['fixture_commissioners'] = $this->league_model->get_all_fixture_commissioners($fixture_id);
		$data['content'] = $this->load->view('league/fixture_payments', $v_data, true);
		$this->load->view('admin/templates/general_page', $data);
	}
	public function send_referee_to_admin($referee_id,$fixture_id,$league_duration_id,$league_id)
	{
		$v_data['league_duration_id'] = $league_duration_id;
		$v_data['fixture_id'] = $fixture_id;
		$v_data['league_id'] = $league_id;
		$v_data['fixture_referees'] = $this->league_model->get_fixture_referee($fixture_id);
		$v_data['fixture_commissioners'] = $this->league_model->get_all_fixture_commissioners($fixture_id);
		if($this->league_model->send_referee_to_admin($referee_id,$fixture_id))
		{
			$this->session->set_userdata("success_message", "Referee sent successfully to admin");
		}
		else
		{
			$this->session->set_userdata("error_message","Could not send referee to admin. Please try again ");
		}
		redirect('soccer-management/add-league-fixture-payments/'.$fixture_id.'/'.$league_duration_id.'/'.$league_id);
	}
	public function send_commissioner_to_admin($commissioner_id,$fixture_id,$league_duration_id,$league_id)
	{
		$v_data['league_duration_id'] = $league_duration_id;
		$v_data['fixture_id'] = $fixture_id;
		$v_data['league_id'] = $league_id;
		$v_data['fixture_referees'] = $this->league_model->get_fixture_referee($fixture_id);
		$v_data['fixture_commissioners'] = $this->league_model->get_all_fixture_commissioners($fixture_id);
		if($this->league_model->send_commissioner_to_admin($commissioner_id,$fixture_id))
		{
			$this->session->set_userdata("success_message", "Referee sent successfully to admin");
		}
		else
		{
			$this->session->set_userdata("error_message","Could not send referee to admin. Please try again ");
		}
		redirect('soccer-management/add-league-fixture-payments/'.$fixture_id.'/'.$league_duration_id.'/'.$league_id);
	}
	public function send_referee_to_accounts($referee_id,$fixture_id,$league_duration_id,$league_id)
	{
		$v_data['league_duration_id'] = $league_duration_id;
		$v_data['fixture_id'] = $fixture_id;
		$v_data['league_id'] = $league_id;
		$v_data['fixture_referees'] = $this->league_model->get_fixture_referee($fixture_id);
		$v_data['fixture_commissioners'] = $this->league_model->get_all_fixture_commissioners($fixture_id);
		if($this->league_model->send_referee_to_accounts($referee_id,$fixture_id))
		{
			$referee_details = $this->referee_model->get_referee($referee_id);
			$row = $referee_details->row();
			$referee_fname = '';
			$referee_onames = '';
			if(!empty($row))
			{
			$referee_onames = $row->personnel_onames;
			$referee_fname = $row->personnel_fname;
			}
			$referee_name = $referee_onames.' '.$referee_fname;
			$this->soccer_management_model->send_email_to_accounts($referee_name,'Referee');
			$this->session->set_userdata("success_message", "Referee sent successfully to accounts");
		}
		else
		{
			$this->session->set_userdata("error_message","Could not send referee to accounts. Please try again ");
		}
		redirect('soccer-management/add-league-fixture-payments/'.$fixture_id.'/'.$league_duration_id.'/'.$league_id);
	}
	public function send_commissioner_to_accounts($commissioner_id,$fixture_id,$league_duration_id,$league_id)
	{
		$v_data['league_duration_id'] = $league_duration_id;
		$v_data['fixture_id'] = $fixture_id;
		$v_data['league_id'] = $league_id;
		$v_data['fixture_referees'] = $this->league_model->get_fixture_referee($fixture_id);
		$v_data['fixture_commissioners'] = $this->league_model->get_all_fixture_commissioners($fixture_id);
		if($this->league_model->send_commissioner_to_accounts($commissioner_id,$fixture_id))
		{
			$referee_details = $this->referee_model->get_referee($commissioner_id);
			$row = $referee_details->row();
			$referee_fname = '';
			$referee_onames = '';
			if(!empty($row))
			{
			$referee_onames = $row->personnel_onames;
			$referee_fname = $row->personnel_fname;
			}
			$referee_name = $referee_onames.' '.$referee_fname;
			$this->soccer_management_model->send_email_to_accounts($referee_name,'Commissioner');
			$this->session->set_userdata("success_message", "Referee sent successfully to accounts");
		}
		else
		{
			$this->session->set_userdata("error_message","Could not send referee to accounts. Please try again ");
		}
		redirect('soccer-management/add-league-fixture-payments/'.$fixture_id.'/'.$league_duration_id.'/'.$league_id);
	}
	public function send_referee_payments($referee_id,$fixture_id,$league_duration_id,$league_id)
	{
		$v_data['league_duration_id'] = $league_duration_id;
		$v_data['fixture_id'] = $fixture_id;
		$v_data['league_id'] = $league_id;
		$v_data['fixture_referees'] = $this->league_model->get_fixture_referee($fixture_id);
		$v_data['fixture_commissioners'] = $this->league_model->get_all_fixture_commissioners($fixture_id);
		
		if($this->league_model->send_payments_to_referee($referee_id,$fixture_id))
		{
			$this->session->set_userdata("success_message", "Referee payment for fixture added successfully");
		}
		else
		{
			$this->session->set_userdata("error_message","Could not add referee payment. Please try again ");
		}
		redirect('soccer-management/add-league-fixture-payments/'.$fixture_id.'/'.$league_duration_id.'/'.$league_id);
	}
	public function send_commissioner_payments($commissioner_id,$fixture_id,$league_duration_id,$league_id)
	{
		$v_data['league_duration_id'] = $league_duration_id;
		$v_data['fixture_id'] = $fixture_id;
		$v_data['league_id'] = $league_id;
		$v_data['fixture_referees'] = $this->league_model->get_fixture_referee($fixture_id);
		$v_data['fixture_commissioners'] = $this->league_model->get_all_fixture_commissioners($fixture_id);
		
		if($this->league_model->send_payments_to_commissioner($commissioner_id,$fixture_id))
		{
			$this->session->set_userdata("success_message", "Referee payment for fixture added successfully");
		}
		else
		{
			$this->session->set_userdata("error_message","Could not add referee payment. Please try again ");
		}
		redirect('soccer-management/add-league-fixture-payments/'.$fixture_id.'/'.$league_duration_id.'/'.$league_id);
	}
	public function view_referee_queue($league_referee_id,$league_referee_payments_queue_id,$league_fixture_type,$fixture_id)
	{
		if($league_referee_payments_queue_id == 0)
		{
			$this->session->set_userdata("error_message","The referee details cannot be viewed until the referee is added to the admin queue");
			redirect('accounts/referee-payments');
		}
		$league_referee_invoice = $this->league_model->get_ref_invoice($fixture_id,$league_referee_id);
		$league_referee_payments = $this->league_model->get_ref_payment($league_referee_payments_queue_id);
		$v_data['league_referee_invoice'] = $league_referee_invoice;
		$v_data['league_referee_payments'] = $league_referee_payments;
		
		$referee = $this->referee_model->get_referee($league_referee_id);
		$referee_details = $referee->row();
		$referee_fname = $referee_details->personnel_fname;
		$referee_onames = $referee_details->personnel_onames;
		$v_data['title'] = $referee_fname.' '.$referee_onames;
		$v_data['home_team'] = $this->league_model->get_home_team($fixture_id);;
		$v_data['away_team'] = $this->league_model->get_home_team($fixture_id);;
		$v_data['fixture_date'] = $this->league_model->get_league_fixture_date($fixture_id);
		
		$data['title'] = 'Referee Fixture Statement';
		$data['content'] = $this->load->view('league/ref_queue_details', $v_data, TRUE);
		$this->load->view('admin/templates/general_page', $data);
		
	}
	public function pay_referee($league_referee_id,$league_referee_payments_queue_id,$league_fixture_type)
	{
		//form validation 
		$this->form_validation->set_rules('payment_amount','Payment Amount','required|xss_clean');
		
		if($this->form_validation->run())
		{
			//save the referee payment
			if($this->tournament_model->pay_referee($league_referee_payments_queue_id))
			{
				$this->session->set_userdata("success_message", "Referee payment for fixture added successfully");
				redirect('accounts/referee-payments');
			}
			else
			{
			}
		}
		if($league_fixture_type == 2)
		{
			$page_title = "Tournament";
		}
		else
		{
			$page_title = "League";
		}
		$referee_details = $this->referee_model->get_referee($league_referee_id)->row();
		$referee_fname = $referee_details->referee_fname;	
		$referee_onames = $referee_details->referee_onames;
		$referee_name = $referee_fname.' '.$referee_onames;
		
		$data['title'] = "Ref Payments";
		$v_data['title'] =  $referee_name.' Payments For '.$page_title;
		$v_data['league_referee_id'] = $league_referee_id;
		$v_data['league_referee_payments_queue_id'] = $league_referee_payments_queue_id;
		$data['content'] = $this->load->view('tournament/pay_referee', $v_data, TRUE);
		
		$this->load->view('admin/templates/general_page', $data);
	}
	public function get_league_fixture_summary($fixture_id,$league_duration_id,$league_id)
	{
		$v_data['fixture_details'] = $this->fixture_model->get_fixture($fixture_id);
		$v_data['goal_scorer'] = $this->league_model->get_all_fixture_players_scorers($fixture_id);
		$v_data['fixture_fouls'] = $this->fixture_model->get_all_fixture_fouls($fixture_id);
		$v_data['total_fixture_goal'] = $this->league_model->get_all_goals_scored($fixture_id);
		$v_data['penalty_goal'] = $this->league_model->get_all_penalty_scored($fixture_id);
		$v_data['home_team'] = $this->league_model->get_home_team($fixture_id);
		$v_data['away_team'] = $this->league_model->get_away_team($fixture_id);
		$v_data['home_players'] = $this->league_model->get_home_players($fixture_id);
		$v_data['ref_summary'] = $this->league_model->get_ref_summary($fixture_id);
		$v_data['away_players'] = $this->league_model->get_away_players($fixture_id);
		$v_data['home_substitutes'] = $this->league_model->get_home_fixture_subs($fixture_id);
		$v_data['away_substitutes'] = $this->league_model->get_away_fixture_subs($fixture_id);
		$v_data['title'] = 'Referee Fixture Summary for '.$v_data['home_team'].' Vs '.$v_data['away_team'];
		$this->load->view('league/ref_league_summary', $v_data);
	}
	public function get_league_commissioner_fixture_summary($fixture_id,$league_duration_id,$league_id)
	{
		$v_data['fixture_details'] = $this->fixture_model->get_fixture($fixture_id);
		$v_data['all_fixture_comments'] = $this->fixture_model->get_comment_fixtures($fixture_id);
		$v_data['all_assessment_fixtures'] = $this->fixture_model->get_fixture_assessments($fixture_id);
		$v_data['home_team'] = $this->league_model->get_home_team($fixture_id);
		$v_data['away_team'] = $this->league_model->get_away_team($fixture_id);
		$v_data['fixture_referees'] = $this->fixture_model->get_fixture_referee($fixture_id);
		
		$v_data['title'] = 'Commissioner Fixture Summary for '.$v_data['home_team'].' Vs '.$v_data['away_team'];
		$this->load->view('league/comm_league_summary', $v_data);
	}
	public function view_commissioner_queue($personnel_id,$personnel_payments_queue_id,$fixture_type,$tournament_fixture_id)
	{
		if($personnel_payments_queue_id == 0)
		{
			$this->session->set_userdata("error_message","The personnel details cannot be viewed until the personnel is added to the admin queue");
			redirect('accounts/commissioner-payments');
		}
		$personnel_invoice = $this->league_model->get_ref_commissioner_invoice($tournament_fixture_id,$personnel_id);
		$personnel_payments = $this->league_model->get_commissioner_ref_payment($personnel_payments_queue_id);
		$v_data['personnel_invoice'] = $personnel_invoice;
		$v_data['personnel_payments'] = $personnel_payments;
		
		$personnel = $this->personnel_model->get_personnel($personnel_id);
		$personnel_details = $personnel->row();
		$personnel_fname = $personnel_details->personnel_fname;
		$personnel_onames = $personnel_details->personnel_onames;
		$v_data['title'] = $personnel_fname.' '.$personnel_onames;
		$v_data['home_team'] = $this->league_model->get_home_team($tournament_fixture_id);
		$v_data['away_team'] = $this->league_model->get_away_team($tournament_fixture_id);
		$v_data['fixture_date'] = $this->league_model->get_fixture_date($tournament_fixture_id);
		
		$data['title'] = 'League Commissioner Fixture Statement';
		$data['content'] = $this->load->view('league/commissioner_ref_queue_details', $v_data, TRUE);
		$this->load->view('admin/templates/general_page', $data);
		
	}

}

?>