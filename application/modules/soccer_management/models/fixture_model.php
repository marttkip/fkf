<?php
class Fixture_model extends CI_Model 
{
	public function get_all_venues()
	{
		$this->db->where('venue_deleted = 0');
		$query = $this->db->get('venue');
		
		return $query;
	}
	public function add_subs($fixture_id)
	{
		$fixture_data = array(
			'fixture_id'=>$fixture_id,
			'player_in'=>$this->input->post('home_player_in'),
			'minute'=>$this->input->post('home_minute'),
			'player_out'=>$this->input->post('home_player_out'),
			'status'=>1,
			'sub_type' => 1//type 1 is home substitution
			);
		if($this->db->insert('subs', $fixture_data))
		{
			return $this->db->insert_id();
		}
		else
		{
			return FALSE;
		}
	}
	public function edit_subs($fixture_id,$home_sub_id)
	{
		$fixture_data = array(
			'fixture_id'=>$fixture_id,
			'player_in'=>$this->input->post('home_player_in'.$home_sub_id),
			'minute'=>$this->input->post('home_minute'.$home_sub_id),
			'player_out'=>$this->input->post('home_player_out'.$home_sub_id),
			'status'=>1,
			'sub_type' => 1//type 1 is home substitution
			);
		$this->db->where('sub_id',$home_sub_id);
		if($this->db->update('subs', $fixture_data))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	public function add_t_subs($fixture_id,$module)
	{
		if(empty($module))
		{
			$fixture_data = array(
				'fixture_id'=>$fixture_id,
				'player_in'=>$this->input->post('home_player_in'),
				'minute'=>$this->input->post('home_minute'),
				'player_out'=>$this->input->post('home_player_out'),
				'status'=>1,
				'sub_type' => 1//type 1 is home substitution
				);
			if($this->db->insert('t_subs', $fixture_data))
			{
				return $this->db->insert_id();
			}
			else
			{
				return FALSE;
			}
		}
		else
		{

			$fixture_data = array(
				'fixture_id'=>$fixture_id,
				'player_in'=>$this->input->post('home_player_in'),
				'minute'=>$this->input->post('home_minute'),
				'player_out'=>$this->input->post('home_player_out'),
				'status'=>1,
				'sub_type' => 1//type 1 is home substitution
				);
			if($this->db->insert('commissioner_t_subs', $fixture_data))
			{
				return $this->db->insert_id();
			}
			else
			{
				return FALSE;
			}

		}
		
	}
	public function add_away_subs($fixture_id,$module)
	{

		if(empty($module))
		{
			$fixture_data = array(
				'fixture_id'=>$fixture_id,
				'player_in'=>$this->input->post('away_player_in'),
				'minute'=>$this->input->post('away_time'),
				'player_out'=>$this->input->post('away_player_out'),
				'status'=>1,
				'sub_type' => 2//type 2 is away substitution
				);
			if($this->db->insert('subs', $fixture_data))
			{
				return $this->db->insert_id();
			}
			else
			{
				return FALSE;
			}

		}
		else
		{
			$fixture_data = array(
				'fixture_id'=>$fixture_id,
				'player_in'=>$this->input->post('away_player_in'),
				'minute'=>$this->input->post('away_time'),
				'player_out'=>$this->input->post('away_player_out'),
				'status'=>1,
				'sub_type' => 2//type 2 is away substitution
				);
			if($this->db->insert('commissioner_subs', $fixture_data))
			{
				return $this->db->insert_id();
			}
			else
			{
				return FALSE;
			}
		}
		
	}

	public function edit_away_subs($fixture_id,$away_sub_id,$module)
	{

		if(empty($module))
		{
			$fixture_data = array(
				'fixture_id'=>$fixture_id,
				'player_in'=>$this->input->post('away_player_in'),
				'minute'=>$this->input->post('away_time'),
				'player_out'=>$this->input->post('away_player_out'),
				'status'=>1,
				'sub_type' => 2//type 2 is away substitution
				);
			$this->db->where('sub_id',$away_sub_id);
			if($this->db->update('subs', $fixture_data))
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}

		}
		else
		{
			$fixture_data = array(
				'fixture_id'=>$fixture_id,
				'player_in'=>$this->input->post('away_player_in'),
				'minute'=>$this->input->post('away_time'),
				'player_out'=>$this->input->post('away_player_out'),
				'status'=>1,
				'sub_type' => 2//type 2 is away substitution
				);
			$this->db->where('sub_id',$away_sub_id);
			if($this->db->updated('commissioner_subs', $fixture_data))
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}
		
	}
	public function add_away_tsubs($tournament_fixture_id,$module)
	{

		if(empty($module))
		{
			$fixture_data = array(
				'fixture_id'=>$tournament_fixture_id,
				'player_in'=>$this->input->post('away_player_in'),
				'minute'=>$this->input->post('away_minute'),
				'player_out'=>$this->input->post('away_player_out'),
				'status'=>1,
				'sub_type' => 2//type 2 is away substitution
				);
			if($this->db->insert('t_subs', $fixture_data))
			{
				return $this->db->insert_id();
			}
			else
			{
				return FALSE;
			}
		}
		else
		{
			$fixture_data = array(
				'fixture_id'=>$tournament_fixture_id,
				'player_in'=>$this->input->post('away_player_in'),
				'minute'=>$this->input->post('away_minute'),
				'player_out'=>$this->input->post('away_player_out'),
				'status'=>1,
				'sub_type' => 2//type 2 is away substitution
				);
			if($this->db->insert('commissioner_t_subs', $fixture_data))
			{
				return $this->db->insert_id();
			}
			else
			{
				return FALSE;
			}

		}
		
	}
	public function add_fixture($league_duration_id)
	{
		$fixture_data = array(
			'league_duration_id'=>$league_duration_id,
			'fixture_date'=>$this->input->post('fixture_date'),
			'fixture_time'=>$this->input->post('fixture_time'),
			'fixture_number'=>$this->input->post('fixture_number'),
			'venue_id'=>$this->input->post('venue_id'),
			'created_by'=>$this->session->userdata('personnel_id'),
			'created_on' =>date('Y-m-d H:i:s')
			);
		if($this->db->insert('fixture', $fixture_data))
		{
			return $this->db->insert_id();
		}
		else
		{
			return FALSE;
		}
	}
	public function get_season_fixtures($league_duration_id)
	{
		$this->db->select('fixture.*, venue.venue_name');
		$this->db->where('venue.venue_id = fixture.venue_id AND league_duration_id = '.$league_duration_id);
		$query = $this->db->get('fixture,venue');
		
		return $query;
	}
	public function activate_league_duration_fixture($fixture_id)
	{
		$data = array(
				'fixture_status' => 1
			);
		$this->db->where('fixture_id', $fixture_id);
		

		if($this->db->update('fixture', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	public function deactivate_league_duration_fixture($fixture_id)
	{
		$data = array(
				'fixture_status' => 0
			);
		$this->db->where('fixture_id', $fixture_id);
		

		if($this->db->update('fixture', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	public function get_fixture_teams($fixture_id)
	{
		$this->db->select('team.team_name,team.team_id,fixture_team.*,fixture_team_type.fixture_team_type_name');
		$this->db->where('team.team_id = league_team.team_id AND league_team.league_team_id = fixture_team.league_team_id AND fixture_team.fixture_team_type_id = fixture_team_type.fixture_team_type_id AND fixture_team.fixture_id = '.$fixture_id);
		$query = $this->db->get('fixture_team,league_team, team, fixture_team_type');
		
		return $query;
	}
	public function get_fixture_team_type()
	{
		$this->db->select('*');
		$this->db->where('fixture_team_type_status = 1');
		$query = $this->db->get('fixture_team_type');
		
		return $query;
	}
	public function add_team_fixture($fixture_id)
	{
		if($this->check_no_of_teams_in_fixture($fixture_id) < 2)
		{
			$fixture_team = array(
			'fixture_id'=>$fixture_id,
			'league_team_id'=>$this->input->post('league_team_id'),
			'fixture_team_type_id'=>$this->input->post('fixture_team_type_id'),
			'added_by'=>$this->session->userdata('personnel_id'),
			'added_on' =>date('Y-m-d H:i:s')
			);
		if($this->db->insert('fixture_team', $fixture_team))
		{
			return $this->db->insert_id();
		}
		else
		{
			return FALSE;
		}
		}
	}
	public function check_no_of_teams_in_fixture($fixture_id)
	{
		$this->db->select('COUNT(league_team_id) as no_of_teams');
		$this->db->where('fixture_id = '.$fixture_id);
		$teams = $this->db->get('fixture_team');
		$no_teams = 0;
		if($teams->num_rows() > 0)
		{
			foreach($teams->result() as $fixtureteams)
			{
				$no_teams = $fixtureteams->no_of_teams;
			}
		}
		return $no_teams;
	}
	public function get_fixture_referee($fixture_id)
	{
		$this->db->select('fixture_referee.*, personnel.*, referee_type.referee_type_name');
		$this->db->where('fixture_referee.referee_id = personnel.personnel_id AND fixture_referee.referee_type_id = referee_type.referee_type_id AND fixture_referee.fixture_id = '.$fixture_id);
		$query = $this->db->get('fixture_referee, personnel, referee_type');
		return $query;
	}
	public function add_fixture_referee($fixture_id)
	{
		$fixture_referee = array(
			'fixture_id'=>$fixture_id,
			'referee_id'=>$this->input->post('referee_id'),
			'added_by'=>$this->session->userdata('personnel_id'),
			'referee_type_id'=>$this->input->post('referee_type_id'),
			'added_on' =>date('Y-m-d H:i:s')
			);
		if($this->db->insert('fixture_referee', $fixture_referee))
		{
			return $this->db->insert_id();
		}
		else
		{
			return FALSE;
		}
	}
	public function get_all_fixture_players($fixture_team_id)
	{
		$this->db->select('player.*,fixture_team_player.fixture_team_player_type_id, fixture_team_player.fixture_team_player_id');
		//$this->db->where('fixture_team_player.fixture_team_id = fixture_team.fixture_team_id AND player.player_id = fixture_team_player.player_id AND fixture_team.fixture_team_id = '.$fixture_team_id.' AND player.player_id NOT IN (SELECT player_id FROM fixture_team_player)');
		$this->db->where('fixture_team_player.fixture_team_id = fixture_team.fixture_team_id AND player.player_id = fixture_team_player.player_id AND fixture_team.fixture_team_id = '.$fixture_team_id.' AND fixture_team_player_deleted = 0');
		$query = $this->db->get('player,fixture_team_player, fixture_team');
		
		return $query;
	}
	public function get_all_fixture_player_types()
	{
		$this->db->select('fixture_player_type.*');
		$this->db->where('fixture_player_type_status = 1');
		$query = $this->db->get('fixture_player_type');
		
		return $query;
	}
	public function add_fixture_player($fixture_team_id)
	{
		$fixture_player_data = array(
			'player_id'=>$this->input->post('player_id'),
			'fixture_team_player_type_id'=>$this->input->post('fixture_player_type_id'),
			'added_by'=>$this->session->userdata('personnel_id'),
			'added_on' =>date('Y-m-d H:i:s'),
			'fixture_team_id'=>$fixture_team_id
			);
		if($this->db->insert('fixture_team_player', $fixture_player_data))
		{
			return $this->db->insert_id();
		}
		else
		{
			return FALSE;
		}
	}
	public function add_fixture_goals($fixture_id,$module)
	{

		if(empty($module))
		{

			$goal_type_id = $this->input->post('goal_type_id');
			if($goal_type_id == 1)
			{
				$penalty_score_status = $this->input->post('penalty_score_status');
			}
			else
			{
				$penalty_score_status = 1;
			}

			$fixture_scores = array(
				'fixture_id'=>$fixture_id,
				'fixture_team_player_id'=>$this->input->post('fixture_team_player_id'),
				'goal_type_id'=>$this->input->post('goal_type_id'),
				'goal_minute'=>$this->input->post('goal_minute'),
				'goal_scored'=>$penalty_score_status,
				'added_by'=>$this->session->userdata('personnel_id'),
				'added_on' =>date('Y-m-d H:i:s')
				);
			if($this->db->insert('goal', $fixture_scores))
			{
				return $this->db->insert_id();
			}
			else
			{
				return FALSE;
			}

		}
		else
		{
			$goal_type_id = $this->input->post('goal_type_id');
			if($goal_type_id == 1)
			{
				$penalty_score_status = $this->input->post('penalty_score_status');
			}
			else
			{
				$penalty_score_status = 1;
			}

			$fixture_scores = array(
				'fixture_id'=>$fixture_id,
				'fixture_team_player_id'=>$this->input->post('fixture_team_player_id'),
				'goal_type_id'=>$this->input->post('goal_type_id'),
				'goal_minute'=>$this->input->post('goal_minute'),
				'goal_scored'=>$penalty_score_status,
				'added_by'=>$this->session->userdata('personnel_id'),
				'added_on' =>date('Y-m-d H:i:s')
				);
			if($this->db->insert('commissioner_goal', $fixture_scores))
			{
				return $this->db->insert_id();
			}
			else
			{
				return FALSE;
			}
		}
		
	}


	public function edit_fixture_goals($fixture_id,$goal_id,$module)
	{

		if(empty($module))
		{

			$goal_type_id = $this->input->post('goal_type_id'.$goal_id);
			if($goal_type_id == 1)
			{
				$penalty_score_status = $this->input->post('penalty_score_status'.$goal_id);
			}
			else
			{
				$penalty_score_status = 1;
			}

			$fixture_scores = array(
				'fixture_id'=>$fixture_id,
				'fixture_team_player_id'=>$this->input->post('fixture_team_player_id'.$goal_id),
				'goal_type_id'=>$this->input->post('goal_type_id'.$goal_id),
				'goal_minute'=>$this->input->post('goal_minute'.$goal_id),
				'goal_scored'=>$penalty_score_status,
				'added_by'=>$this->session->userdata('personnel_id'.$goal_id),
				'added_on' =>date('Y-m-d H:i:s')
				);
			$this->db->where('goal_id',$goal_id);
			if($this->db->update('goal', $fixture_scores))
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}

		}
		else
		{
			$goal_type_id = $this->input->post('goal_type_id'.$goal_id);
			if($goal_type_id == 1)
			{
				$penalty_score_status = $this->input->post('penalty_score_status'.$goal_id);
			}
			else
			{
				$penalty_score_status = 1;
			}

			$fixture_scores = array(
				'fixture_id'=>$fixture_id,
				'fixture_team_player_id'=>$this->input->post('fixture_team_player_id'.$goal_id),
				'goal_type_id'=>$this->input->post('goal_type_id'.$goal_id),
				'goal_minute'=>$this->input->post('goal_minute'.$goal_id),
				'goal_scored'=>$penalty_score_status,
				'added_by'=>$this->session->userdata('personnel_id'.$goal_id)
				);
			$this->db->where('goal_id',$goal_id);
			if($this->db->update('commissioner_goal', $fixture_scores))
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}
		
	}
	public function add_fixture_timing($fixture_id,$module)
	{
		if(empty($module))
		{
			$fixture_scores = array(
				'first_half_start_time'=>$this->input->post('first_half_start_time'),
				'first_half_end_time'=>$this->input->post('first_half_end_time'),
				'second_half_start_time'=>$this->input->post('second_half_start_time'),
				'second_half_end_time'=>$this->input->post('second_half_end_time')
				);
			$this->db->where('fixture_id',$fixture_id);
			if($this->db->update('fixture', $fixture_scores))
			{
				return $fixture_id;
			}
			else
			{
				return FALSE;
			}
		}
		else
		{
			$fixture_scores = array(
				'commissioner_first_half_start_time'=>$this->input->post('first_half_start_time'),
				'commissioner_first_half_end_time'=>$this->input->post('first_half_end_time'),
				'commissioner_second_half_start_time'=>$this->input->post('second_half_start_time'),
				'commissioner_second_half_end_time'=>$this->input->post('second_half_end_time')
				);
			$this->db->where('fixture_id',$fixture_id);
			if($this->db->update('fixture', $fixture_scores))
			{
				return $fixture_id;
			}
			else
			{
				return FALSE;
			}

		}
	}

	public function add_tournament_fixture_timing($fixture_id,$module)
	{
		if(empty($module))
		{
			$fixture_scores = array(
				'first_half_start_time'=>$this->input->post('first_half_start_time'),
				'first_half_end_time'=>$this->input->post('first_half_end_time'),
				'second_half_start_time'=>$this->input->post('second_half_start_time'),
				'second_half_end_time'=>$this->input->post('second_half_end_time')
				);
			$this->db->where('tournament_fixture_id',$fixture_id);
			if($this->db->update('tournament_fixture', $fixture_scores))
			{
				return $fixture_id;
			}
			else
			{
				return FALSE;
			}
		}
		else
		{
			$fixture_scores = array(
				'commissioner_first_half_start_time'=>$this->input->post('first_half_start_time'),
				'commissioner_first_half_end_time'=>$this->input->post('first_half_end_time'),
				'commissioner_second_half_start_time'=>$this->input->post('second_half_start_time'),
				'commissioner_second_half_end_time'=>$this->input->post('second_half_end_time')
				);
			$this->db->where('tournament_fixture_id',$fixture_id);
			if($this->db->update('tournament_fixture', $fixture_scores))
			{
				return $fixture_id;
			}
			else
			{
				return FALSE;
			}

		}
	}
	public function add_fixture_fouls($fixture_id,$module)
	{

		if(empty($module))
		{
			$fixture_fouls = array(
				'fixture_id'=>$fixture_id,
				'foul_type_id'=>$this->input->post('foul_type_id'),
				'action_id'=>$this->input->post('action_id'),
				'fixture_team_player_id'=>$this->input->post('foul_player_id'),
				'foul_minute'=>$this->input->post('foul_minute'),
				'added_by'=>$this->session->userdata('personnel_id'),
				'added_on' =>date('Y-m-d H:i:s')
				);
			if($this->db->insert('foul', $fixture_fouls))
			{
				return $this->db->insert_id();
			}
			else
			{
				return FALSE;
			}
		}
		else
		{
			$fixture_fouls = array(
				'fixture_id'=>$fixture_id,
				'foul_type_id'=>$this->input->post('foul_type_id'),
				'action_id'=>$this->input->post('action_id'),
				'fixture_team_player_id'=>$this->input->post('foul_player_id'),
				'foul_minute'=>$this->input->post('foul_minute'),
				'added_by'=>$this->session->userdata('personnel_id'),
				'added_on' =>date('Y-m-d H:i:s')
				);
			if($this->db->insert('commissioner_foul', $fixture_fouls))
			{
				return $this->db->insert_id();
			}
			else
			{
				return FALSE;
			}

		}
		
	}

	public function edit_fixture_fouls($fixture_id,$foul_id,$module)
	{

		if(empty($module))
		{
			$fixture_fouls = array(
				'fixture_id'=>$fixture_id,
				'foul_type_id'=>$this->input->post('foul_type_id'.$foul_id),
				'action_id'=>$this->input->post('action_id'.$foul_id),
				'fixture_team_player_id'=>$this->input->post('foul_player_id'.$foul_id),
				'foul_minute'=>$this->input->post('foul_minute'.$foul_id)
				);
			$this->db->where('foul_id',$foul_id);
			if($this->db->update('foul', $fixture_fouls))
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}
		else
		{
			$fixture_fouls = array(
				'fixture_id'=>$fixture_id,
				'foul_type_id'=>$this->input->post('foul_type_id'.$foul_id),
				'action_id'=>$this->input->post('action_id'.$foul_id),
				'fixture_team_player_id'=>$this->input->post('foul_player_id'.$foul_id),
				'foul_minute'=>$this->input->post('foul_minute'.$foul_id)
				);
			$this->db->where('foul_id',$foul_id);
			if($this->db->insert('commissioner_foul', $fixture_fouls))
			{
				return $this->db->insert_id();
			}
			else
			{
				return FALSE;
			}

		}
		
	}
	public function get_all_fixture_fouls($fixture_id)
	{
		$this->db->select('player.*,team.team_name,foul.foul_minute, foul.foul_id, action.action_name, action.action_id,foul_type.foul_type_name,foul_type.foul_type_id');
		$this->db->where('foul.fixture_team_player_id = fixture_team_player.fixture_team_player_id AND fixture_team_player.player_id = player.player_id AND player.team_id = team.team_id AND foul.action_id = action.action_id AND foul.foul_type_id = foul_type.foul_type_id AND foul.fixture_id = '.$fixture_id.' AND foul.foul_deleted = 0');
		$query = $this->db->get('action,team,foul_type,player,foul,fixture_team_player');
		return $query;
	}
	public function get_all_fixture_fouls_commissioner($fixture_id)
	{
		$this->db->select('player.*,team.team_name,foul.foul_minute, action.action_name,foul_type.foul_type_name');
		$this->db->where('foul.fixture_team_player_id = fixture_team_player.fixture_team_player_id AND fixture_team_player.player_id = player.player_id AND player.team_id = team.team_id AND foul.action_id = action.action_id AND foul.foul_type_id = foul_type.foul_type_id AND foul.fixture_id = '.$fixture_id);
		$query = $this->db->get('action,team,foul_type,player,commissioner_foul AS foul,fixture_team_player');
		return $query;
	}
	public function get_all_fixture_commissioners($fixture_id)
	{
		$this->db->select('personnel.personnel_fname,personnel.personnel_onames, fixture_commissioner.*');
		$this->db->where('personnel.personnel_id = fixture_commissioner.personnel_id AND fixture_commissioner.fixture_id = '.$fixture_id);
		$query = $this->db->get('personnel,fixture_commissioner');
		return $query;
	}
	public function get_all_personnel()
	{
		$this->db->select('*');
		$this->db->where('personnel_status = 1 AND personnel_type_id = 4');
		$query = $this->db->get('personnel');
		return $query;
	}
	public function add_fixture_commissioner($fixture_id)
	{
		$fixture_commissioner = array(
			'fixture_id'=>$fixture_id,
			'personnel_id'=>$this->input->post('commissioner_id'),
			'added_by'=>$this->session->userdata('personnel_id'),
			'added_on' =>date('Y-m-d H:i:s')
			);
		if($this->db->insert('fixture_commissioner', $fixture_commissioner))
		{
			return $this->db->insert_id();
		}
		else
		{
			return FALSE;
		}
	}
	public function add_fixture_comments($fixture_id,$comment_id)
	{
		$fixture_comments = array(
			'fixture_id'=>$fixture_id,
			'comment_id'=>$comment_id,
			'comment_fixture_description'=>$this->input->post('comment_fixture_description'),
			'added_by'=>$this->session->userdata('personnel_id'),
			'added_on' =>date('Y-m-d H:i:s')
			);
		if($this->db->insert('comment_fixture', $fixture_comments))
		{
			return $this->db->insert_id();
		}
		else
		{
			return FALSE;
		}
	}
	public function add_fixture_assessments($fixture_id,$assessment_id)
	{
		$fixture_assessments = array(
			'fixture_id'=>$fixture_id,
			'assessment_id'=>$assessment_id,
			'assessment_fixture_description'=>$this->input->post('assessment_fixture_description'),
			'assessment_fixture_rating'=>$this->input->post('assessment_name'),
			'added_by'=>$this->session->userdata('personnel_id'),
			'added_on' =>date('Y-m-d H:i:s')
			);
		if($this->db->insert('assessment_fixture', $fixture_assessments))
		{
			return $this->db->insert_id();
		}
		else
		{
			return FALSE;
		}
	}
	public function add_summary($fixture_id)
	{
		$fixture_assessments = array(
			'fixture_id'=>$fixture_id,
			
	        'summary'=>$this->input->post('summary'),
			'status'=>1,
			
			);
		if($this->db->insert('fixture_summary', $fixture_assessments))
		{
			return $this->db->insert_id();
		}
		else
		{
			return FALSE;
		}
	}
	public function add_t_summary($fixture_id,$module)
	{
		if(empty($module))
		{
			$fixture_assessments = array(
				'ts_fixture_id'=>$fixture_id,
		        'summary'=>$this->input->post('summary'),
				'status'=>1,
				
				);
			if($this->db->insert('tornament_summary', $fixture_assessments))
			{
				return $this->db->insert_id();
			}
			else
			{
				return FALSE;
			}

		}
		else
		{
			$fixture_assessments = array(
				'ts_fixture_id'=>$fixture_id,
		        'summary'=>$this->input->post('summary'),
				'status'=>1,
				
				);
			if($this->db->insert('commissioner_tornament_summary', $fixture_assessments))
			{
				return $this->db->insert_id();
			}
			else
			{
				return FALSE;
		}

		}
		
	}
	public function get_all_comment_types()
	{
		$this->db->select('*');
		$this->db->where('comment_status = 1');
		$query = $this->db->get('comment');
		return $query;
	}
	public function get_all_goal_types()
	{
		$this->db->select('*');
		$this->db->where('goal_type_status = 1');
		$query = $this->db->get('goal_type');
		return $query;
	}
	public function get_comment_fixtures($fixture_id)
	{
		$this->db->select('comment_fixture.*, comment.comment_name');
		$this->db->where('comment.comment_id = comment_fixture.comment_id AND comment_fixture.fixture_id = '.$fixture_id);
		$query = $this->db->get('comment,comment_fixture');
		return $query;
	}
	public function get_fixture_assessments($fixture_id)
	{
		$this->db->select('assessment_fixture.*, assessment.assessment_name');
		$this->db->where('assessment.assessment_id = assessment_fixture.assessment_id AND assessment_fixture.fixture_id = '.$fixture_id);
		$query = $this->db->get('assessment,assessment_fixture');
		return $query;
	}
	public function get_parent_name($assessment_parent)
	{
		$this->db->select('assessment_name');
		$this->db->where('assessment_status = 1 AND assessment_parent = '.$assessment_parent);
		$query = $this->db->get('assessment');
		$assessment_parent_name = '';
		if($query->num_rows() > 0)
		{
			foreach($query->result() as $row)
			{
				$assessment_parent_name = $row->assessment_name;
			}
		}
		return $assessment_parent_name;
	}
	public function get_all_assessments()
	{
		$this->db->select('*');
		$this->db->where('assessment_status = 1 AND assessment_parent != 0');
		$query = $this->db->get('assessment');
		return $query;
	}
	public function get_fixture_referee_type()
	{
		$this->db->select('*');
		$this->db->where('referee_type_status = 1');
		$query = $this->db->get('referee_type');
		return $query;

	}
	public function get_fixture_teams_details($fixture_id)
	{
		$this->db->select('player.*');
		$this->db->where('player.player_id = fixture_team_player.player_id AND fixture_team_player.fixture_team_id = fixture_team.fixture_team_id AND fixture_team.fixture_id = fixture.fixture_id AND fixture.fixture_id = '.$fixture_id);
		$this->db->order_by('fixture_team_player.fixture_team_player_type_id', 'ASC');
		$query = $this->db->get('fixture_team, fixture, player, fixture_team_player');

		return $query;

	}
	public function get_fixture_teams_details_home_in($fixture_id)
	{
		$this->db->select('player.*');
		$this->db->where('fixture_team.fixture_team_type_id = 1 AND fixture_team_player.fixture_team_player_type_id = 2 AND player.player_id = fixture_team_player.player_id AND fixture_team_player.fixture_team_id = fixture_team.fixture_team_id AND fixture_team.fixture_id = fixture.fixture_id AND fixture.fixture_id = '.$fixture_id);
		$this->db->order_by('fixture_team_player.fixture_team_player_type_id', 'ASC');
		$query = $this->db->get('fixture_team, fixture, player, fixture_team_player');

		return $query;

	}

	public function get_fixture_teams_details_home_in_team($fixture_id,$team_id)
	{
		$this->db->select('player.*');
		$this->db->where('fixture_team.fixture_team_type_id = 1 AND fixture_team_player.fixture_team_player_type_id = 2 AND player.player_id = fixture_team_player.player_id AND fixture_team_player.fixture_team_id = fixture_team.fixture_team_id AND fixture_team.fixture_id = fixture.fixture_id AND fixture.fixture_id = '.$fixture_id.' AND fixture_team.league_team_id = league_team.league_team_id AND league_team.team_id = '.$team_id);
		$this->db->order_by('fixture_team_player.fixture_team_player_type_id', 'ASC');
		$query = $this->db->get('fixture_team, fixture, player, fixture_team_player,league_team');

		return $query;

	}

	public function get_fixture_teams_details_home_out_team($fixture_id,$team_id)
	{
		$this->db->select('player.*');
		$this->db->where('fixture_team.fixture_team_type_id = 1 AND fixture_team_player.fixture_team_player_type_id = 1 AND player.player_id = fixture_team_player.player_id AND fixture_team_player.fixture_team_id = fixture_team.fixture_team_id AND fixture_team.fixture_id = fixture.fixture_id AND fixture.fixture_id = '.$fixture_id.' AND fixture_team.league_team_id = league_team.league_team_id AND league_team.team_id = '.$team_id);
		$this->db->order_by('fixture_team_player.fixture_team_player_type_id', 'ASC');
		$query = $this->db->get('fixture_team, fixture, player, fixture_team_player,league_team');

		return $query;

	}


	public function get_fixture_teams_details_home_out($fixture_id)
	{
		$this->db->select('player.*');
		$this->db->where('fixture_team.fixture_team_type_id = 1 AND fixture_team_player.fixture_team_player_type_id = 1 AND player.player_id = fixture_team_player.player_id AND fixture_team_player.fixture_team_id = fixture_team.fixture_team_id AND fixture_team.fixture_id = fixture.fixture_id AND fixture.fixture_id = '.$fixture_id);
		$this->db->order_by('fixture_team_player.fixture_team_player_type_id', 'ASC');
		$query = $this->db->get('fixture_team, fixture, player, fixture_team_player');

		return $query;

	}
	public function get_fixture_teams_details_away_in($fixture_id)
	{
		$this->db->select('player.*');
		$this->db->where('fixture_team.fixture_team_type_id = 2 AND fixture_team_player.fixture_team_player_type_id = 2 AND player.player_id = fixture_team_player.player_id AND fixture_team_player.fixture_team_id = fixture_team.fixture_team_id AND fixture_team.fixture_id = fixture.fixture_id AND fixture.fixture_id = '.$fixture_id);
		$this->db->order_by('fixture_team_player.fixture_team_player_type_id', 'ASC');
		$query = $this->db->get('fixture_team, fixture, player, fixture_team_player');

		return $query;

	}
	public function get_fixture_teams_details_away_out($fixture_id)
	{
		$this->db->select('player.*');
		$this->db->where('fixture_team.fixture_team_type_id = 2 AND fixture_team_player.fixture_team_player_type_id = 1 AND player.player_id = fixture_team_player.player_id AND fixture_team_player.fixture_team_id = fixture_team.fixture_team_id AND fixture_team.fixture_id = fixture.fixture_id AND fixture.fixture_id = '.$fixture_id);
		$this->db->order_by('fixture_team_player.fixture_team_player_type_id', 'ASC');
		$query = $this->db->get('fixture_team, fixture, player, fixture_team_player');

		return $query;

	}
	public function get_tornament_teams_details($tournament_fixture_id)
	{
		$this->db->select('player.*');
		$this->db->where('player.player_id = tournament_fixture_team_player.player_id AND tournament_fixture_team_player.tournament_fixture_team_id = tournament_fixture_team.tournament_fixture_team_id AND tournament_fixture_team.tournament_fixture_id = tournament_fixture.tournament_fixture_id AND tournament_fixture.tournament_fixture_id = '.$tournament_fixture_id);
		$this->db->order_by('tournament_fixture_team_player.fixture_team_player_type_id', 'ASC');
		$query = $this->db->get('tournament_fixture_team, tournament_fixture, player, tournament_fixture_team_player');

		return $query;

	}
	public function get_tournament_fixture_teams_details_home_in($tournament_fixture_id)
	{
		$this->db->select('player.*');
		$this->db->where('tournament_fixture_team.fixture_team_type_id = 1 AND tournament_fixture_team_player.fixture_team_player_type_id = 2 AND player.player_id = tournament_fixture_team_player.player_id AND tournament_fixture_team_player.tournament_fixture_team_id = tournament_fixture_team.tournament_fixture_team_id AND tournament_fixture_team.tournament_fixture_id = tournament_fixture.tournament_fixture_id AND tournament_fixture.tournament_fixture_id = '.$tournament_fixture_id);
		$this->db->order_by('tournament_fixture_team_player.tournament_fixture_team_player_id', 'ASC');
		$query = $this->db->get('tournament_fixture_team, tournament_fixture, player, tournament_fixture_team_player');

		return $query;
	}
	public function get_tournament_fixture_teams_details_home_out($tournament_fixture_id)
	{
		$this->db->select('player.*');
		$this->db->where('tournament_fixture_team.fixture_team_type_id = 1 AND tournament_fixture_team_player.fixture_team_player_type_id = 1 AND player.player_id = tournament_fixture_team_player.player_id AND tournament_fixture_team_player.tournament_fixture_team_id = tournament_fixture_team.tournament_fixture_team_id AND tournament_fixture_team.tournament_fixture_id = tournament_fixture.tournament_fixture_id AND tournament_fixture.tournament_fixture_id = '.$tournament_fixture_id);
		$this->db->order_by('tournament_fixture_team_player.tournament_fixture_team_player_id', 'ASC');
		$query = $this->db->get('tournament_fixture_team, tournament_fixture, player, tournament_fixture_team_player');

		return $query;
	}
	public function get_tournament_fixture_teams_details_away_in($tournament_fixture_id)
	{
		$this->db->select('player.*');
		$this->db->where('tournament_fixture_team.fixture_team_type_id = 2 AND tournament_fixture_team_player.fixture_team_player_type_id = 2 AND player.player_id = tournament_fixture_team_player.player_id AND tournament_fixture_team_player.tournament_fixture_team_id = tournament_fixture_team.tournament_fixture_team_id AND tournament_fixture_team.tournament_fixture_id = tournament_fixture.tournament_fixture_id AND tournament_fixture.tournament_fixture_id = '.$tournament_fixture_id);
		$this->db->order_by('tournament_fixture_team_player.tournament_fixture_team_player_id', 'ASC');
		$query = $this->db->get('tournament_fixture_team, tournament_fixture, player, tournament_fixture_team_player');

		return $query;
	}
	public function get_tournament_fixture_teams_details_away_out($tournament_fixture_id)
	{
		$this->db->select('player.*');
		$this->db->where('tournament_fixture_team.fixture_team_type_id = 2 AND tournament_fixture_team_player.fixture_team_player_type_id = 1 AND player.player_id = tournament_fixture_team_player.player_id AND tournament_fixture_team_player.tournament_fixture_team_id = tournament_fixture_team.tournament_fixture_team_id AND tournament_fixture_team.tournament_fixture_id = tournament_fixture.tournament_fixture_id AND tournament_fixture.tournament_fixture_id = '.$tournament_fixture_id);
		$this->db->order_by('tournament_fixture_team_player.tournament_fixture_team_player_id', 'ASC');
		$query = $this->db->get('tournament_fixture_team, tournament_fixture, player, tournament_fixture_team_player');

		return $query;
	}	
	public function get_fixture($fixture_id)
	{
		$this->db->where('fixture_id = '.$fixture_id.' AND fixture.venue_id = venue.venue_id');
		
		return $this->db->get('fixture, venue');
	}
	public function delete_league_fixture_summary($fixture_summary_id)
	{
		$data = array('fixture_summary_deleted'=>1, 'deleted_by'=>$this->session->userdata('personnel_id'),'deleted_on'=>date('Y-m-d'));
		$this->db->where('fixture_summary_id = '.$fixture_summary_id);
		if($this->db->update('fixture_summary',$data))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	public function delete_league_subs($sub_id)
	{
		$data = array('sub_deleted'=>1, 'deleted_by'=>$this->session->userdata('personnel_id'),'deleted_on'=>date('Y-m-d'));
		$this->db->where('sub_id = '.$sub_id);
		if($this->db->update('subs',$data))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	public function delete_league_foul($foul_id)
	{
		$data = array('foul_deleted'=>1, 'deleted_by'=>$this->session->userdata('personnel_id'),'deleted_on'=>date('Y-m-d'));
		$this->db->where('foul_id = '.$foul_id);
		if($this->db->update('foul',$data))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	public function delete_league_goal($goal_id)
	{
		$data = array('goal_delete'=>1, 'deleted_by'=>$this->session->userdata('personnel_id'),'deleted_on'=>date('Y-m-d'));
		$this->db->where('goal_id = '.$goal_id);
		if($this->db->update('goal',$data))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
}
?>