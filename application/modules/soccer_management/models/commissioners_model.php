<?php
class Commissioners_model extends CI_Model 
{
	//get all league fixtures asssigned to commissioner
	public function get_all_commissioner_games($personnel_id)
	{
		$this->db->select('*');
		$this->db->where('personnel_id = '.$personnel_id);
		$query = $this->db->get('fixture_commissioner');
		
		return $query;
	}
	
	//get all tournament fixtures assigned to logged in personnel
	public function get_all_tournament_commissioner_games($personnel_id)
	{
		$this->db->select('*');
		$this->db->where('personnel_id = '.$personnel_id);
		$query = $this->db->get('tournament_fixture_commissioner');
		
		return $query;
	}
	public function get_all_league_fixtures($table, $where, $config, $page, $order, $order_method)
	{
		//retrieve all leaguees
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$query = $this->db->get('', $config, $page);
		
		return $query;
	}
}
?>