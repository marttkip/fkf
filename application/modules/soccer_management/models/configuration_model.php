<?php
class Configuration_model extends CI_Model 
{
	public function get_referee_payments()
	{
		$this->db->select('referee_payment.*,referee_type.referee_type_name');
		$this->db->where('referee_payment.referee_type_id = referee_type.referee_type_id');
		$query = $this->db->get('referee_type,referee_payment');
		return $query;
	}
	function add_referee_payment($referee_type_id)
	{
		$referee_payment = array(
							'referee_type_id '=>$referee_type_id,
							'referee_payment_amount'=>$this->input->post('payment_amount')
							);
		if($this->db->insert('referee_payment', $referee_payment))
		{
			return $this->db->insert_id();
		}
		else
		{
			return TRUE;
		}
	}
	function update_referee_payment($referee_payment_id)
	{
		$referee_payment = array(
							'referee_payment_amount'=>$this->input->post('payment_amount2')
							);
		$this->db->where('referee_payment_id ',$referee_payment_id);
		if($this->db->update('referee_payment', $referee_payment))
		{
			return TRUE;
		}
		else
		{
			return TRUE;
		}
	}
	function referee_payment_exists_not($referee_type_id)
	{
		$this->db->select('*');
		$this->db->where('referee_payment.referee_type_id = '.$referee_type_id);
		$query = $this->db->get('referee_payment');
		if($query->num_rows() > 0)
		{
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}
}
?>