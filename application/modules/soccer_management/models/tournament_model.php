<?php
class Tournament_model extends CI_Model 
{
	public function get_all_tournament($table, $where, $config, $page, $order, $order_method)
	{
		$this->db->from($table);
		$this->db->select('*');
		$this->db->where($where);
		$this->db->order_by($order, $order_method);
		$query = $this->db->get('', $config, $page);
		return $query;
	}
	public function add_tournament()
	{
		$data = array(
			'tournament_name'=>ucwords(strtolower($this->input->post('tournament_name'))),
			'tournament_type_id'=>ucwords(strtolower($this->input->post('tournament_type_id'))),
			'added_by'=>$this->session->userdata('personnel_id'),
			'added_on' =>date('Y-m-d H:i:s'),
		);
		
		if($this->db->insert('tournament', $data))
		{
			return $this->db->insert_id();
		}
		else{
			return FALSE;
		}
	}
	public function get_tournament_type_name($tournament_type_id)
	{
		$this->db->select('tournament_type_name');
		$this->db->where('tournament_type_id = '.$tournament_type_id);
		$query = $this->db->get('tournament_type');
		
		$type_name = '';
		$name = $query->row();
		$type_name = $name->tournament_type_name;
		
		return $type_name;
	}
	public function get_all_tournament_types()
	{
		$this->db->where('tournament_type_status = 1');
		$query = $this->db->get('tournament_type');
		
		return $query;
	}
	public function activate_tournament($tournament_id)
	{
		$data = array(
				'tournament_status' => 1
			);
		$this->db->where('tournament_id', $tournament_id);
		

		if($this->db->update('tournament', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	public function deactivate_tournament($tournament_id)
	{
		$data = array(
				'tournament_status' => 0
			);
		$this->db->where('tournament_id', $tournament_id);
		

		if($this->db->update('tournament', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	public function get_tournament($tournament_id)
	{
		$this->db->select('*');
		$this->db->where('tournament_id = '.$tournament_id);
		$query = $this->db->get('tournament');
		
		return $query;
	}
	public function edit_tournament($tournament_id)
	{
		$data = array(
			'tournament_name'=>ucwords(strtolower($this->input->post('tournament_name'))),
			'tournament_type_id'=>$this->input->post('tournament_type_id'),
			'modified_by'=>$this->session->userdata('personnel_id'),
			'last_modified' =>date('Y-m-d H:i:s'),
		);
		
		$this->db->where('tournament_id', $tournament_id);
		if($this->db->update('tournament', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	public function delete_tournament($tournament_id)
	{
		$data = array(
				'tournament_delete' => 1,
				'deleted_on' =>date('Y-m-d H:i:s'),
				'deleted_by'=>$this->session->userdata('personnel_id'),
			);
		$this->db->where('tournament_id', $tournament_id);
		
		if($this->db->update('tournament', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	public function get_tournament_name($tournament_id)
	{
		$name = '';
		$this->db->select('tournament_name');
		$this->db->where('tournament_id = '.$tournament_id);
		$query = $this->db->get('tournament');
		if($query->num_rows() > 0)
		{
			foreach($query->result() as $tournamentname)
			{
				$name = $tournamentname->tournament_name;
			}
		}
		return $name;
	}
	public function get_all_tournament_durations($tournament_id)
	{
		$this->db->select('*');
		$this->db->where('tournament_duration_deleted = 0 AND tournament_id = '.$tournament_id);
		$query = $this->db->get('tournament_duration');
		
		return $query;
	}
	public function add_tournament_duration($tournament_id)
	{
		$season_data = array(
			'tournament_duration_end_date'=>$this->input->post('tournament_duration_end_date'),
			'tournament_duration_start_date'=>$this->input->post('tournament_duration_start_date'),
			'created' =>date('Y-m-d H:i:s'),
			'created_by'=>$this->session->userdata('personnel_id'),
			'tournament_id'=>$tournament_id
			);
			if($this->db->insert('tournament_duration', $season_data))
			{
				return $this->db->insert_id();
			}
			else
			{
				return FALSE;
			}
	}
	public function edit_tournament_duration($tournament_duration_id)
	{
		$season_updates = array(
				'tournament_duration_end_date'=>$this->input->post('tournament_duration_end_date'),
				'modified_by'=>$this->session->userdata('personnel_id'),
				'tournament_duration_start_date'=>$this->input->post('tournament_duration_start_date')
				);
		$this->db->where('tournament_duration_id', $tournament_duration_id);
		

		if($this->db->update('tournament_duration', $season_updates))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	public function get_tournament_duration($tournament_duration_id)
	{
		$this->db->select('*');
		$this->db->where('tournament_duration_id = '.$tournament_duration_id);
		$query = $this->db->get('tournament_duration');
		return $query;
	}
	public function activate_tournament_duration($tournament_duration_id)
	{
		$data = array(
				'tournament_duration_status' => 1
			);
		$this->db->where('tournament_duration_id', $tournament_duration_id);
		

		if($this->db->update('tournament_duration', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	public function deactivate_tournament_duration($tournament_duration_id)
	{
		$data = array(
				'tournament_duration_status' => 0
			);
		$this->db->where('tournament_duration_id', $tournament_duration_id);
		

		if($this->db->update('tournament_duration', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	public function delete_tournament_duration($tournament_duration_id)
	{
		//delete season
		$data = array(
				'tournament_duration_deleted' => 1,
				'deleted_on' =>date('Y-m-d H:i:s'),
				'deleted_by'=>$this->session->userdata('personnel_id'),
			);
		$this->db->where('tournament_duration_id', $tournament_duration_id);
		
		if($this->db->update('tournament_duration', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	public function add_tournament_duration_team($tournament_duration_id)
	{
		$tournament_team_data = array(
			'team_id'=>$this->input->post('team_id'),
			'created' =>date('Y-m-d H:i:s'),
			'added_by'=>$this->session->userdata('personnel_id'),
			'tournament_duration_id'=>$tournament_duration_id
			);
			if($this->db->insert('tournament_team', $tournament_team_data))
			{
				return $this->db->insert_id();
			}
			else
			{
				return FALSE;
			}
	}
	public function get_tournament_teams($tournament_duration_id)
	{
		$this->db->select('*');
		$this->db->where('tournament_team.tournament_team_deleted = 0 AND team.team_id = tournament_team.team_id AND tournament_duration_id = '.$tournament_duration_id);
		$query = $this->db->get('tournament_team, team');
		return $query;
	}
	public function all_tournament_teams($tournament_duration_id)
	{
		$this->db->select('*');
		$this->db->where('team.team_id not in(SELECT team_id FROM tournament_team where tournament_team_deleted = 0 
AND tournament_team.tournament_duration_id = '.$tournament_duration_id.')');
		$query = $this->db->get('team');
		return $query;
	}
	function remove_tournament_team($team_id,$tournament_duration_id)
	{
		$data = array(
				'tournament_team_deleted' => 1,
				'deleted_on' =>date('Y-m-d H:i:s'),
				'deleted_by'=>$this->session->userdata('personnel_id'),
			);
		$this->db->where('team_id = '.$team_id.'  AND tournament_duration_id = '.$tournament_duration_id);
		
		if($this->db->update('tournament_team', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	public function add_fixture($tournament_duration_id)
	{
		$tournament_fixture_data = array(
			'tournament_duration_id'=>$tournament_duration_id,
			'tournament_fixture_date'=>$this->input->post('tournament_fixture_date'),
			'tournament_fixture_time'=>$this->input->post('tournament_fixture_time'),
			'tournament_fixture_number'=>$this->input->post('tournament_fixture_number'),
			'venue_id'=>$this->input->post('venue_id'),
			'created_by'=>$this->session->userdata('personnel_id'),
			'created_on' =>date('Y-m-d H:i:s')
			);
		if($this->db->insert('tournament_fixture', $tournament_fixture_data))
		{
			return $this->db->insert_id();
		}
		else
		{
			return FALSE;
		}
	}
	public function get_season_fixtures($tournament_duration_id)
	{
		$this->db->select('tournament_fixture.*, venue.venue_name');
		$this->db->where('venue.venue_id = tournament_fixture.venue_id AND tournament_duration_id = '.$tournament_duration_id);
		$query = $this->db->get('tournament_fixture,venue');
		
		return $query;
	}
	public function get_tournament_fixture_teams($tournament_duration_id,$tournament_fixture_id)
	{
		$this->db->select('team.team_name,team.team_id, tournament_team.*');
		$this->db->where('team.team_id = tournament_team.team_id 
AND tournament_team.tournament_duration_id  = '.$tournament_duration_id.' AND tournament_team.tournament_team_id NOT IN(SELECT tournament_team_id from tournament_fixture_team where tournament_fixture_id = '.$tournament_fixture_id.')');
		$query = $this->db->get('team,tournament_team');
		
		return $query;
	}
	public function get_fixture_teams($tournament_fixture_id)
	{
		$this->db->select('team.team_name,team.team_id,tournament_fixture_team.*,fixture_team_type.fixture_team_type_name');
		$this->db->where('team.team_id = tournament_team.team_id AND tournament_team.tournament_team_id = tournament_fixture_team.tournament_team_id AND tournament_fixture_team.fixture_team_type_id = fixture_team_type.fixture_team_type_id AND tournament_fixture_team.tournament_fixture_id = '.$tournament_fixture_id);
		$query = $this->db->get('tournament_fixture_team,tournament_team, team, fixture_team_type');
		
		return $query;
	}
	public function get_fixture_referee($tournament_fixture_id)
	{
		$this->db->select('tournament_fixture_referee.*, personnel.*, referee_type.referee_type_name');
		$this->db->where('tournament_fixture_referee.referee_id = personnel.personnel_id AND tournament_fixture_referee.referee_type_id = referee_type.referee_type_id AND tournament_fixture_referee.tournament_fixture_id = '.$tournament_fixture_id);
		$query = $this->db->get('tournament_fixture_referee, personnel,referee_type');

		return $query;
	}
	public function get_fixture_commissioner($tournament_fixture_id)
	{
		$this->db->select('tournament_fixture_commissioner.*, personnel_id.*');
		$this->db->where('tournament_fixture_commissioner.personnel_id = personnel.personnel_id AND tournament_fixture_commissioner.tournament_fixture_id = '.$tournament_fixture_id);
		$query = $this->db->get('tournament_fixture_commissioner, personnel');
		return $query;
	}
	public function get_all_fixture_commissioners($tournament_fixture_id)
	{
		$this->db->select('personnel.personnel_fname,personnel.personnel_onames, tournament_fixture_commissioner.*');
		$this->db->where('personnel.personnel_id = tournament_fixture_commissioner.personnel_id AND tournament_fixture_commissioner.tournament_fixture_id = '.$tournament_fixture_id);
		$query = $this->db->get('personnel,tournament_fixture_commissioner');
		return $query;
	}
	public function add_fixture_referee($tournament_fixture_id)
	{
		$tournament_fixture_referee = array(
			'tournament_fixture_id'=>$tournament_fixture_id,
			'referee_id'=>$this->input->post('referee_id'),
			'added_by'=>$this->session->userdata('personnel_id'),
			'referee_type_id'=>$this->input->post('referee_type_id'),
			'added_on' =>date('Y-m-d H:i:s')
			);
		if($this->db->insert('tournament_fixture_referee', $tournament_fixture_referee))
		{
			return $this->db->insert_id();
		}
		else
		{
			return FALSE;
		}
	}
	public function add_fixture_commissioner($tournament_fixture_id)
	{
		$tournament_fixture_commissioner = array(
			'tournament_fixture_id'=>$tournament_fixture_id,
			'personnel_id'=>$this->input->post('commissioner_id'),
			'added_by'=>$this->session->userdata('personnel_id'),
			'added_on' =>date('Y-m-d H:i:s')
			);
		if($this->db->insert('tournament_fixture_commissioner', $tournament_fixture_commissioner))
		{
			return $this->db->insert_id();
		}
		else
		{
			return FALSE;
		}
	}
	public function add_tournament_fixture_team($tournament_fixture_id)
	{
		if($this->check_no_of_teams_in_fixture($tournament_fixture_id) < 2)
		{
			$fixture_team = array(
			'tournament_fixture_id'=>$tournament_fixture_id,
			'tournament_team_id'=>$this->input->post('tournament_team_id'),
			'fixture_team_type_id'=>$this->input->post('fixture_team_type_id'),
			'added_by'=>$this->session->userdata('personnel_id'),
			'added_on' =>date('Y-m-d H:i:s')
			);
			if($this->db->insert('tournament_fixture_team', $fixture_team))
			{
				return $this->db->insert_id();
			}
			else
			{
				return FALSE;
			}
		}
	}
	public function check_no_of_teams_in_fixture($tournament_fixture_id)
	{
		$this->db->select('COUNT(tournament_team_id) as no_of_teams');
		$this->db->where('tournament_fixture_id = '.$tournament_fixture_id);
		$teams = $this->db->get('tournament_fixture_team');
		$no_teams = 0;
		if($teams->num_rows() > 0)
		{
			foreach($teams->result() as $fixtureteams)
			{
				$no_teams = $fixtureteams->no_of_teams;
			}
		}
		return $no_teams;
	}
	public function get_all_fixture_players($tournament_fixture_team_id)
	{
		$this->db->select('player.*,tournament_fixture_team_player.fixture_team_player_type_id, tournament_fixture_team_player.tournament_fixture_team_player_id');
		$this->db->where('tournament_fixture_team_player.tournament_fixture_team_id = tournament_fixture_team.tournament_fixture_team_id AND player.player_id = tournament_fixture_team_player.player_id AND tournament_fixture_team.tournament_fixture_team_id = '.$tournament_fixture_team_id.' AND tournament_fixture_team_player_deleted = 0');
		$query = $this->db->get('player,tournament_fixture_team_player, tournament_fixture_team');
		
		return $query;
	}
	public function add_fixture_player($tournament_fixture_team_id)
	{
		$tournament_fixture_player_data = array(
			'player_id'=>$this->input->post('player_id'),
			'fixture_team_player_type_id'=>$this->input->post('fixture_player_type_id'),
			'added_by'=>$this->session->userdata('personnel_id'),
			'added_on' =>date('Y-m-d H:i:s'),
			'tournament_fixture_team_id'=>$tournament_fixture_team_id
			);
		if($this->db->insert('tournament_fixture_team_player', $tournament_fixture_player_data))
		{
			return $this->db->insert_id();
		}
		else
		{
			return FALSE;
		}
	}
	public function remove_league_team($tournament_fixture_team_id,$tournament_fixture_id)
	{
		$this->db->where('tournament_fixture_team_id = '.$tournament_fixture_team_id. ' AND tournament_fixture_id = '.$tournament_fixture_id);
		
		if($this->db->delete('tournament_fixture_team'))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	public function get_comment_fixtures($tournament_fixture_id)
	{
		$this->db->select('comment_tournament_fixture.*, comment.comment_name');
		$this->db->where('comment.comment_id = comment_tournament_fixture.comment_id AND comment_tournament_fixture.tournament_fixture_id = '.$tournament_fixture_id);
		$query = $this->db->get('comment,comment_tournament_fixture');
		return $query;
	}
	public function get_fixture_assessments($tournament_fixture_id)
	{
		$this->db->select('assessment_tournament_fixture.*, assessment.assessment_name');
		$this->db->where('assessment.assessment_id = assessment_tournament_fixture.assessment_id AND assessment_tournament_fixture.tournament_fixture_id = '.$tournament_fixture_id);
		$query = $this->db->get('assessment,assessment_tournament_fixture');
		return $query;
	}
	public function get_all_fixture_players_scorers($tournament_fixture_id)
	{
		$this->db->select('player.*, tournament_fixture_team_player.fixture_team_player_type_id');
		$this->db->where('tournament_fixture_team.tournament_fixture_team_id = tournament_fixture_team_player.tournament_fixture_team_id AND tournament_fixture_team_player.player_id = player.player_id AND tournament_fixture_team.tournament_fixture_id = '.$tournament_fixture_id);
		$query = $this->db->get('tournament_fixture_team_player, player,tournament_fixture_team');
		return $query;
	}
	public function get_all_fixture_fouls($tournament_fixture_id)
	{
		$this->db->select('player.*,team.team_name,tournament_foul.foul_minute, action.action_name, action.action_id,foul_type.foul_type_name,foul_type.foul_type_id, tournament_foul.tournament_foul_id');
		$this->db->where('tournament_foul.tournament_fixture_team_player_id = tournament_fixture_team_player.tournament_fixture_team_player_id AND tournament_fixture_team_player.player_id = player.player_id AND player.team_id = team.team_id AND tournament_foul.action_id = action.action_id AND tournament_foul.foul_type_id = foul_type.foul_type_id AND tournament_foul.tournament_fixture_id = '.$tournament_fixture_id.' AND tournament_foul.foul_deleted = 0');
		$query = $this->db->get('action,team,foul_type,player,tournament_foul,tournament_fixture_team_player');
		return $query;
	}
	public function get_all_fixture_fouls_commissioner($tournament_fixture_id)
	{
		$this->db->select('player.*,team.team_name,tournament_foul.foul_minute, action.action_name,foul_type.foul_type_name');
		$this->db->where('tournament_foul.tournament_fixture_team_player_id = tournament_fixture_team_player.tournament_fixture_team_player_id AND tournament_fixture_team_player.player_id = player.player_id AND player.team_id = team.team_id AND tournament_foul.action_id = action.action_id AND tournament_foul.foul_type_id = foul_type.foul_type_id AND tournament_foul.tournament_fixture_id = '.$tournament_fixture_id);
		$query = $this->db->get('action,team,foul_type,player,commissioner_tournament_foul AS tournament_foul,tournament_fixture_team_player');
		return $query;
	}
	public function get_all_goals_scored($tournament_fixture_id)
	{
		$this->db->select('player.*,team.team_name,tournament_goal.goal_minute, tournament_goal.tournament_goal_id, goal_type.goal_type_name, tournament_goal.goal_scored');
		$this->db->where('tournament_goal.tournament_fixture_team_player_id = tournament_fixture_team_player.tournament_fixture_team_player_id AND tournament_fixture_team_player.player_id = player.player_id AND player.team_id = team.team_id AND tournament_fixture_team.tournament_fixture_team_id = tournament_fixture_team_player.tournament_fixture_team_id AND tournament_fixture_team.tournament_fixture_id = '.$tournament_fixture_id.' AND tournament_goal.goal_type_id = goal_type.goal_type_id AND tournament_goal.goal_delete = 0');
		$query = $this->db->get('team,player,tournament_goal,tournament_fixture_team_player,tournament_fixture_team, goal_type');
		return $query;
	}

	public function get_all_goals_scored_commissioner($tournament_fixture_id)
	{
		$this->db->select('player.*,team.team_name,commissioner_tournament_goal.goal_minute, goal_type.goal_type_name, commissioner_tournament_goal.goal_scored');
		$this->db->where('commissioner_tournament_goal.tournament_fixture_team_player_id = tournament_fixture_team_player.tournament_fixture_team_player_id AND tournament_fixture_team_player.player_id = player.player_id AND player.team_id = team.team_id AND tournament_fixture_team.tournament_fixture_team_id = tournament_fixture_team_player.tournament_fixture_team_id AND tournament_fixture_team.tournament_fixture_id = '.$tournament_fixture_id.' AND commissioner_tournament_goal.goal_type_id = goal_type.goal_type_id');
		$query = $this->db->get('team,player,commissioner_tournament_goal,tournament_fixture_team_player,tournament_fixture_team, goal_type');
		return $query;
	}
	public function get_home_team($tournament_fixture_id)
	{
		$this->db->select('team.team_name');
		$this->db->where('tournament_fixture_team.tournament_team_id = tournament_team.tournament_team_id AND tournament_team.team_id = team.team_id AND tournament_fixture_team.fixture_team_type_id = 1 AND tournament_fixture_team.tournament_fixture_id = '.$tournament_fixture_id);
		$query = $this->db->get('tournament_fixture_team,tournament_team, team');
		$home_team = '';
		if($query->num_rows() > 0)
		{
			foreach($query->result() as $name)
			{
				$home_team = $name->team_name;
			}
		}
		return $home_team;
	}
	public function get_away_team($tournament_fixture_id)
	{
		$this->db->select('team.team_name');
		$this->db->where('tournament_fixture_team.tournament_team_id = tournament_team.tournament_team_id AND tournament_team.team_id = team.team_id AND tournament_fixture_team.fixture_team_type_id = 2 AND tournament_fixture_team.tournament_fixture_id = '.$tournament_fixture_id);
		$query = $this->db->get('tournament_fixture_team,tournament_team, team');
		$away_team = '';
		if($query->num_rows() > 0)
		{
			foreach($query->result() as $name)
			{
				$away_team = $name->team_name;
			}
		}
		return $away_team;
	}
	
	public function add_fixture_goals($tournament_fixture_id)
	{

		$goal_type_id = $this->input->post('goal_type_id');
		if($goal_type_id == 1)
		{
			$penalty_score_status = $this->input->post('penalty_score_status');
		}
		else
		{
			$penalty_score_status = 1;
		}

		$fixture_scores = array(
			'tournament_fixture_id'=>$fixture_id,
			'tournament_fixture_team_player_id'=>$this->input->post('tournament_fixture_team_player_id'),
			'goal_type_id'=>$this->input->post('goal_type_id'),
			'goal_minute'=>$this->input->post('goal_minute'),
			'goal_scored'=>$penalty_score_status,
			'added_by'=>$this->session->userdata('personnel_id'),
			'added_on' =>date('Y-m-d H:i:s')
			);
		if($this->db->insert('tournament_goal', $fixture_scores))
		{
			return $this->db->insert_id();
		}
		else
		{
			return FALSE;
		}
	}

	public function add_tournament_fixture_goals($tournament_fixture_id,$module)
	{
		if(!empty($module))
		{
			$goal_type_id = $this->input->post('goal_type_id');
			if($goal_type_id == 1)
			{
				$penalty_score_status = $this->input->post('penalty_score_status');
			}
			else
			{
				$penalty_score_status = 1;
			}

			$fixture_scores = array(
				'tournament_fixture_id'=>$tournament_fixture_id,
				'tournament_fixture_team_player_id'=>$this->input->post('tournament_fixture_team_player_id'),
				'goal_type_id'=>$this->input->post('goal_type_id'),
				'goal_minute'=>$this->input->post('goal_minute'),
				'goal_scored'=>$penalty_score_status,
				'added_by'=>$this->session->userdata('personnel_id'),
				'added_on' =>date('Y-m-d H:i:s')
				);
			if($this->db->insert('commissioner_tournament_goal', $fixture_scores))
			{
				return $this->db->insert_id();
			}
			else
			{
				return FALSE;
			}

		}
		else
		{
			$goal_type_id = $this->input->post('goal_type_id');
			if($goal_type_id == 1)
			{
				$penalty_score_status = $this->input->post('penalty_score_status');
			}
			else
			{
				$penalty_score_status = 1;
			}

			$fixture_scores = array(
				'tournament_fixture_id'=>$tournament_fixture_id,
				'tournament_fixture_team_player_id'=>$this->input->post('tournament_fixture_team_player_id'),
				'goal_type_id'=>$this->input->post('goal_type_id'),
				'goal_minute'=>$this->input->post('goal_minute'),
				'goal_scored'=>$penalty_score_status,
				'added_by'=>$this->session->userdata('personnel_id'),
				'added_on' =>date('Y-m-d H:i:s')
				);
			if($this->db->insert('tournament_goal', $fixture_scores))
			{
				return $this->db->insert_id();
			}
			else
			{
				return FALSE;
			}

		}
		
	}
	public function add_fixture_fouls($tournament_fixture_id,$module)
	{

		if(empty($module))
		{
			$fixture_fouls = array(
				'tournament_fixture_id'=>$tournament_fixture_id,
				'foul_type_id'=>$this->input->post('foul_type_id'),
				'action_id'=>$this->input->post('action_id'),
				'tournament_fixture_team_player_id'=>$this->input->post('foul_player_id'),
				'foul_minute'=>$this->input->post('foul_minute'),
				'added_by'=>$this->session->userdata('personnel_id'),
				'added_on' =>date('Y-m-d H:i:s')
				);
			if($this->db->insert('tournament_foul', $fixture_fouls))
			{
				return $this->db->insert_id();
			}
			else
			{
				return FALSE;
			}
		}
		else
		{
			$fixture_fouls = array(
				'tournament_fixture_id'=>$tournament_fixture_id,
				'foul_type_id'=>$this->input->post('foul_type_id'),
				'action_id'=>$this->input->post('action_id'),
				'tournament_fixture_team_player_id'=>$this->input->post('foul_player_id'),
				'foul_minute'=>$this->input->post('foul_minute'),
				'added_by'=>$this->session->userdata('personnel_id'),
				'added_on' =>date('Y-m-d H:i:s')
				);
			if($this->db->insert('commissioner_tournament_foul', $fixture_fouls))
			{
				return $this->db->insert_id();
			}
			else
			{
				return FALSE;
			}

		}
		
	}
	public function add_fixture_comments($tournament_fixture_id,$comment_id)
	{
		$fixture_comments = array(
			'tournament_fixture_id'=>$tournament_fixture_id,
			'comment_id'=>$comment_id,
			'comment_fixture_description'=>$this->input->post('comment_fixture_description'),
			'added_by'=>$this->session->userdata('personnel_id'),
			'added_on' =>date('Y-m-d H:i:s')
			);
		if($this->db->insert('comment_tournament_fixture', $fixture_comments))
		{
			return $this->db->insert_id();
		}
		else
		{
			return FALSE;
		}
	}
	public function add_fixture_assessmentss($tournament_fixture_id,$assessment_id)
	{
		$fixture_assessments = array(
			'tournament_fixture_id'=>$tournament_fixture_id,
			'assessment_id'=>$assessment_id,
			'assessment_fixture_description'=>$this->input->post('assessment_fixture_description'),
			'assessment_fixture_rating'=>$this->input->post('assessment_name'),
			'added_by'=>$this->session->userdata('personnel_id'),
			'added_on' =>date('Y-m-d H:i:s')
			);
		if($this->db->insert('assessment_tournament_fixture', $fixture_assessments))
		{
			return $this->db->insert_id();
		}
		else
		{
			return FALSE;
		}
	}
	public function get_league_teams($tournament_duration_id)
	{
		$this->db->select('*');
		$this->db->where('tournament_team.tournament_team_deleted = 0 AND team.team_id = tournament_team.team_id AND tournament_duration_id = '.$tournament_duration_id);
		$query = $this->db->get('tournament_team, team');
		return $query;
	}
	public function calculate_results($tournament_team_id)
	{
		$this->db->where('tournament_team_id = '.$tournament_team_id);
		$team_fixtures = $this->db->get('tournament_fixture_team');
		$wins = $draws = $losses = $goals_scored = $goals_against = 0;
		//echo $team_fixtures->num_rows();die();
		if($team_fixtures->num_rows() > 0)
		{
			//get opponents
			foreach($team_fixtures->result() as $fixtures_team)
			{
				$tournament_fixture_id = $fixtures_team->tournament_fixture_id;
				$this->db->where('tournament_team_id != '.$tournament_team_id.' AND tournament_fixture_id = '.$tournament_fixture_id);
				$opponent_fixture =$this->db->get('tournament_fixture_team');
				$team_goals = $this->calculate_fixture_goals($tournament_team_id);
				//var_dump($team_goals);die();
				$opponent_goals = 0;
				//echo $opponent_fixture->num_rows();die();
				if($opponent_fixture->num_rows() > 0)
				{
					$row = $opponent_fixture->row();
					$opponent_league_team_id = $row->tournament_team_id;
					//echo $opponent_league_team_id;die();
					$opponent_goals = $this->calculate_fixture_goals($opponent_league_team_id);
				}
				//var_dump($opponent_goals);die();
				//check wins
				if($team_goals > $opponent_goals)
				{
					$wins ++;
				}
				elseif($team_goals < $opponent_goals)
				{
					$losses++;
				}
				else
				{
					$draws++;
				}
				$goals_scored += $team_goals;
				$goals_against += $opponent_goals;
			}
		}
		$team_league_result = array(
			"wins"=>$wins,"losses"=>$losses,"draws"=>$draws,"goals_scored"=>$goals_scored,"goals_against"=>$goals_against);
			return $team_league_result;
	}
	public function calculate_fixture_goals($tournament_team_id)
	{
		$this->db->select('COUNT(tournament_goal_id) AS goals');
		$this->db-> where ('tournament_goal.tournament_fixture_team_player_id = tournament_fixture_team_player.tournament_fixture_team_player_id AND tournament_fixture_team_player.tournament_fixture_team_id = tournament_fixture_team.tournament_fixture_team_id AND tournament_fixture_team.tournament_team_id = '.$tournament_team_id);
		$result = $this->db->get('tournament_goal, tournament_fixture_team_player, tournament_fixture_team');
		$goasl= 0;
		if($result->num_rows()>0)
		{
			$row = $result->row();
			$goals = $row->goals;
		}
		return $goals;
	}
	function send_referee_to_admin($referee_id,$tournament_fixture_id)
	{
		//fixture_type  2 = tournament payments
		//insert details to the referee payments queue
		$referee_queue_data = array(
				'referee_id'=>$referee_id,
				'tournament_fixture_id'=>$tournament_fixture_id,
				'to_admin_sender'=>$this->session->userdata('personnel_id'),
				'to_admin_on'=>date('Y-m-d'),
				'referee_payment_status'=>0,
				'fixture_type'=>2
				);
		if($this->db->insert('referee_payments_queue', $referee_queue_data))
		{
			//update referee payment status to 1
			if($this->update_payment_to_admin($referee_id,$tournament_fixture_id,2))
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}
		else
		{
			return FALSE;
		}
	}
	function send_commissioner_to_admin($commissioner_id,$tournament_fixture_id)
	{
		//fixture_type  2 = tournament payments
		//insert details to the referee payments queue
		$commissioner_queue_data = array(
				'commissioner_id'=>$commissioner_id,
				'tournament_fixture_id'=>$tournament_fixture_id,
				'to_admin_sender'=>$this->session->userdata('personnel_id'),
				'to_admin_on'=>date('Y-m-d'),
				'commissioner_payment_status'=>0,
				'fixture_type'=>2
				);
		if($this->db->insert('commissioner_payments_queue', $commissioner_queue_data))
		{
			//update commissioner payment status to 1
			if($this->update_commissioner_payment_to_accounts($commissioner_id,$tournament_fixture_id,2,1))
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}
		else
		{
			return FALSE;
		}
	}
	function update_payment_to_admin($referee_id,$tournament_fixture_id,$fixture_type)
	{
		$data = array(
				'referee_payment_status' => 1
			);
		$this->db->where('referee_id = '.$referee_id.' AND tournament_fixture_id = '.$tournament_fixture_id.' AND fixture_type = '.$fixture_type);
		
		if($this->db->update('referee_payments_queue', $data))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	public function send_referee_to_accounts($referee_id,$tournament_fixture_id)
	{
		//update referee payment status to 2
		if($this->update_payment_to_accounts($referee_id,$tournament_fixture_id,2))
		{
			//insert details to the referee payments queue
			$referee_queue_data = array(
					'to_accounts_sender'=>$this->session->userdata('personnel_id'),
					'to_accounts_on'=>date('Y-m-d'),
					'referee_payment_status'=>2
					);
			//if($this->db->insert('referee_payments_queue', $referee_queue_data))
			$this->db->where('referee_id = '.$referee_id.' AND tournament_fixture_id = '.$tournament_fixture_id.' AND fixture_type = 2' );
			if($this->db->update('referee_payments_queue', $referee_queue_data))
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}
	}
	public function send_commissioner_to_accounts($commissioner_id,$tournament_fixture_id)
	{
		//update referee payment status to 2
		if($this->update_commissioner_payment_to_accounts($commissioner_id,$tournament_fixture_id,2,2))
		{
			//insert details to the referee payments queue
			$commissioner_queue_data = array(
					'to_accounts_sender'=>$this->session->userdata('personnel_id'),
					'to_accounts_on'=>date('Y-m-d'),
					'commissioner_payment_status'=>2
					);
			//if($this->db->insert('referee_payments_queue', $commissioner_queue_data))
			$this->db->where('commissioner_id = '.$commissioner_id.' AND tournament_fixture_id = '.$tournament_fixture_id.' AND fixture_type = 2' );
			if($this->db->update('commissioner_payments_queue', $commissioner_queue_data))
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}
	}
	function update_payment_to_accounts($referee_id,$tournament_fixture_id,$fixture_type)
	{
		$data = array(
				'referee_payment_status' => 2
			);
		$this->db->where('referee_id = '.$referee_id.' AND tournament_fixture_id = '.$tournament_fixture_id.' AND fixture_type = '.$fixture_type );
		

		if($this->db->update('referee_payments_queue', $data))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	function update_commissioner_payment_to_accounts($commissioner_id,$tournament_fixture_id,$fixture_type,$status = NULL)
	{
		$data = array(
				'commissioner_payment_status' => $status
			);
		$this->db->where('commissioner_id = '.$commissioner_id.' AND tournament_fixture_id = '.$tournament_fixture_id.' AND fixture_type = '.$fixture_type );
		

		if($this->db->update('commissioner_payments_queue', $data))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	public function send_payments_to_referee($referee_id,$tournament_fixture_id)
	{
		//tournament fixture are type 2 and league type 1
		$fixture_type = 2;
		//update to 3
		if($this->update_payment_to_paid($referee_id, $tournament_fixture_id, $fixture_type))
		{
			//get the position of that referee for the fixture
			$referee_type_id = $this->get_referee_type($referee_id,$tournament_fixture_id);
			//get the amount to be paid for that referee type
			$referee_amount = $this->get_referee_amount($referee_type_id);
			
			$referee_payment_data = array(
					'referee_id'=>$referee_id,
					'tournament_fixture_id'=>$tournament_fixture_id,
					'amount_paid'=>$referee_amount,
					'paid_by'=>$this->session->userdata('personnel_id'),
					'paid_on'=>date('Y-m-d'),
					'fixture_type' =>$fixture_type 
					);
					
			if($this->db->insert('tournament_fixture_referee_payment',$referee_payment_data))
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}
		
	}

	public function send_payments_to_commissioner($commissioner_id,$tournament_fixture_id)
	{
		//tournament fixture are type 2 and league type 1
		$fixture_type = 2;
		//update to 3
		if($this->update_commissioner_payment_to_accounts($commissioner_id, $tournament_fixture_id, $fixture_type,3))
		{
			//get the position of that commissioner for the fixture
			// $referee_type_id = $this->get_referee_type($referee_id,$tournament_fixture_id);
			//get the amount to be paid for that referee type
			$commissioner_amount = $this->get_commissioner_amount(2);
			
			$commissioner_payment_data = array(
					'commissioner_id'=>$commissioner_id,
					'tournament_fixture_id'=>$tournament_fixture_id,
					'amount_paid'=>$commissioner_amount,
					'paid_by'=>$this->session->userdata('personnel_id'),
					'paid_on'=>date('Y-m-d'),
					'fixture_type' =>$fixture_type 
					);
					
			if($this->db->insert('tournament_fixture_commissioner_payment',$commissioner_payment_data))
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}
		
	}

	
	function get_referee_type($referee_id,$tournament_fixture_id)
	{
		$referee_type_id = '';
		$this->db->select('referee_type_id');
		$this->db->where('referee_id = '.$referee_id.' AND tournament_fixture_id = '.$tournament_fixture_id);
		$query = $this->db->get('tournament_fixture_referee');
		$type_id = $query->row();
		 
		$referee_type_id = $type_id->referee_type_id;
		return $referee_type_id; 
	}
	function get_referee_amount($referee_type_id)
	{
		$referee_payment_amount = '';
		$this->db->select('referee_payment_amount');
		$this->db->where('referee_type_id = '.$referee_type_id);
		$query = $this->db->get('referee_payment');
		$amount = $query->row();
		
		$referee_payment_amount = $amount->referee_payment_amount;
		return $referee_payment_amount;
	}
	function get_commissioner_amount($commissioner_type_id)
	{
		$commissioner_payment_amount = '';
		$this->db->select('commissioner_payment_amount');
		$this->db->where('commissioner_type_id = '.$commissioner_type_id);
		$query = $this->db->get('commissioner_payment');
		$amount = $query->row();
		
		$commissioner_payment_amount = $amount->commissioner_payment_amount;
		return $commissioner_payment_amount;
	}
	function update_payment_to_paid($referee_id, $tournament_fixture_id, $fixture_type)
	{
		$data = array(
				'referee_payment_status' => 3
			);
		$this->db->where('referee_id = '.$referee_id.' AND tournament_fixture_id = '.$tournament_fixture_id.' AND fixture_type = '.$fixture_type);
		

		if($this->db->update('referee_payments_queue', $data))
		{
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
	public function pay_referee($referee_payments_queue_id)
	{
		$ref_payment_data = array(
			'referee_payments_queue_id'=>$referee_payments_queue_id,
			'payment_amount'=>$this->input->post('payment_amount'),
			'paid_by'=>$this->session->userdata('personnel_id'),
			'paid_on'=>date('Y-m-d H-i-s')
			);
		if($this->db->insert('referee_paid',$ref_payment_data))
		{
			//update the staus of the queue
			$this->db->where('referee_payments_queue_id = '.$referee_payments_queue_id);
			if($this->db->update('referee_payments_queue',array('referee_payment_status'=>3)))
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}
		else
		{
			return FALSE;
		}
	}
	public function pay_commissioner($commissioner_payments_queue_id)
	{
		$ref_payment_data = array(
			'commissioner_payments_queue_id'=>$commissioner_payments_queue_id,
			'payment_amount'=>$this->input->post('payment_amount'),
			'paid_by'=>$this->session->userdata('personnel_id'),
			'paid_on'=>date('Y-m-d H-i-s')
			);
		if($this->db->insert('commissioner_paid',$ref_payment_data))
		{
			//update the staus of the queue
			$this->db->where('commissioner_payments_queue_id = '.$commissioner_payments_queue_id);
			if($this->db->update('commissioner_payments_queue',array('commissioner_payment_status'=>3)))
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}
		else
		{
			return FALSE;
		}
	}
	public function get_referee_payment_status($referee_id,$tournament_fixture_id,$fixture_type)
	{
		$status = 0;
		$this->db->select('referee_payment_status');
		$this->db->where('referee_id = '.$referee_id.' AND fixture_type = '.$fixture_type.' AND tournament_fixture_id = '.$tournament_fixture_id);
		$query = $this->db->get('referee_payments_queue');
		
		if($query->num_rows() > 0)
		{
			$status_row = $query->row();
			$status = $status_row->referee_payment_status;
		}
		
		return $status;
	}
	public function get_commissioner_payment_status($commissioner_id,$tournament_fixture_id,$fixture_type)
	{
		$status = 0;
		$this->db->select('commissioner_payment_status');
		$this->db->where('commissioner_id = '.$commissioner_id.' AND fixture_type = '.$fixture_type.' AND tournament_fixture_id = '.$tournament_fixture_id);
		$query = $this->db->get('commissioner_payments_queue');
		
		if($query->num_rows() > 0)
		{
			$status_row = $query->row();
			$status = $status_row->commissioner_payment_status;
		}

		
		return $status;
	}
	public function get_ref_invoice($tournament_fixture_id,$referee_id)
	{
		$invoice_total = 0;
		$this->db->select('SUM(amount_paid) AS total_invoice');
		$this->db->where('tournament_fixture_id = '.$tournament_fixture_id.' AND referee_id = '.$referee_id);
		$query = $this->db->get('tournament_fixture_referee_payment');
		
		if($query->num_rows() > 0)
		{
			$invoice_row = $query->row();
			$invoice_total = $invoice_row->total_invoice;
		}
		return $invoice_total;
	}
	public function get_ref_payment($referee_payments_queue_id)
	{
		$payment_total = 0;
		$this->db->select('SUM(payment_amount) AS total_payment');
		$this->db->where('referee_payments_queue_id = '.$referee_payments_queue_id);
		$query = $this->db->get('referee_paid');
		
		if($query->num_rows() > 0)
		{
			$payment_row = $query->row();
			$payment_total = $payment_row->total_payment;
		}
		return $payment_total;
	}
	public function get_commissioner_ref_payment($commissioner_payments_queue_id)
	{
		$payment_total = 0;
		$this->db->select('SUM(payment_amount) AS total_payment');
		$this->db->where('commissioner_payments_queue_id = '.$commissioner_payments_queue_id);
		$query = $this->db->get('commissioner_paid');
		
		if($query->num_rows() > 0)
		{
			$payment_row = $query->row();
			$payment_total = $payment_row->total_payment;
		}
		return $payment_total;
	}
	public function get_ref_commissioner_invoice($tournament_fixture_id,$commissioner_id)
	{
		$invoice_total = 0;
		$this->db->select('SUM(amount_paid) AS total_invoice');
		$this->db->where('tournament_fixture_id = '.$tournament_fixture_id.' AND commissioner_id = '.$commissioner_id.' AND fixture_type = 2');
		$query = $this->db->get('tournament_fixture_commissioner_payment');
		
		if($query->num_rows() > 0)
		{
			$invoice_row = $query->row();
			$invoice_total = $invoice_row->total_invoice;
		}
		return $invoice_total;
	}
	public function get_fixture_detail($tournament_fixture_id)
	{
		$this->db->select('*');
		$this->db->where('tournament_fixture_id = '.$tournament_fixture_id.' AND venue.venue_id = tournament_fixture.venue_id');
		$query = $this->db->get('tournament_fixture, venue');
		
		return $query;
	}
	public function get_home_players($tournament_fixture_id)
	{
		$home_team = 0;
		$this->db->select('tournament_fixture_team.tournament_fixture_team_id');
		$this->db->where('tournament_fixture_team.tournament_team_id = tournament_team.tournament_team_id AND tournament_team.team_id = team.team_id AND fixture_team_type_id = 1 AND tournament_fixture_team.tournament_fixture_id = '.$tournament_fixture_id);
		$query = $this->db->get('tournament_fixture_team,tournament_team, team');
		$home_team = '';
		if($query->num_rows() > 0)
		{
			foreach($query->result() as $name)
			{
				$home_team = $name->tournament_fixture_team_id;
			}
		}
		//get all players forthat team
		$this->db->where('tournament_fixture_team_player.tournament_fixture_team_id = '.$home_team.' AND tournament_fixture_team_player.player_id = player.player_id AND tournament_fixture_team_player.fixture_team_player_type_id = fixture_player_type.fixture_player_type_id');
		$query = $this->db->get('fixture_player_type, tournament_fixture_team_player, player');
		
		return $query;
		
	}
	public function get_away_players($tournament_fixture_id)
	{
		$away = 0;
		$this->db->select('tournament_fixture_team.tournament_fixture_team_id');
		$this->db->where('tournament_fixture_team.tournament_team_id = tournament_team.tournament_team_id AND tournament_team.team_id = team.team_id AND fixture_team_type_id = 2 AND tournament_fixture_team.tournament_fixture_id = '.$tournament_fixture_id);
		$query = $this->db->get('tournament_fixture_team,tournament_team, team');
		if($query->num_rows() > 0)
		{
			foreach($query->result() as $name)
			{
				$away = $name->tournament_fixture_team_id;
			}
		}
		//get all players forthat team
		$this->db->where('tournament_fixture_team_player.tournament_fixture_team_id = '.$away.' AND tournament_fixture_team_player.player_id = player.player_id AND tournament_fixture_team_player.fixture_team_player_type_id = fixture_player_type.fixture_player_type_id');
		$query = $this->db->get('fixture_player_type, tournament_fixture_team_player, player');
		
		return $query;
		
	}
	public function get_ref_summary($fixture_id)
	{
		$this->db->where('ts_fixture_id = '.$fixture_id.' AND fixture_summary_deleted = 0');
		$query = $this->db->get('tornament_summary');
		
		return $query;
	}
	public function get_home_fixture_subs($fixture_id)
	{
		$this->db->where('t_subs.fixture_id = '.$fixture_id.' AND t_subs.player_in = player.player_id AND t_subs.sub_type = 1 AND sub_deleted = 0');
		$query = $this->db->get('t_subs,player');
		
		return $query;
	}
	public function get_away_fixture_subs($fixture_id)
	{
		$this->db->where('t_subs.fixture_id = '.$fixture_id.' AND t_subs.player_in = player.player_id AND t_subs.sub_type = 2 AND sub_deleted = 0');
		$query = $this->db->get('t_subs,player');
		
		return $query;
	}
	public function delete_league_fixture_summary($fixture_summary_id)
	{
		$data = array('fixture_summary_deleted'=>1, 'deleted_by'=>$this->session->userdata('personnel_id'),'deleted_on'=>date('Y-m-d'));
		$this->db->where('ts_id = '.$fixture_summary_id);
		if($this->db->update('tornament_summary',$data))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	public function delete_league_subs($sub_id)
	{
		$data = array('sub_deleted'=>1, 'deleted_by'=>$this->session->userdata('personnel_id'),'deleted_on'=>date('Y-m-d'));
		$this->db->where('sub_id = '.$sub_id);
		if($this->db->update('t_subs',$data))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	public function delete_league_foul($foul_id)
	{
		$data = array('foul_deleted'=>1, 'deleted_by'=>$this->session->userdata('personnel_id'),'deleted_on'=>date('Y-m-d'));
		$this->db->where('tournament_foul_id = '.$foul_id);
		if($this->db->update('tournament_foul',$data))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	public function delete_league_goal($goal_id)
	{
		$data = array('goal_delete'=>1, 'deleted_by'=>$this->session->userdata('personnel_id'),'deleted_on'=>date('Y-m-d'));
		$this->db->where('tournament_goal_id = '.$goal_id);
		if($this->db->update('tournament_goal',$data))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	public function get_all_penalty_scored($tournament_fixture_id)
	{
		//goal type _id 1 = penalty
		$this->db->select('player.*,team.team_name,tournament_goal.goal_minute, tournament_goal.goal_scored');
		$this->db->where('tournament_goal.tournament_fixture_team_player_id = tournament_fixture_team_player.tournament_fixture_team_player_id AND tournament_fixture_team_player.player_id = player.player_id AND player.team_id = team.team_id AND tournament_fixture_team.tournament_fixture_team_id = tournament_fixture_team_player.tournament_fixture_team_id AND tournament_fixture_team.tournament_fixture_id = '.$tournament_fixture_id.' AND tournament_goal.goal_type_id = 1 AND tournament_goal.goal_delete = 0');
		$query = $this->db->get('team,player,tournament_goal,tournament_fixture_team_player,tournament_fixture_team');
		return $query;
	}
	public function delete_fixture_player($player_fixture_id)
	{
		$data = array('tournament_fixture_team_player_deleted'=>1, 'deleted_by'=>$this->session->userdata('personnel_id'),'deleted_on'=>date('Y-m-d H-i-s'));
		$this->db->where('tournament_fixture_team_player_id = '.$player_fixture_id);
		if($this->db->update('tournament_fixture_team_player', $data))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
}
?>