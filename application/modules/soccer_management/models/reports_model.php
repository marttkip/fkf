<?php
class Reports_model extends CI_Model 
{
	public function get_all_tournament_payments($table, $where)
	{
		$this->db->select('tournament_fixture_referee_payment.tournament_fixture_id, tournament_fixture_referee_payment.paid_on, tournament_fixture_referee_payment.amount_paid, referee.referee_fname, referee.referee_onames,team.team_name, tournament_fixture.tournament_fixture_date');
		$this->db->where($where);
		$query = $this->db->get($table);
		return $query;
	}
	public function referee_tournament_payements()
	{
		$this->db->select('*');
		$this->db->where('referee.referee_id = tournament_fixture_referee_payment.referee_id');
		$query = $this->db->get('tournament_fixture_referee_payment, referee');
		return $query;
	}

	public function referee_league_payements($referee_id,$fixture_id)
	{
		$this->db->select('*');
		$this->db->where('referee.referee_id = tournament_fixture_referee_payment.referee_id AND tournament_fixture_referee_payment.tournament_fixture_id = '.$fixture_id.' AND tournament_fixture_referee_payment.referee_id = '.$referee_id);
		$query = $this->db->get('tournament_fixture_referee_payment, referee');
		return $query;
	}
	public function get_league_matches($date_from,$date_to)
	{
		if(!empty($date_from) OR !empty($date_to))
		{
			$where = ' AND fixture.fixture_date >= "'.$date_from.'"  AND fixture.fixture_date <= "'.$date_to.'"';
		}
		else
		{
			$where = '';
		}
		$this->db->select('*');
		$this->db->where('fixture.league_duration_id = league_duration.league_duration_id AND league_duration.league_id = league.league_id '.$where);
		$query = $this->db->get('league,fixture,league_duration');
		return $query;

	}

	public function get_league_matches_referee($date_from,$date_to)
	{
		if(!empty($date_from) OR !empty($date_to))
		{
			$where = ' AND fixture.fixture_date >= "'.$date_from.'"  AND fixture.fixture_date <= "'.$date_to.'"';
		}
		else
		{
			$where = '';
		}
		$this->db->select('*');
		$this->db->where('fixture.league_duration_id = league_duration.league_duration_id AND league_duration.league_id = league.league_id AND fixture_referee.fixture_id = fixture.fixture_id AND fixture_referee.referee_id = referee.referee_id AND fixture_referee.referee_type_id = referee_type.referee_type_id'.$where);
		$query = $this->db->get('league,fixture,league_duration,fixture_referee,referee,referee_type');
		return $query;

	}
	public function get_tournament_matches_referee($date_from,$date_to)
	{
		if(!empty($date_from) OR !empty($date_to))
		{
			$where = ' AND tournament_fixture.tournament_fixture_date >= "'.$date_from.'"  AND tournament_fixture.tournament_fixture_date <= "'.$date_to.'"';
		}
		else
		{
			$where = '';
		}
		$this->db->select('*');
		$this->db->where('tournament_fixture.tournament_duration_id = tournament_duration.tournament_duration_id AND tournament_duration.tournament_id = tournament.tournament_id AND tournament_fixture_referee.tournament_fixture_id = tournament_fixture.tournament_fixture_id AND tournament_fixture_referee.referee_id = referee.referee_id AND tournament_fixture_referee.referee_type_id = referee_type.referee_type_id'.$where);
		$query = $this->db->get('tournament,tournament_fixture,tournament_duration,tournament_fixture_referee,referee,referee_type');
		return $query;

	}
	public function get_tournament_matches($date_from,$date_to)
	{
		if(!empty($date_from) OR !empty($date_to))
		{
			$where = ' AND tournament_fixture.tournament_fixture_date >= "'.$date_from.'"  AND tournament_fixture.tournament_fixture_date <= "'.$date_to.'"';
		}
		else
		{
			$where = '';
		}
		$this->db->select('*');
		$this->db->where('tournament_fixture.tournament_duration_id = tournament_duration.tournament_duration_id AND tournament_duration.tournament_id = tournament.tournament_id'.$where);
		$query = $this->db->get('tournament,tournament_fixture,tournament_duration');
		return $query;

	}
	function getWeekDates($year, $week, $start=true)
	{
	    $from = date("Y-m-d", strtotime("{$year}-W{$week}-1")); //Returns the date of monday in week
	    $to = date("Y-m-d", strtotime("{$year}-W{$week}-7"));   //Returns the date of sunday in week
	 
	    // if($start) {
	        $response['from'] = $from;
	    // } else {
	        $response['to'] = $to;
	    // }
	    return $response;
	    //return "Week {$week} in {$year} is from {$from} to {$to}.";
	}
	public function tournament_fixture_details($tournament_fixture_id)
	{
		$this->db->select('tournament_fixture.*');
		$this->db->where('tournament_fixture_team.tournament_fixture_id = '.$tournament_fixture_id.' AND tournament_fixture_team.tournament_fixture_id = tournament_fixture.tournament_fixture_id');
		$query = $this->db->get('tournament_fixture, tournament_fixture_team');
		return $query;
	}

	public function tournament_fixture_details_report($tournament_fixture_id)
	{
		$this->db->select('tournament_fixture.*,referee.*');
		$this->db->where('tournament_fixture_team.tournament_fixture_id = '.$tournament_fixture_id.' AND tournament_fixture_team.tournament_fixture_id = tournament_fixture.tournament_fixture_id  AND referee.referee_id = tournament_fixture_referee.referee_id AND tournament_fixture_referee.referee_type_id = 1');
		$query = $this->db->get('tournament_fixture, tournament_fixture_team,tournament_fixture_referee,referee');
		return $query;
	}

	public function league_fixture_details($fixture_id)
	{
		$this->db->select('fixture.*,referee.*');
		$this->db->where('fixture_team.fixture_id = '.$fixture_id.' AND fixture_team.fixture_id = fixture.fixture_id AND fixture.fixture_id = fixture_referee.fixture_id AND referee.referee_id = fixture_referee.referee_id AND fixture_referee.referee_type_id = 1');
		$query = $this->db->get('fixture, fixture_team,fixture_referee,referee');
		return $query;
	}
	public function referee_type($referee_id, $tournament_fixture_id)
	{
		$referee_name = '';
		$this->db->select('referee_type.referee_type_name');
		$this->db->where('tournament_fixture_referee.referee_id = '.$referee_id.' AND tournament_fixture_referee.tournament_fixture_id ='.$tournament_fixture_id.' AND tournament_fixture_referee.referee_type_id = referee_type.referee_type_id');
		$query = $this->db->get('tournament_fixture_referee,referee_type');
		if($query->num_rows() > 0)
		{
			$name = $query->row();
			$referee_name = $name->referee_type_name;
		}
		return $referee_name;
	}
	
}
?>