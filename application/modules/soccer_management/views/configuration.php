<div class="row">
<?php $referee_type_id = set_value('referee_type_id');?>
	<div class="col-md-6">
    	<section class="panel">
            <header class="panel-heading">						
                <h2 class="panel-title">Edit referee payments</h2>
            </header>
            <div class="panel-body">
            	 <!-- Adding Errors -->
                    <?php
						$success = $this->session->userdata('success_message');
						$error = $this->session->userdata('error_message');
						
						if(!empty($success))
						{
							echo '
								<div class="alert alert-success">'.$success.'</div>
							';
							
							$this->session->unset_userdata('success_message');
						}
						
						if(!empty($error))
						{
							echo '
								<div class="alert alert-danger">'.$error.'</div>
							';
							
							$this->session->unset_userdata('error_message');
						}
						$validation_errors = validation_errors();
						
						if(!empty($validation_errors))
						{
							echo '<div class="alert alert-danger"> Oh snap! '.$validation_errors.' </div>';
						}
                    ?>
                    
            	<table class='table table-striped table-hover table-condensed'>
                    <form action="<?php echo site_url("soccer-management/configuration/add-referee-payment");?>" method="post">
                    <tr>
                    	<td><select class="form-control" name="referee_type_id">
                                <option value="">---Select Referee Type---</option>
                                <?php
                                    if($referee_types->num_rows()> 0)
                                    {
                                        foreach($referee_types->result() as $res)
                                        {
                                            $db_referee_type_id = $res->referee_type_id;
                                            $referee_type_name = $res->referee_type_name;
                                            
                                            if($db_referee_type_id == $referee_type_id)
                                            {
                                                echo '<option value="'.$db_referee_type_id.'" selected>'.$referee_type_name.'</option>';
                                            }
                                            
                                            else
                                            {
                                                echo '<option value="'.$db_referee_type_id.'">'.$referee_type_name.'</option>';
                                            }
                                        }
                                    }
                                ?>
                            </select>
                        </td>
                        <td><input type='text' name='payment_amount' class="form-control"></td>
                        <td colspan="2"><button class='btn btn-info btn-sm' type='submit'>Add payment</button></td>
                    </tr>
                    </form>
                <?php
                
                if($referee_payments->num_rows() > 0)
                {
                    foreach ($referee_payments->result() as $row2)
                    {
                        $referee_payment_id = $row2->referee_payment_id;
                        $payment_amount= $row2->referee_payment_amount;
						$referee_type_name= $row2->referee_type_name;
                        ?>
                        <form action="<?php echo site_url("soccer-management/configuration/edit-referee-payment/".$referee_payment_id);?>" method="post">
                        <tr>
                        	 <td><input type='text' name='referee_type_name<?php echo $referee_payment_id;?>' value='<?php echo $referee_type_name;?>' class="form-control" readonly></td>
                            <td><input type='text' name='payment_amount<?php echo $referee_payment_id;?>' value='<?php echo $payment_amount;?>' class="form-control"></td>
                            <td><button class='btn btn-success btn-xs' type='submit'><i class='fa fa-pencil'></i> Edit</button></td>
                            <td><a href="<?php echo site_url("soccer-management/configuration/delete-referee-payment/".$referee_payment_id);?>" onclick="return confirm('Do you want to delete <?php echo $payment_amount;?>?');" title="Delete <?php echo $payment_amount;?>"><button class='btn btn-danger btn-xs' type='button'><i class='fa fa-trash'></i> Delete</button></a></td>
                        </tr>
                        </form>
                        <?php
                    }
                }
                ?>
                </table>
            </div>
        </section>
    </div>
    <div class="col-md-6">
    	<section class="panel">
            <header class="panel-heading">						
                <h2 class="panel-title">Edit commissioner payments</h2>
            </header>
            <div class="panel-body">
            	<table class='table table-striped table-hover table-condensed'>
                    <form action="<?php echo site_url("soccer_management/configuration/add_new_referee_payment");?>" method="post">
                    <tr>
                    	<td><select class="form-control" name="referee_type_id">
                                <option value="">---Select Referee Type---</option>
                                <?php
                                    if($referee_types->num_rows()> 0)
                                    {
                                        foreach($referee_types->result() as $res)
                                        {
                                            $db_referee_type_id = $res->referee_type_id;
                                            $referee_type_name = $res->referee_type_name;
                                            
                                            if($db_referee_type_id == $referee_type_id)
                                            {
                                                echo '<option value="'.$db_referee_type_id.'" selected>'.$referee_type_name.'</option>';
                                            }
                                            
                                            else
                                            {
                                                echo '<option value="'.$db_referee_type_id.'">'.$referee_type_name.'</option>';
                                            }
                                        }
                                    }
                                ?>
                            </select>
                        </td>
                        <td><input type='text' name='payment_name' class="form-control"></td>
                        <td colspan="2"><button class='btn btn-info btn-sm' type='submit'>Add payment</button></td>
                    </tr>
                    </form>
                <?php
                
                if($referee_payments->num_rows() > 0)
                {
                    foreach ($referee_payments->result() as $row2)
                    {
                        $payment_id = $row2->referee_payment_id;
                        $payment_amount= $row2->referee_payment_amount;
						$referee_type_name= $row2->referee_type_name;
                        ?>
                        <form action="<?php echo site_url("soccer-management/configuration/edit-referee-payment/".$payment_id);?>" method="post">
                        <tr>
                        	 <td><input type='text' name='referee_type_name<?php echo $payment_id;?>' value='<?php echo $referee_type_name;?>' class="form-control" readonly></td>
                            <td><input type='text' name='payment_name<?php echo $payment_id;?>' value='<?php echo $payment_amount;?>' class="form-control" ></td>
                            <td><button class='btn btn-success btn-xs' type='submit'><i class='fa fa-pencil'></i> Edit</button></td>
                            <td><a href="<?php echo site_url("soccer-management/configuration/delete-referee-payment/".$payment_id);?>" onclick="return confirm('Do you want to delete <?php echo $payment_amount;?>?');" title="Delete <?php echo $payment_amount;?>"><button class='btn btn-danger btn-xs' type='button'><i class='fa fa-trash'></i> Delete</button></a></td>
                        </tr>
                        </form>
                        <?php
                    }
                }
                ?>
                </table>
            </div>
        </section>
    </div>
</div>