<?php
		
		$result = '';
		
		//if users exist display them
		if ($query->num_rows() > 0)
		{
			$count = $page;
			
			$result .= 
			'
			<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th><a href="'.site_url().'soccer-management/league/league_name/'.$order_method.'/'.$page.'">League name</a></th>
						<th><a href="'.site_url().'soccer-management/league/league_status/'.$order_method.'/'.$page.'">Status</a></th>
						<th colspan="4">Actions</th>
					</tr>
				</thead>
				  <tbody>
				  
			';
			
			//get all administrators
			$administrators = $this->users_model->get_active_users();
			if ($administrators->num_rows() > 0)
			{
				$admins = $administrators->result();
			}
			
			else
			{
				$admins = NULL;
			}
			
			foreach ($query->result() as $row)
			{
				$league_id = $row->league_id;
				$league_name = $row->league_name;
				$league_status = $row->league_status;
				//status
				if($league_status == 1)
				{
					$status = 'Active';
				}
				else
				{
					$status = 'Disabled';
				}
				
				//create deactivated status display
				if($league_status == 0)
				{
					$status = '<span class="label label-default">Deactivated</span>';
					$button = '<a class="btn btn-info" href="'.site_url().'soccer-management/activate-league/'.$league_id.'" onclick="return confirm(\'Do you want to activate '.$league_name.'?\');" title="Activate '.$league_name.'"><i class="fa fa-thumbs-up"></i>Activate</a>';
				}
				//create activated status display
				else if($league_status == 1)
				{
					$status = '<span class="label label-success">Active</span>';
					$button = '<a class="btn btn-default" href="'.site_url().'soccer-management/deactivate-league/'.$league_id.'" onclick="return confirm(\'Do you want to deactivate '.$league_name.'?\');" title="Deactivate '.$league_name.'"><i class="fa fa-thumbs-down"></i>Deactivate</a>';
				}
				$job_title_id = $this->session->userdata('job_title_id');
				// var_dump($job_title_id); die();
				if($job_title_id == 15)
				{
					$access = '<td><a href="'.site_url().'soccer-management/add-league-duration/'.$league_id.'" class="btn btn-sm btn-success" title="Add schedule for '.$league_name.'"><i class="fa fa-plus"></i> Seasons</a></td>
						      ';
				}
				else
				{
					$access = '
								<td><a href="'.site_url().'soccer-management/edit-league/'.$league_id.'" class="btn btn-sm btn-success" title="Edit '.$league_name.'"><i class="fa fa-pencil"></i>Edit League</a></td>
								<td>'.$button.'</td>
								<td><a href="'.site_url().'soccer-management/delete-league/'.$league_id.'" class="btn btn-sm btn-danger" onclick="return confirm(\'Do you really want to delete '.$league_name.'?\');" title="Delete '.$league_name.'"><i class="fa fa-trash"> Delete</i></a></td>
								<td><a href="'.site_url().'soccer-management/add-league-duration/'.$league_id.'" class="btn btn-sm btn-success" title="Add schedule for '.$league_name.'"><i class="fa fa-plus"></i> Seasons</a></td>
						      ';
				}
				
				$count++;
				$result .= 
				'
					<tr>
						<td>'.$count.'</td>
						<td>'.$league_name.'</td>
						<td>'.$status.'</td>
						'.$access.'
					</tr> 
				';
			}
			
			$result .= 
			'
						  </tbody>
						</table>
			';
		}
		
		else
		{
			$result .= "There are no league";
		}
?>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Search league</h2>
    </header>
    
    <!-- Widget content -->
   <div class="panel-body">
    	<div class="padd">
			<?php
            echo form_open("soccer_management/league/search_league", array("class" => "form-horizontal"));
            ?>
            <div class="row">
                <div class="form-group">
                    <label class="col-md-4 control-label">League name: </label>
                    
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="league_name" placeholder="League names">
                    </div>
                </div>
        
                <div class="row">
                    <div class="col-md-8 col-md-offset-4">
                        <div class="center-align">
                            <button type="submit" class="btn btn-info btn-sm">Search</button>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            echo form_close();
            ?>
    	</div>
    </div>
</section>
						<section class="panel">
							<header class="panel-heading">						
								<h2 class="panel-title"><?php echo $title;?></h2>
							</header>
							<div class="panel-body">
                            	<?php
								$search = $this->session->userdata('league_search_title2');
								
								if(!empty($search))
								{
									echo '<h6>Filtered by: '.$search.'</h6>';
									echo '<a href="'.site_url().'soccer_management/league/close_search" class="btn btn-sm btn-info pull-left">Close search</a>';
								}
                                $success = $this->session->userdata('success_message');
		
								if(!empty($success))
								{
									echo '<div class="alert alert-success"> <strong>Success!</strong> '.$success.' </div>';
									$this->session->unset_userdata('success_message');
								}
								
								$error = $this->session->userdata('error_message');
								
								if(!empty($error))
								{
									echo '<div class="alert alert-danger"> <strong>Oh snap!</strong> '.$error.' </div>';
									$this->session->unset_userdata('error_message');
								}
								?>
                            	<div class="row" style="margin-bottom:20px;">
                                    <!--<div class="col-lg-2 col-lg-offset-8">
                                        <a href="<?php echo site_url();?>soccer-management/export-league" class="btn btn-sm btn-success pull-right">Export</a>
                                    </div>-->
                                    <div class="col-lg-12">
                                    	<a href="<?php echo site_url();?>soccer-management/add-league" class="btn btn-sm btn-info pull-right">Add League</a>
                                    </div>
                                </div>
								<div class="table-responsive">
                                	
									<?php echo $result;?>
							
                                </div>
							</div>
                            <div class="panel-footer">
                            	<?php if(isset($links)){echo $links;}?>
                            </div>
						</section>