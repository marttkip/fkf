<?php
$commissioner_id = set_value('commissioner_id');
$result = '';
if($fixture_commissioners->num_rows() > 0)
{
	$count = 0;
	$result .= 
			'<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th>Commissioner name</th>
						<th>Action</th>
					</tr>
				</thead>
				  <tbody>
			';
	foreach($fixture_commissioners->result() as $commissioners)
	{
		$personnel_id = $commissioners->personnel_id;
		$personnel_fname = $commissioners->personnel_fname;
		$personnel_onames = $commissioners->personnel_onames;
		$personnel_name = $personnel_fname. ' '.$personnel_onames;
		$count++;
		
		$result .=
				'
				<tr>
					<td>'.$count.'</td>
					<td>'.$personnel_name.'</td>
					<td></td>
				</tr>';
	}
}
else
{
	$result .= 'No commissioner added to the fixture';
}
?>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title"><?php echo $title;?></h2>
    </header>
    <div class="panel-body">
        <div class="row" style="margin-bottom:20px;">
            <div class="col-lg-12">
                <a href="<?php echo base_url().'soccer-management/add-league-duration-fixture/'.$league_duration_id.'/'.$league_id;?>" class="btn btn-info pull-right">Back to fixture</a>
            </div>
        </div>
            
        <!-- Adding Errors -->
        <?php
            $success = $this->session->userdata('success_message');
            $error = $this->session->userdata('error_message');
            
            if(!empty($success))
            {
                echo '
                    <div class="alert alert-success">'.$success.'</div>
                ';
                
                $this->session->unset_userdata('success_message');
            }
            
            if(!empty($error))
            {
                echo '
                    <div class="alert alert-danger">'.$error.'</div>
                ';
                
                $this->session->unset_userdata('error_message');
            }
            $validation_errors = validation_errors();
            
            if(!empty($validation_errors))
            {
                echo '<div class="alert alert-danger"> Oh snap! '.$validation_errors.' </div>';
            }
        ?>
        
        <?php echo form_open($this->uri->uri_string(), array("class" => "form-horizontal", "role" => "form"));?>
        <div class="row">
            <div class="col-md-6">
            	<div class="form-group">
                    <label class="col-lg-5 control-label">Commissioner: </label>
                    <div class="col-lg-7">
                        <select class="form-control" name="commissioner_id">
                            <?php
                                if($personnel_commissioners->num_rows()> 0)
                                {
									echo '<option value="--Select Commissioner--" selected>--Select Commissioner--</option>';
                                    foreach($personnel_commissioners->result() as $res)
                                    {
                                        $db_commissioner_id = $res->personnel_id;
                                        $commissioner_fname = $res->personnel_fname;
                                        $commissioner_onames = $res->personnel_onames;
                                        $commissioner_name = $commissioner_fname. ' ' .$commissioner_onames;
                                        
                                        if($db_commissioner_id == $commissioner_id)
                                        {
                                            echo '<option value="'.$db_commissioner_id.'" selected>'.$commissioner_name.'</option>';
                                        }
                                        
                                        else
                                        {
                                            echo '<option value="'.$db_commissioner_id.'">'.$commissioner_name.'</option>';
                                        }
                                    }
                                }
                            ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
            </div>
        </div>
        <div class="row" style="margin-top:10px;">
            <div class="col-md-12">
                <div class="form-actions center-align">
                    <button class="submit btn btn-primary" type="submit">
                        Add commmissioner
                    </button>
                </div>
            </div>
        </div>
        <?php echo form_close();?>
    </div>
</section>
<section class="panel">

    <header class="panel-heading">
        <h2 class="panel-title">Commissioner for <?php echo $fixture_id;?></h2>
    </header>
    <div class="panel-body">
    	<div class="table-responsive">
            
            <?php echo $result;?>
    
        </div>
    </div>
</section>