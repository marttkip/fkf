<?php
//league data
$row = $league->row();

$league_name = $row->league_name;

//repopulate data if validation errors occur
$validation_error = validation_errors();
				
if(!empty($validation_error))
{
	$league_names = set_value('league_name');
}
?>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">About <?php echo $league_name;?></h2>
    </header>
    <div class="panel-body">
    <!-- Adding Errors -->
    <?php
    if(isset($error)){
        echo '<div class="alert alert-danger"> Oh snap! Change a few things up and try submitting again. </div>';
    }
    if(!empty($validation_errors))
    {
        echo '<div class="alert alert-danger"> Oh snap! '.$validation_errors.' </div>';
    }

    ?>
            
<?php echo form_open($this->uri->uri_string(), array("class" => "form-horizontal", "role" => "form"));?>
<div class="row">
	<div class="col-md-6">
    	<div class="form-group">
            <label class="col-lg-5 control-label">League name: </label>
            
            <div class="col-lg-7">
            	<input type="text" class="form-control" name="league_name" placeholder="League name" value="<?php echo $league_name;?>">
            </div>
        </div>
	</div>
</div>
<div class="row" style="margin-top:10px;">
	<div class="col-md-12">
        <div class="form-actions center-align">
            <button class="submit btn btn-primary" type="submit">
                Edit league
            </button>
        </div>
    </div>
</div>
            <?php echo form_close();?>
                </div>
            </section>