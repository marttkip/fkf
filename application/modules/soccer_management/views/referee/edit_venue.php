<?php
if($venue->num_rows() > 0)
{
	$venue_row = $venue->row();
	$venue_name = $venue_row->venue_name;
	$venue_location = $venue_row->venue_location;
	$venue_longitude = $venue_row->venue_longitude;
	$venue_latitude = $venue_row->venue_latitude;
}
$validation_error = validation_errors();
				
if(!empty($validation_error))
{
	$venue_name = set_value('venue_name');
	$venue_location = set_value('venue_location');
	$venue_longitude = set_value('venue_longitude');
	$venue_latitude = set_value('venue_latitude');
	}
?>
<section class="panel">
                <header class="panel-heading">
                    <h2 class="panel-title"><?php echo $title;?></h2>
                </header>
                <div class="panel-body">
                	<div class="row" style="margin-bottom:20px;">
                        <div class="col-lg-12">
                            <a href="<?php echo site_url();?>soccer-management/venues" class="btn btn-info pull-right">Back to venue</a>
                        </div>
                    </div>
                        
                    <!-- Adding Errors -->
                    <?php
						$success = $this->session->userdata('success_message');
						$error = $this->session->userdata('error_message');
						
						if(!empty($success))
						{
							echo '
								<div class="alert alert-success">'.$success.'</div>
							';
							
							$this->session->unset_userdata('success_message');
						}
						
						if(!empty($error))
						{
							echo '
								<div class="alert alert-danger">'.$error.'</div>
							';
							
							$this->session->unset_userdata('error_message');
						}
						$validation_errors = validation_errors();
						
						if(!empty($validation_errors))
						{
							echo '<div class="alert alert-danger"> Oh snap! '.$validation_errors.' </div>';
						}
                    ?>
                    
                    <?php echo form_open($this->uri->uri_string(), array("class" => "form-horizontal", "role" => "form"));?>
<div class="row">
	<div class="col-md-6">
    	<div class="form-group">
            <label class="col-lg-5 control-label">Venue name: </label>
            
            <div class="col-lg-7">
            	<input type="text" class="form-control" name="venue_name" placeholder="Venue name" value="<?php echo $venue_name;?>">
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg-5 control-label">Venue latitude: </label>
            
            <div class="col-lg-7">
            	<input type="text" class="form-control" name="venue_latitude" placeholder="Venue latitude" value="<?php echo $venue_latitude;?>">
            </div>
        </div>
	</div>
    <div class="col-md-6">
    	<div class="form-group">
            <label class="col-lg-5 control-label">Venue Locaction: </label>
            
            <div class="col-lg-7">
            	<input type="text" class="form-control" name="venue_location" placeholder="Venue Locaction" value="<?php echo $venue_location;?>">
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg-5 control-label">Venue longitude: </label>
            
            <div class="col-lg-7">
            	<input type="text" class="form-control" name="venue_longitude" placeholder="Venue longitude" value="<?php echo $venue_longitude;?>">
            </div>
        </div>
	</div>
</div>
<div class="row" style="margin-top:10px;">
	<div class="col-md-12">
        <div class="form-actions center-align">
            <button class="submit btn btn-primary" type="submit">
                Add venue
            </button>
        </div>
    </div>
</div>
                    <?php echo form_close();?>
                </div>
            </section>