<?php
$fixture_team_player_id = set_value('fixture_team_player_id');
$foul_player_id = set_value('foul_player_id');
$foul_minute = set_value('foul_minute');
$action_id= set_value('action_id');
$foul_type_id= set_value('foul_type_id');
$goal_minute = set_value('goal_minute');
$goal_type_id = set_value('goal_type_id');
$result = '';
$away_substitutes_result = '';
$ref_sum_results = '';
$home_substitutes_result = '';
$match_timings_result = '';
$foul_result = '';

if($match_timings->num_rows() > 0)
{
	$match_timings_count = 0;
	$match_timings_result.=
			'<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>Time</th>
						<th>First Half</th>
						<th>Second Half</th>
					</tr>
				</thead>
				<tbody>
				';
	foreach($match_timings->result() as $timings)
	{
		$match_timings_count++;
		$first_half_start_time = $timings->first_half_start_time;
		$first_half_end_time = $timings->first_half_end_time;
		$second_half_start_time = $timings->second_half_start_time;
		$second_half_end_time = $timings->second_half_end_time;
		
		$match_timings_result .=
					'
						<tr>
							<td>Start Time</td>
							<td>'.$first_half_start_time.'</td>
							<td>'.$second_half_start_time.'</td>
						</tr>
						<tr>
							<td>End Time</td>
							<td>'.$first_half_end_time.'</td>
							<td>'.$second_half_end_time.'</td>
						</tr>
					';
	}
	$match_timings_result.=
					'
					</tbody>
				</table>
					';
}
if($ref_summary->num_rows() > 0)
{
	$count = 0;
	$ref_sum_results .=
			'<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th>Comments</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
				';
	foreach($ref_summary->result() as $summary)
	{
		$count++;
		$referee_summary = $summary->summary;
		$fixture_summary_id = $summary->fixture_summary_id;
		
		$ref_sum_results .=
					'
					<tr>
						<td>'.$count.'</td>
						<td>'.$referee_summary.'</td>
						<td><a href="'.site_url().'soccer-management/delete-ref-summary/'.$fixture_summary_id.'/'.$fixture_id.'" class="btn btn-sm btn-danger" onclick="return confirm(\'Do you really want to delete this summary?\');" title="Delete Summary"><i class="fa fa-trash"> Delete</i></a></td>
					</tr>
					';
		
	}
	$ref_sum_results .='</tbody>
				</table>';
}
else
{
	$ref_sum_results .= 'No comments were added by the commissioning referee';
}
if($home_substitutes->num_rows() > 0)
{
	$count = 0;
	$home_substitutes_result.='
			<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th>Player In</th>
						<th>Player Out</th>
						<th>MInutes</th>
						<th colspan=2>Action</th>
					</tr>
				</thead>
				<tbody>';
	foreach($home_substitutes->result() as $subs)
	{
		$count++;
		$home_player_in = $subs->player_in;
		$player_in_name = $this->league_model->get_player_name($home_player_in);
		$home_player_out = $subs->player_out;
		$player_out_name = $this->league_model->get_player_name($home_player_out);
		$home_player_min = $subs->minute;
		$home_sub_id = $subs->sub_id;
		
		$home_substitutes_result.='
					<tr>
						<td>'.$count.'</td>
						<td>'.$player_in_name.'</td>
						<td>'.$player_out_name.'</td>
						<td>'.$home_player_min.'</td>
						<td>
							<a class="btn btn-sm btn-warning" title="Edit"  data-toggle="modal" data-target="#add_subs'.$home_sub_id.'"><i class="fa fa-pencil"></i></a>
							<div class="modal fade" id="add_subs'.$home_sub_id.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
						        <div class="modal-dialog" role="document">
						            <div class="modal-content">
						                <div class="modal-header">
						                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						                    <h4 class="modal-title" id="myModalLabel">Make Substitution</h4>
						                </div>
						                <div class="modal-body">';
						                   

						                   $home_substitutes_result.= '
						                    <div class = "row">
						                        <div class = "col-md-12">
						                          
				                                     '.form_open(base_url().'soccer_management/referee/edit-home-substitute/'.$fixture_id.'/'.$home_sub_id, array("class" => "form-horizontal", "role" => "form")).'
				                                    <div class="form-group">
				                                        <label class="col-lg-5 control-label">Minute: </label>
				                                        
				                                        <div class="col-lg-7">
				                                            <input type="text" class="form-control" name="home_minute'.$home_sub_id.'" placeholder="minute" value="'.$home_player_min.'">
				                                        </div>
				                                    </div>
				                                    <div class="form-group">
				                                        <label class="col-lg-5 control-label">Player Out: </label>
				                                        
				                                        <div class="col-lg-7">
				                                            <select class="form-control" name="home_player_out'.$home_sub_id.'">';
				                                               
				                                                    if($team_details_home_out->num_rows() > 0)
				                                                    {
				                                                        $gender = $team_details_home_out->result();
				                                                        
				                                                        foreach($gender as $res)
				                                                        {
				                                                            $player_id = $res->player_id;
				                                                            $player_fname = $res->player_fname;
				                                                            $player_onames = $res->player_onames;
				                                                            $player_name = $player_fname.' '.$player_onames;
				                                                            
				                                                            if($player_id == $home_player_out)
				                                                            {
				                                                                $home_substitutes_result.= '<option value="'.$player_id.'" selected>'.$player_name.'</option>';
				                                                            }
				                                                            
				                                                            else
				                                                            {
				                                                                $home_substitutes_result.= '<option value="'.$player_id.'">'.$player_name.'</option>';
				                                                            }
				                                                        }
				                                                    }
				                                                $home_substitutes_result.= '
				                                            </select>
				                                        </div>
				                    
				                                    </div>
				                                    <div class="form-group">
				                                        <label class="col-lg-5 control-label">Player In: </label>
				                                        
				                                        <div class="col-lg-7">
				                                            <select class="form-control" name="home_player_in'.$home_sub_id.'">';
				                                               
				                                                    if($team_details_home_in->num_rows() > 0)
				                                                    {
				                                                        $gender = $team_details_home_in->result();
				                                                        
				                                                        foreach($gender as $res)
				                                                        {
				                                                            $player_id = $res->player_id;
				                                                            $player_fname = $res->player_fname;
				                                                            $player_onames = $res->player_onames;
				                                                            $player_name = $player_fname.' '.$player_onames;
				                                                            
				                                                            if($player_id == $home_player_in)
				                                                            {
				                                                                $home_substitutes_result.= '<option value="'.$player_id.'" selected>'.$player_name.'</option>';
				                                                            }
				                                                            
				                                                            else
				                                                            {
				                                                                $home_substitutes_result.= '<option value="'.$player_id.'">'.$player_name.'</option>';
				                                                            }
				                                                        }
				                                                    }
				                                              $home_substitutes_result.= '
				                                            </select>
				                                        </div>
				                                    </div>
				                                    <div class="row" style="margin-top:10px;">
				                                        <div class="form-actions center-align">
				                                            <button class="submit btn btn-success" type="submit">
				                                                Edit Home Substitute
				                                            </button>
				                                        </div>
				                                    </div>
				                                   '.form_close().'
						                               
						                        </div>
						                        
						                    </div>
						                </div>
						            </div>
						        </div>
						    </div>
						</td>
						<td><a href="'.site_url().'soccer-management/delete-sub/'.$home_sub_id.'/'.$fixture_id.'" class="btn btn-sm btn-danger" onclick="return confirm(\'Do you really want to delete this substitution?\');" title="Delete Sub"><i class="fa fa-trash"></i></a></td>
					</tr>';
	}
	$home_substitutes_result.=
				'
				</tbody>
			</table>
			';
}
else
{
	$home_substitutes_result.= 'No substitues made for '.$home_team;
}
if($away_substitutes->num_rows() > 0)
{
	$count = 0;
	$away_substitutes_result.='
			<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th>Player In</th>
						<th>Player Out</th>
						<th>MInutes</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>';
	foreach($away_substitutes->result() as $awaysubs)
	{
		$count++;
		$away_home_player_in = $awaysubs->player_in;
		$away_player_in_name = $this->league_model->get_player_name($away_home_player_in);
		$away_home_player_out = $awaysubs->player_out;
		$away_player_out_name = $this->league_model->get_player_name($away_home_player_out);
		$away_home_player_min = $awaysubs->minute;
		$away_sub_id = $awaysubs->sub_id;
		
		$away_substitutes_result.='
					<tr>
						<td>'.$count.'</td>
						<td>'.$away_player_in_name.'</td>
						<td>'.$away_player_out_name.'</td>
						<td>'.$away_home_player_min.'</td>
						<td>
							<a class="btn btn-sm btn-warning" title="Edit"  data-toggle="modal" data-target="#add_subs'.$away_sub_id.'"><i class="fa fa-pencil"></i></a>
							<div class="modal fade" id="add_subs'.$away_sub_id.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
						        <div class="modal-dialog" role="document">
						            <div class="modal-content">
						                <div class="modal-header">
						                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						                    <h4 class="modal-title" id="myModalLabel">Make Substitution</h4>
						                </div>
						                <div class="modal-body">
						                    <div class="row">
						                        <div class ="col-md-12">
						                          
				                                    '.form_open(base_url().'soccer_management/referee/edit-away-substitute/'.$fixture_id.'/'.$away_sub_id, array("class" => "form-horizontal", "role" => "form")).'
					                                    <div class="form-group">
					                                        <label class="col-lg-5 control-label">Minute: </label>
					                                        
					                                        <div class="col-lg-7">
					                                            <input type="text" class="form-control" name="away_minute'.$away_sub_id.'" placeholder="minute" value="'.$away_home_player_min.'">
					                                        </div>
					                                    </div>
					                                    <div class="form-group">
					                                        <label class="col-lg-5 control-label">Player Out: </label>
					                                        
					                                        <div class="col-lg-7">
					                                            <select class="form-control" name="away_player_out'.$away_sub_id.'">';
					                                                
					                                                    if($team_details_away_out->num_rows() > 0)
					                                                    {
					                                                        $away_out = $team_details_away_out->result();
					                                                        
					                                                        foreach($away_out as $away_res)
					                                                        {
					                                                            $player_id = $away_res->player_id;
					                                                            $player_fname = $away_res->player_fname;
					                                                            $player_onames = $res->player_onames;
					                                                            $player_name = $player_fname.' '.$player_onames;
					                                                            
					                                                            if($player_id == $away_home_player_out)
					                                                            {
					                                                                $away_substitutes_result.= '<option value="'.$player_id.'" selected>'.$player_name.'</option>';
					                                                            }
					                                                            
					                                                            else
					                                                            {
					                                                                $away_substitutes_result.= '<option value="'.$player_id.'">'.$player_name.'</option>';
					                                                            }
					                                                        }
					                                                    }
					                                             $away_substitutes_result.= '
					                                            </select>
					                                        </div>

					                                    </div>
					                                    <div class="form-group">
					                                        <label class="col-lg-5 control-label">Player In: </label>
					                                        
					                                        <div class="col-lg-7">
					                                            <select class="form-control" name="away_player_in'.$away_sub_id.'">';
					                                                
					                                                    if($team_details_away_in->num_rows() > 0)
					                                                    {
					                                                        $away_in = $team_details_away_in->result();
					                                                        
					                                                        foreach($away_in as $away_res)
					                                                        {
					                                                            $player_id = $res->player_id;
					                                                            $player_fname = $res->player_fname;
					                                                            $player_onames = $res->player_onames;
					                                                            $player_name = $player_fname.' '.$player_onames;
					                                                            
					                                                            if($player_id == $away_home_player_in)
					                                                            {
					                                                                $away_substitutes_result.= '<option value="'.$player_id.'" selected>'.$player_name.'</option>';
					                                                            }
					                                                            
					                                                            else
					                                                            {
					                                                                $away_substitutes_result.= '<option value="'.$player_id.'">'.$player_name.'</option>';
					                                                            }
					                                                        }
					                                                    }
					                                             $away_substitutes_result.= '
					                                            </select>
					                                        </div>
					                                    </div>
					                                    <div class="row" style="margin-top:10px;">
					                                        <div class="form-actions center-align">
					                                            <button class="submit btn btn-danger" type="submit">
					                                                Edit Away Substitute
					                                            </button>
					                                        </div>
					                                    </div>
				                                   '.form_close().'
						                               
						                        </div>
						                        
						                    </div>
						                </div>
						            </div>
						        </div>
						    </div>
						</td>
						<td><a href="'.site_url().'soccer-management/delete-sub/'.$away_sub_id.'/'.$fixture_id.'" class="btn btn-sm btn-danger" onclick="return confirm(\'Do you really want to delete this substitution?\');" title="Delete Sub"><i class="fa fa-trash">Delete</i></a></td>
					</tr>';
	}
	$away_substitutes_result.=
				'
				</tbody>
			</table>
			';
}
else
{
	$away_substitutes_result.= 'No substitues made for '.$away_team;
}
if($fixture_fouls->num_rows() > 0)
{
	$count = 0;
	$foul_result .=
				'
				<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th>Team</th>
						<th>Number</th>
						<th>Player name</th>
						<th>Minute</th>
						<th>Reason</th>
						<th>Action</th>
						<th colspan=2>Actions</th>
					</tr>
				</thead>
				  <tbody>
				';
	foreach($fixture_fouls->result() as $fouls_committed)
	{
		$action_name = $fouls_committed->action_name;
		$action_id = $fouls_committed->action_id;
		$foul_type_name = $fouls_committed->foul_type_name;
		$foul_type_idd = $fouls_committed->foul_type_id;
		$foul_team_name = $fouls_committed->team_name;
		$foul_player_number = $fouls_committed->player_number;
		$player_fname = $fouls_committed->player_fname;
		$player_onames = $fouls_committed->player_onames;
		$foul_player_name = $player_fname. ' ' .$player_onames;
		$foul_minute_time = $fouls_committed->foul_minute;
		$foul_id = $fouls_committed->foul_id;
		$team_id = $fouls_committed->team_id;
		$count++;
		$foul_result .=
					'
					<tr>
						<td>'.$count.'</td>
						<td>'.$foul_team_name.'</td>
						<td>'.$foul_player_number.'</td>
						<td>'.$foul_player_name.'</td>
						<td>'.$foul_minute_time.'</td>
						<td>'.$foul_type_name.'</td>
						<td>'.$action_name.'</td>
						<td>
						<a class="btn btn-sm btn-warning" title="Edit"  data-toggle="modal" data-target="#add_faul_items'.$foul_id.'"><i class="fa fa-pencil"></i></a>
						 <div class="modal fade" id="add_faul_items'.$foul_id.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
					        <div class="modal-dialog" role="document">
					            <div class="modal-content">
					                <div class="modal-header">
					                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					                    <h4 class="modal-title" id="myModalLabel">Cautions</h4>
					                </div>
					                <div class="modal-body">
					                    '.form_open('soccer_management/referee/edit_fixture_foul/'.$fixture_id.'/'.$foul_id, array("class" => "form-horizontal", "role" => "form")).'
					                    <div class="row">
					                        <div class="col-md-12">
					                        
					                            <div class="form-group">
					                                <label class="col-lg-3 control-label">Team: </label>
					                                <div class="col-lg-8">
					                                    <select class="form-control" name="team_id'.$foul_id.'" id="faul_team_id'.$foul_id.'" onchange="check_department_type_faul_db('.$foul_id.')">';
					                                        
					                                            if($fixture_teams->num_rows()> 0)
					                                            {
					                                                $foul_result .= '<option value="0" selected>--Select Team--</option>';
					                                                foreach($fixture_teams->result() as $res)
					                                                {
					                                                    $db_team_id = $res->team_id;
					                                                    $team_name = $res->team_name;
					                                                    
					                                                    if($team_id == $db_team_id)
					                                                    {
					                                                    	$foul_result .='<option value="'.$team_id.'" selected>'.$team_name.'</option>';

					                                                    }
					                                                    else
					                                                    {

					                                                    	$foul_result .='<option value="'.$team_id.'">'.$team_name.'</option>';
					                                                    }
					                                                }
					                                            }
					                                        $foul_result .= '
					                                    </select>
					                                </div>
					                            </div>

					                            <div class="form-group">
					                                <label class="col-lg-3 control-label">Select Player: </label>
					                                
					                                <div class="col-lg-8">
					                                    <select name="foul_player_id'.$foul_id.'" class="form-control" id="faul_team_players_db'.$foul_id.'">
					                                        
					                                    </select>
					                                </div>
					                            </div>
					                            <div class="form-group">
					                                <label class="col-lg-3 control-label">Foul type: </label>
					                                <div class="col-lg-8">
					                                    <select class="form-control" name="foul_type_id'.$foul_id.'">';
					                                        
					                                            if($foul_types->num_rows()> 0)
					                                            {
					                                                $foul_result .= '<option value="--Select Offense--" selected>--Select Offense-</option>';
					                                                foreach($foul_types->result() as $fouls)
					                                                {
					                                                    $db_foul_type_id = $fouls->foul_type_id;
					                                                    $foul_type_name = $fouls->foul_type_name;
					                                                    
					                                                    if($db_foul_type_id == $foul_type_idd)
					                                                    {
					                                                        $foul_result .= '<option value="'.$db_foul_type_id.'" selected>'.$foul_type_name.'</option>';
					                                                    }
					                                                    
					                                                    else
					                                                    {
					                                                        $foul_result .= '<option value="'.$db_foul_type_id.'">'.$foul_type_name.'</option>';
					                                                    }
					                                                }
					                                            }
					                                        $foul_result .='
					                                    </select>
					                                </div>
					                            </div>
					                            <div class="form-group">
					                                <label class="col-lg-3 control-label">Foul Minute </label>
					                                
					                                <div class="col-lg-8">
					                                    <input type="text" class="form-control" name="foul_minute'.$foul_id.'" placeholder="Minute" value="'.$foul_minute_time.'">
					                                </div>
					                            </div>
					                            <div class="form-group">
					                                <label class="col-lg-3 control-label">Action: </label>
					                                <div class="col-lg-8">
					                                    <select class="form-control" name="action_id'.$foul_id.'">';
					                                        
					                                            if($action_types->num_rows()> 0)
					                                            {
					                                                $foul_result .= '<option value="--Select Action--" selected>--Select Action--</option>';
					                                                foreach($action_types->result() as $actions)
					                                                {
					                                                    $db_action_id = $actions->action_id;
					                                                    $action_name = $actions->action_name;
					                                                    
					                                                    if($db_action_id == $action_id)
					                                                    {
					                                                        $foul_result .= '<option value="'.$db_action_id.'" selected>'.$action_name.'</option>';
					                                                    }
					                                                    
					                                                    else
					                                                    {
					                                                        $foul_result .= '<option value="'.$db_action_id.'">'.$action_name.'</option>';
					                                                    }
					                                                }
					                                            }
					                                        $foul_result .= '
					                                    </select>
					                                </div>
					                            </div>
					                            <div class="row" style="margin-top:10px;">
					                                <div class="col-md-12">
					                                    <div class="form-actions center-align">
					                                        <button class="submit btn btn-danger" type="submit">
					                                            Edit caution
					                                        </button>
					                                    </div>
					                                </div>
					                            </div>
					                        </div>
					                    </div>
					                    '.form_close().'
					                </div>
					            </div>
					        </div>
					    </div>
						</td>

						<td><a href="'.site_url().'soccer-management/delete-foul/'.$foul_id.'/'.$fixture_id.'" class="btn btn-sm btn-danger" onclick="return confirm(\'Do you really want to delete this foul?\');" title="Delete'.$foul_type_name.'"><i class="fa fa-trash"></i></a></td>
					</tr>
					';
		
	}
	$foul_result .='</tbody>
				</table>';
}
else
{
	$foul_result .= "No foouls committed in the match";
}
if($total_fixture_goal->num_rows() > 0)
{
	$count = 0;
	$result.= 
			'
			<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th>Team</th>
						<th>Number</th>
						<th>Player name</th>
						<th>Minute</th>
						<th>Goal Type</th>
                        <th></th>
						<th colspan=2>Action</th>
					</tr>
				</thead>
				  <tbody>
				  
			';
	foreach($total_fixture_goal->result() as $fixture_goals)
	{
		
		$team_name = $fixture_goals->team_name;
		$team_id = $fixture_goals->team_id;
		$player_number = $fixture_goals->player_number;
		$player_fname = $fixture_goals->player_fname;
		$player_onames = $fixture_goals->player_onames;
		$player_name = $player_fname. ' ' .$player_onames;
		$score_minute = $fixture_goals->goal_minute;
		$goal_type = $fixture_goals->goal_type_name;
		$goal_type_idd = $fixture_goals->goal_type_id;
        $goal_scored = $fixture_goals->goal_scored;
		$goal_id = $fixture_goals->goal_id;

        if($goal_scored == 1)
        {
            $is_scored = '<div class="success">scored</div>';
        }
        elseif($goal_scored == 0)
        {
            $is_scored = '<div class="danger">missed</div>';
        }
		else
        {
            $is_scored = '<div class="warning">-</div>';
        }
		$count++;
		
		$result.=
				'
				<tr>
					<td>'.$count.'</td>
					<td>'.$team_name.'</td>
					<td>'.$player_number.'</td>
					<td>'.$player_name.'</td>
					<td>'.$score_minute.'</td>
					<td>'.$goal_type.'</td>
                    <td>'.$is_scored.'</td>
                    <td><a class="btn btn-sm btn-warning" title="Edit"  data-toggle="modal" data-target="#add_scores_items_edit'.$goal_id.'"><i class="fa fa-pencil"></i></a>
                    	<div class="modal fade" id="add_scores_items_edit'.$goal_id.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
					        <div class="modal-dialog" role="document">
					            <div class="modal-content">
					                <div class="modal-header">
					                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					                    <h4 class="modal-title" id="myModalLabel">Edit Goal</h4>
					                </div>
					                <div class="modal-body">
					                    '.form_open('soccer_management/referee/edit_fixture_goal/'.$fixture_id.'/'.$goal_id, array("class" => "form-horizontal", "role" => "form")).'
					                    		<div class="row">
							                        <div class="col-md-12">
							                            <div class="form-group">
							                                <label class="col-lg-3 control-label">Team: </label>
							                                <div class="col-lg-8">
							                                    <select class="form-control" name="team_id'.$goal_id.'" id="player_team_id_db'.$goal_id.'" onchange="check_department_type_db('.$goal_id.')">';
							                                        
							                                            if($fixture_teams->num_rows()> 0)
							                                            {
							                                                $result.='<option value="0" selected>--Select Team--</option>';
							                                                foreach($fixture_teams->result() as $res)
							                                                {
							                                                    $db_team_id = $res->team_id;
							                                                    $team_name = $res->team_name;

							                                                    if($team_id == $db_team_id)
							                                                    {
							                                                    	$result .='<option value="'.$team_id.'" selected>'.$team_name.'</option>';

							                                                    }
							                                                    else
							                                                    {

							                                                    	$result .='<option value="'.$team_id.'">'.$team_name.'</option>';
							                                                    }
							                                                    
							                                                }
							                                            }
							                                      $result.='
							                                    </select>
							                                </div>
							                            </div>

							                            <div class="form-group">
							                                <label class="col-lg-3 control-label">Select Player: </label>
							                                
							                                <div class="col-lg-8">
							                                    <select name="fixture_team_player_id'.$goal_id.'" class="form-control" id="team_players_db'.$goal_id.'">
							                                        
							                                    </select>
							                                </div>
							                            </div>
							                        
							                            <div class="form-group">
							                                <label class="col-lg-3 control-label">Goal Type: </label>
							                                <div class="col-lg-8">
							                                    <select class="form-control" name="goal_type_id'.$goal_id.'" id="goal_type_id" onchange="check_goal_type()">';
							                                       
							                                            if($goul_types->num_rows()> 0)
							                                            {
							                                                $result .= '<option value="--Select Scorer--" selected>--Select Goal Type--</option>';
							                                                foreach($goul_types->result() as $res)
							                                                {
							                                                    $db_goul_type_id = $res->goal_type_id;
							                                                    $goul_type_name = $res->goal_type_name;
							                                                    
							                                                    if($db_goul_type_id == $goal_type_idd)
							                                                    {
							                                                        $result .= '<option value="'.$db_goul_type_id.'" selected>'.$goul_type_name.'</option>';
							                                                    }
							                                                    
							                                                    else
							                                                    {
							                                                        $result .= '<option value="'.$db_goul_type_id.'">'.$goul_type_name.'</option>';
							                                                    }
							                                                }
							                                            }
							                                       $result .='
							                                    </select>
							                                </div>
							                            </div>
							                             <div class="form-group" id="goal_type_score" style="display:none">
							                                    <label class="col-lg-3 control-label">Scored ? </label>
							                                    <div class="col-lg-4">
							                                        <div class="radio">
							                                            <label>
							                                                <input id="optionsRadios2" type="radio" name="penalty_score_status'.$goal_id.'" value="0" checked="checked" >
							                                                No
							                                            </label>
							                                        </div>
							                                    </div>
							                                    
							                                    <div class="col-lg-5">
							                                        <div class="radio">
							                                            <label>
							                                                <input id="optionsRadios2" type="radio" name="penalty_score_status'.$goal_id.'" value="1" >
							                                                Yes
							                                            </label>
							                                        </div>
							                                    </div>
							                                </div>
							                            <div class="form-group">
							                                <label class="col-lg-3 control-label">Score Minute </label>
							                                
							                                <div class="col-lg-8">
							                                    <input type="text" class="form-control" name="goal_minute'.$goal_id.'" placeholder="Minute" value="'.$score_minute.'">
							                                </div>
							                            </div>
							                        
							                            <div class="row" style="margin-top:10px;">
							                                <div class="col-md-12">
							                                    <div class="form-actions center-align">
							                                        <button class="submit btn btn-success" type="submit">
							                                            Edit Score
							                                        </button>
							                                    </div>
							                                </div>
							                            </div>
							                        </div>
							                    </div>
					                   		
					                    '.form_close().'
					                </div>
					            </div>
					        </div>
					    </div>
                    </td>
					<td><a href="'.site_url().'soccer-management/delete-goal/'.$goal_id.'/'.$fixture_id.'" class="btn btn-sm btn-danger" onclick="return confirm(\'Do you really want to delete this goal?\');" title="Delete"><i class="fa fa-trash"></i></a></td>
				</tr>
				';
	}
	$result .='</tbody>
				</table>';
}
else
{
	$result .= 'No goals scored';
}
foreach ($fixture_detail->result() as $key_fixture) {
    # code...
    $first_half_start_time = $key_fixture->first_half_start_time;
    $first_half_end_time = $key_fixture->first_half_end_time;
    $second_half_start_time = $key_fixture->second_half_start_time;
    $second_half_end_time = $key_fixture->second_half_end_time;
    $first_half_score = 0;
    $second_half_score = 0;
    $penalty_scores = 0;
   if($first_half_end_time != NULL )
   {
         $array = explode(':', $first_half_end_time);
         $first_half_minute = $array[1];

         $first_half_score = $this->referee_model->get_first_half_goals($first_half_minute,$fixture_id);
   }
   
   if($second_half_end_time != null)
   {

    $array2 = explode(':', $second_half_end_time);

    $second_half_minute = $array[1];

    $second_half_score = $this->referee_model->get_second_half_goals($first_half_minute,$fixture_id,$second_half_minute);
    
    $penalty_scores = $this->referee_model->get_penalties_goals($fixture_id,$second_half_minute);

   }
   

}
?>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title"><?php echo $title;?></h2>
        <div class="pull-right"  style="margin-top: -25px;">
            <a href="<?php echo base_url().'referee/league';?>" class="btn  btn-sm btn-info fa fa-arrow-left"> Back</a>
        </div>
    </header>
    <div class="panel-body">
    	<div class="row">
            <div class="col-md-2">
                <button type="button" class="btn btn-default btn-sm col-md-12"  data-toggle="modal" data-target="#add_match_timing">
                     Match Timings
                    </button>
            </div>
            <div class="col-md-2">
                <button type="button" class="btn btn-success btn-sm col-md-12"  data-toggle="modal" data-target="#add_scores_items">
                     Add Score
                    </button>
            </div>
            <div class="col-md-2">
                <button type="button" class="btn btn-danger btn-sm col-md-12"  data-toggle="modal" data-target="#add_faul_items">
                     Add Caution
                    </button>
            </div>
            <div class="col-md-2">
                <button type="button" class="btn btn-warning btn-sm col-md-12"  data-toggle="modal" data-target="#add_subs">
                     Substitutions
                    </button>
            </div>
             <div class="col-md-2">
                <button type="button" class="btn btn-danger btn-sm col-md-12"  data-toggle="modal" data-target="#add_match_summary">
                     Match Summary
                    </button>
            </div>
            
            <div class="col-md-2">
                <button type="button" class="btn btn-primary btn-sm col-md-12"  data-toggle="modal" data-target="#add_scores">
                     Scores Report
                    </button>
            </div>
            
        </div>
         <!-- Adding Errors -->
                    <?php
						$success = $this->session->userdata('success_message');
						$error = $this->session->userdata('error_message');
						
						if(!empty($success))
						{
							echo '
								<div class="alert alert-success">'.$success.'</div>
							';
							
							$this->session->unset_userdata('success_message');
						}
						
						if(!empty($error))
						{
							echo '
								<div class="alert alert-danger">'.$error.'</div>
							';
							
							$this->session->unset_userdata('error_message');
						}
						$validation_errors = validation_errors();
						
						if(!empty($validation_errors))
						{
							echo '<div class="alert alert-danger"> Oh snap! '.$validation_errors.' </div>';
						}
                    ?>
       
    </div>
    <div class="modal fade" id="add_scores_items" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add Score</h4>
                </div>
                <div class="modal-body">
                    <?php echo form_open('soccer_management/referee/add_fixture_goal/'.$fixture_id, array("class" => "form-horizontal", "role" => "form"));
                    ?>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-lg-3 control-label">Team: </label>
                                <div class="col-lg-8">
                                    <select class="form-control" name="team_id" id="player_team_id" onchange="check_department_type()">
                                        <?php
                                            if($fixture_teams->num_rows()> 0)
                                            {
                                                echo '<option value="0" selected>--Select Team--</option>';
                                                foreach($fixture_teams->result() as $res)
                                                {
                                                    $team_id = $res->team_id;
                                                    $team_name = $res->team_name;
                                                    
                                                    echo '<option value="'.$team_id.'">'.$team_name.'</option>';
                                                }
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-3 control-label">Select Player: </label>
                                
                                <div class="col-lg-8">
                                    <select name="fixture_team_player_id" class="form-control" id="team_players">
                                        
                                    </select>
                                </div>
                            </div>
                        
                            <div class="form-group">
                                <label class="col-lg-3 control-label">Goal Type: </label>
                                <div class="col-lg-8">
                                    <select class="form-control" name="goal_type_id" id="goal_type_id" onchange="check_goal_type()">
                                        <?php
                                            if($goul_types->num_rows()> 0)
                                            {
                                                echo '<option value="--Select Scorer--" selected>--Select Goal Type--</option>';
                                                foreach($goul_types->result() as $res)
                                                {
                                                    $db_goul_type_id = $res->goal_type_id;
                                                    $goul_type_name = $res->goal_type_name;
                                                    
                                                    if($db_goul_type_id == $goal_type_id)
                                                    {
                                                        echo '<option value="'.$db_goul_type_id.'" selected>'.$goul_type_name.'</option>';
                                                    }
                                                    
                                                    else
                                                    {
                                                        echo '<option value="'.$db_goul_type_id.'">'.$goul_type_name.'</option>';
                                                    }
                                                }
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                             <div class="form-group" id="goal_type_score" style="display:none">
                                    <label class="col-lg-3 control-label">Scored ? </label>
                                    <div class="col-lg-4">
                                        <div class="radio">
                                            <label>
                                                <input id="optionsRadios2" type="radio" name="penalty_score_status" value="0" checked="checked" >
                                                No
                                            </label>
                                        </div>
                                    </div>
                                    
                                    <div class="col-lg-5">
                                        <div class="radio">
                                            <label>
                                                <input id="optionsRadios2" type="radio" name="penalty_score_status" value="1" >
                                                Yes
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            <div class="form-group">
                                <label class="col-lg-3 control-label">Score Minute </label>
                                
                                <div class="col-lg-8">
                                    <input type="text" class="form-control" name="goal_minute" placeholder="Minute" value="<?php echo $goal_minute;?>">
                                </div>
                            </div>
                        
                            <div class="row" style="margin-top:10px;">
                                <div class="col-md-12">
                                    <div class="form-actions center-align">
                                        <button class="submit btn btn-success" type="submit">
                                            Submit Score
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php echo form_close();?>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="add_subs" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Make Substitution</h4>
                </div>
                <div class="modal-body">
                    <?php 
                    $minute = set_value('minute');
					$away_minute = set_value('away_minute');
                    $home_player_in = set_value('home_player_in');
                    $away_player_in = set_value('away_player_in');
                    $home_player_out = set_value('home_player_out');
                    $away_player_out = set_value('away_player_out');
                    ?>
                    <div class = "row">
                        <div class = "col-md-6">
                            <section class="panel">
                                <header class="panel-heading">
                                    <h2 class="panel-title"><?php echo $title_home;?></h2>
                                </header>
                               
                                <div class="panel-body">
                                     <?php echo form_open(base_url().'soccer_management/referee/home-substitute/'.$fixture_id, array("class" => "form-horizontal", "role" => "form"));?>
                                    <div class="form-group">
                                        <label class="col-lg-5 control-label">Minute: </label>
                                        
                                        <div class="col-lg-7">
                                            <input type="text" class="form-control" name="home_minute" placeholder="minute" value="<?php echo $minute;?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-5 control-label">Player Out: </label>
                                        
                                        <div class="col-lg-7">
                                            <select class="form-control" name="home_player_out">
                                                <?php
                                                    if($team_details_home_out->num_rows() > 0)
                                                    {
                                                        $gender = $team_details_home_out->result();
                                                        
                                                        foreach($gender as $res)
                                                        {
                                                            $player_id = $res->player_id;
                                                            $player_fname = $res->player_fname;
                                                            $player_onames = $res->player_onames;
                                                            $player_name = $player_fname.' '.$player_onames;
                                                            
                                                            if($player_id == $home_player_out)
                                                            {
                                                                echo '<option value="'.$player_id.'" selected>'.$player_name.'</option>';
                                                            }
                                                            
                                                            else
                                                            {
                                                                echo '<option value="'.$player_id.'">'.$player_name.'</option>';
                                                            }
                                                        }
                                                    }
                                                ?>
                                            </select>
                                        </div>
                    
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-5 control-label">Player In: </label>
                                        
                                        <div class="col-lg-7">
                                            <select class="form-control" name="home_player_in">
                                                <?php
                                                    if($team_details_home_in->num_rows() > 0)
                                                    {
                                                        $gender = $team_details_home_in->result();
                                                        
                                                        foreach($gender as $res)
                                                        {
                                                            $player_id = $res->player_id;
                                                            $player_fname = $res->player_fname;
                                                            $player_onames = $res->player_onames;
                                                            $player_name = $player_fname.' '.$player_onames;
                                                            
                                                            if($player_id == $home_player_in)
                                                            {
                                                                echo '<option value="'.$player_id.'" selected>'.$player_name.'</option>';
                                                            }
                                                            
                                                            else
                                                            {
                                                                echo '<option value="'.$player_id.'">'.$player_name.'</option>';
                                                            }
                                                        }
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-top:10px;">
                                        <div class="form-actions center-align">
                                            <button class="submit btn btn-success" type="submit">
                                                Add Home Substitute
                                            </button>
                                        </div>
                                    </div>
                                    <?php echo form_close();?>
                                </div>
                            </section>
                        </div>
                        <div class = "col-md-6">
                            <section class="panel">
                                <header class="panel-heading">
                                    <h2 class="panel-title"><?php echo $title_away;?></h2>
                                </header>
                               
                                <div class="panel-body">
                                    <?php echo form_open(base_url().'soccer_management/referee/away-substitute/'.$fixture_id, array("class" => "form-horizontal", "role" => "form"));?>
                                    <div class="form-group">
                                        <label class="col-lg-5 control-label">Minute: </label>
                                        
                                        <div class="col-lg-7">
                                            <input type="text" class="form-control" name="away_minute" placeholder="minute" value="<?php echo $away_minute;?>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-5 control-label">Player Out: </label>
                                        
                                        <div class="col-lg-7">
                                            <select class="form-control" name="away_player_out">
                                                <?php
                                                    if($team_details_away_out->num_rows() > 0)
                                                    {
                                                        $away_out = $team_details_away_out->result();
                                                        
                                                        foreach($away_out as $away_res)
                                                        {
                                                            $player_id = $away_res->player_id;
                                                            $player_fname = $away_res->player_fname;
                                                            $player_onames = $res->player_onames;
                                                            $player_name = $player_fname.' '.$player_onames;
                                                            
                                                            if($player_id == $away_player_out)
                                                            {
                                                                echo '<option value="'.$player_id.'" selected>'.$player_name.'</option>';
                                                            }
                                                            
                                                            else
                                                            {
                                                                echo '<option value="'.$player_id.'">'.$player_name.'</option>';
                                                            }
                                                        }
                                                    }
                                                ?>
                                            </select>
                                        </div>

                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-5 control-label">Player In: </label>
                                        
                                        <div class="col-lg-7">
                                            <select class="form-control" name="away_player_in">
                                                <?php
                                                    if($team_details_away_in->num_rows() > 0)
                                                    {
                                                        $away_in = $team_details_away_in->result();
                                                        
                                                        foreach($away_in as $away_res)
                                                        {
                                                            $player_id = $res->player_id;
                                                            $player_fname = $res->player_fname;
                                                            $player_onames = $res->player_onames;
                                                            $player_name = $player_fname.' '.$player_onames;
                                                            
                                                            if($player_id == $away_player_in)
                                                            {
                                                                echo '<option value="'.$player_id.'" selected>'.$player_name.'</option>';
                                                            }
                                                            
                                                            else
                                                            {
                                                                echo '<option value="'.$player_id.'">'.$player_name.'</option>';
                                                            }
                                                        }
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-top:10px;">
                                        <div class="form-actions center-align">
                                            <button class="submit btn btn-danger" type="submit">
                                                Add Away Substitute
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                
                            </section>
                
                        </div>
                    </div>
                    <?php echo form_close();?>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="add_faul_items" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Cautions</h4>
                </div>
                <div class="modal-body">
                    <?php echo form_open('soccer_management/referee/add_fixture_foul/'.$fixture_id, array("class" => "form-horizontal", "role" => "form"));?>
                    <div class="row">
                        <div class="col-md-12">
                        
                            <div class="form-group">
                                <label class="col-lg-3 control-label">Team: </label>
                                <div class="col-lg-8">
                                    <select class="form-control" name="team_id" id="faul_team_id" onchange="check_department_type_faul()">
                                        <?php
                                            if($fixture_teams->num_rows()> 0)
                                            {
                                                echo '<option value="0" selected>--Select Team--</option>';
                                                foreach($fixture_teams->result() as $res)
                                                {
                                                    $team_id = $res->team_id;
                                                    $team_name = $res->team_name;
                                                    
                                                    echo '<option value="'.$team_id.'">'.$team_name.'</option>';
                                                }
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-3 control-label">Select Player: </label>
                                
                                <div class="col-lg-8">
                                    <select name="foul_player_id" class="form-control" id="faul_team_players">
                                        
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-3 control-label">Foul type: </label>
                                <div class="col-lg-8">
                                    <select class="form-control" name="foul_type_id">
                                        <?php
                                            if($foul_types->num_rows()> 0)
                                            {
                                                echo '<option value="--Select Offense--" selected>--Select Offense-</option>';
                                                foreach($foul_types->result() as $fouls)
                                                {
                                                    $db_foul_type_id = $fouls->foul_type_id;
                                                    $foul_type_name = $fouls->foul_type_name;
                                                    
                                                    if($db_foul_type_id == $foul_type_id)
                                                    {
                                                        echo '<option value="'.$db_foul_type_id.'" selected>'.$foul_type_name.'</option>';
                                                    }
                                                    
                                                    else
                                                    {
                                                        echo '<option value="'.$db_foul_type_id.'">'.$foul_type_name.'</option>';
                                                    }
                                                }
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-3 control-label">Foul Minute </label>
                                
                                <div class="col-lg-8">
                                    <input type="text" class="form-control" name="foul_minute" placeholder="Minute" value="<?php echo $foul_minute;?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-3 control-label">Action: </label>
                                <div class="col-lg-8">
                                    <select class="form-control" name="action_id">
                                        <?php
                                            if($action_types->num_rows()> 0)
                                            {
                                                echo '<option value="--Select Action--" selected>--Select Action--</option>';
                                                foreach($action_types->result() as $actions)
                                                {
                                                    $db_action_id = $actions->action_id;
                                                    $action_name = $actions->action_name;
                                                    
                                                    if($db_action_id == $action_id)
                                                    {
                                                        echo '<option value="'.$db_action_id.'" selected>'.$action_name.'</option>';
                                                    }
                                                    
                                                    else
                                                    {
                                                        echo '<option value="'.$db_action_id.'">'.$action_name.'</option>';
                                                    }
                                                }
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="row" style="margin-top:10px;">
                                <div class="col-md-12">
                                    <div class="form-actions center-align">
                                        <button class="submit btn btn-danger" type="submit">
                                            Add caution
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php echo form_close();?>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="add_match_summary" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Cautions</h4>
                </div>
                <div class="modal-body">
                    <?php echo form_open('soccer-management/referee-summary/'.$fixture_id, array("class" => "form-horizontal", "role" => "form"));?>
                        <div class="row">
                             <div class="form-group">
                                <label class="col-lg-2 control-label">Summary: </label>
                                
                                <div class="col-lg-8">
                                    <textarea class="col-md-12 cleditor" rows="5"  id="summary" name="summary"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="margin-top:10px;">
                            <div class="col-md-12">
                                <div class="form-actions center-align">
                                    <button class="submit btn btn-primary" type="submit">
                                        Add referee Summary
                                    </button>
                                </div>
                            </div>
                        </div>
                    
                    <?php echo form_close();?>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="add_match_timing" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add Match Timings</h4>
                </div>
                <div class="modal-body">
                    <?php echo form_open('soccer_management/referee/add_fixture_timing/'.$fixture_id, array("class" => "form-horizontal", "role" => "form"));
                    ?>
                    <div class="row">
                        <div class="col-md-6">
                            <h5>First Half Timings</h5>
                            <div class="form-group">
                                <label class="col-lg-4 control-label">Start time : </label>
                            
                                <div class="col-lg-8">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-clock-o"></i>
                                        </span>
                                        <input type="text" class="form-control"  placeholder="08:00:00" name="first_half_start_time" value="<?php echo $first_half_start_time;?>" >
                                    </div>
                                </div>
                            </div>
                                
                            <div class="form-group">
                                <label class="col-lg-4 control-label">End time : </label>
                                
                                <div class="col-lg-8">      
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-clock-o"></i>
                                        </span>
                                        <input type="text" class="form-control"  placeholder="08:45:00"  name="first_half_end_time" value="<?php echo $first_half_end_time;?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <h5>Second Half Timings</h5>
                            <div class="form-group">
                                <label class="col-lg-4 control-label">Start time : </label>
                            
                                <div class="col-lg-8">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-clock-o"></i>
                                        </span>
                                        <input type="text" class="form-control"  placeholder="09:00:00"  name="second_half_start_time" value="<?php echo $second_half_start_time;?>">
                                    </div>
                                </div>
                            </div>
                                
                            <div class="form-group">
                                <label class="col-lg-4 control-label">End time : </label>
                                
                                <div class="col-lg-8">      
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-clock-o"></i>
                                        </span>
                                        <input type="text" class="form-control"  placeholder="09:45:00"  name="second_half_end_time" value="<?php echo $second_half_end_time;?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="margin-top:10px;">
                        <div class="col-md-12">
                            <div class="form-actions center-align">
                                <button class="submit btn btn-success" type="submit">
                                    Save
                                </button>
                            </div>
                        </div>
                    </div>
                    <?php echo form_close();?>
                </div>
            </div>
        </div>
    </div>

        <div class="row" style="margin-bottom:20px;">
            <div class="modal fade" id="add_scores" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				<div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Scores</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-4"><label class="col-lg-12 control-label">Team</label>
                                </div>
                                <div class="col-md-4"><label class="col-lg-12 control-label">Timings</label>
                                </div>
                                <div class="col-md-4"><label class="col-lg-12 control-label">Scores</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <div class="col-lg-12">
                                           <p><strong>Home:</strong> <?php echo $home_team;?></p>
                                           <p><strong> Away:</strong> <?php echo $away_team;?></p>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <div class="col-lg-12">
                                        <p><strong> First Half :</strong> <?php echo $first_half_start_time.' - '.$first_half_end_time;?></p>
                                        <p><strong> Second Half:</strong> <?php echo $second_half_start_time.' - '.$second_half_end_time;?></p>
                                        <p><strong> Penalties:</strong></p>
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <div class="col-lg-12">
                                            <p><?php echo $first_half_score;?></p>
                                            <p><?php echo $second_half_score;?></p>
                                            <p><?php echo $penalty_scores;?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
				</div>
			</div>
        </div>
    

</section>
<div class="row">
    <div class="col-md-6">
        <section class="panel">
            <header class="panel-heading">
                <h2 class="panel-title">Goals</h2>
            </header>
            <div class="panel-body">
                <div class="table-responsive">
                    <?php echo $result;?>
                </div> 
            </div>
        </section>
    </div>
    <div class="col-md-6">
        <section class="panel">
            <header class="panel-heading">
                <h2 class="panel-title">Cautions</h2>
            </header>
            <div class="panel-body">
                <div class="table-responsive">
                    <?php echo $foul_result;?>
                </div> 
            </div>
        </section>
    </div>
</div>
<div class="row">
	<div class="col-md-6">
    	<section class="panel">
            <header class="panel-heading">
                <h2 class="panel-title"><?php echo $title_home;?></h2>
            </header>
            <div class="panel-body">
                <div class="table-responsive">
                    <?php echo $home_substitutes_result;?>
                </div> 
            </div>
        </section>
    </div>
    <div class="col-md-6">
    	<section class="panel">
            <header class="panel-heading">
                <h2 class="panel-title"><?php echo $title_away;?></h2>
            </header>
            <div class="panel-body">
                <div class="table-responsive">
                    <?php echo $away_substitutes_result;?>
                </div> 
            </div>
        </section>	
    </div>
</div>
 
<div class="row">
	<div class="col-md-6">
    	<section class="panel">
            <header class="panel-heading">
                <h2 class="panel-title">Ref Summary</h2>
            </header>
            <div class="panel-body">
                <div class="table-responsive">
                    <?php echo $ref_sum_results;?>
                </div>
            </div>
        </section>
    </div>
    <div class="col-md-6">
    	<section class="panel">
            <header class="panel-heading">
                <h2 class="panel-title">Match Timings</h2>
            </header>
            <div class="panel-body">
                <div class="table-responsive">
                    <?php echo $match_timings_result;?>
                </div>
            </div>
        </section>
    </div>
    
</div>

<script type="text/javascript">


	function check_department_type_db(goal_id)
    {
        var player_team_id = document.getElementById("player_team_id_db"+goal_id).value;
        // var team_id = myTarget;

        var fixture_id = <?php echo $fixture_id?>;
        //get department services
        var data_url = "<?php echo site_url();?>referee/get_team_players/"+player_team_id+"/"+fixture_id;

        $.get( data_url , function( data ) 
        {
            $( "#team_players_db"+goal_id ).html( data );
        });
    }
    
    function check_department_type()
    {
        var player_team_id = document.getElementById("player_team_id").value;
        // var team_id = myTarget;

        var fixture_id = <?php echo $fixture_id?>;
        //get department services
        var data_url = "<?php echo site_url();?>referee/get_team_players/"+player_team_id+"/"+fixture_id;

        $.get( data_url , function( data ) 
        {
            $( "#team_players" ).html( data );
        });
    }
    function check_department_type_faul_db(foul_id)
    {
        var player_team_id = document.getElementById("faul_team_id"+foul_id).value;
        // var team_id = myTarget;

        var fixture_id = <?php echo $fixture_id?>;
        //get department services
        var data_url = "<?php echo site_url();?>referee/get_team_players/"+player_team_id+"/"+fixture_id;
     
        $.get( data_url , function( data ) 
        {
            $( "#faul_team_players_db"+foul_id ).html( data );
        });
    }
    function check_department_type_faul()
    {
        var player_team_id = document.getElementById("faul_team_id").value;
        // var team_id = myTarget;

        var fixture_id = <?php echo $fixture_id?>;
        //get department services
        var data_url = "<?php echo site_url();?>referee/get_team_players/"+player_team_id+"/"+fixture_id;
     
        $.get( data_url , function( data ) 
        {
            $( "#faul_team_players" ).html( data );
        });
    }
     function check_department_type_substitution()
    {
        var player_team_id = document.getElementById("sub_team_id").value;
        // var team_id = myTarget;

        var fixture_id = <?php echo $fixture_id?>;
        //get department services
        var data_url = "<?php echo site_url();?>referee/get_team_league_substitutions/"+player_team_id+"/"+fixture_id;
       
        $.get( data_url , function( data ) 
        {
            $( "#sub_team_players" ).html( data );
        });
    }
    function check_goal_type()
    {
          var goal_type_id = document.getElementById("goal_type_id").value;

          var myTarget2 = document.getElementById("goal_type_score");


          if(goal_type_id == 1)
          {
            // this is a penlty
            myTarget2.style.display = 'block';
          }
          else
          {
            myTarget2.style.display = 'none';
          }
    }
</script>