<?php
		
		$result = '';
		
		//if users exist display them
		if ($query->num_rows() > 0)
		{
			$count = 0;
			
			$result .= 
			'
			<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th>Venue Name</th>
						<th>Venue Location</th>
						<th>Venue Status</th>
						<th colspan="3">Actions</th>
					</tr>
				</thead>
				  <tbody>
				  
			';
			
			
			foreach ($query->result() as $row)
			{
				$venue_id = $row->venue_id;
				$venue_name = $row->venue_name;
				$venue_location = $row->venue_location;
				$venue_status = $row->venue_status;
				
				//status
				if($venue_status == 1)
				{
					$status = 'Active';
				}
				else
				{
					$status = 'Disabled';
				}
				
				//create deactivated status display
				if($venue_status == 0)
				{
					$status = '<span class="label label-default">Deactivated</span>';
					$button = '<a class="btn btn-info" href="'.site_url().'soccer-management/activate-venue/'.$venue_id.'" onclick="return confirm(\'Do you want to activate '.$venue_id.'?\');" title="Activate '.$venue_name.'"><i class="fa fa-thumbs-up"></i>Activate</a>';
				}
				//create activated status display
				else if($venue_status == 1)
				{
					$status = '<span class="label label-success">Active</span>';
					$button = '<a class="btn btn-default" href="'.site_url().'soccer-management/deactivate-venue/'.$venue_id.'" onclick="return confirm(\'Do you want to deactivate '.$venue_name.'?\');" title="Deactivate '.$venue_name.'"><i class="fa fa-thumbs-down"></i>Deactivate</a>';
				}
				
				$count++;
				$result .= 
				'
					<tr>
						<td>'.$count.'</td>
						<td>'.$venue_name.'</td>
						<td>'.$venue_location.'</td>
						<td>'.$status.'</td>
						<td><a href="'.site_url().'soccer-management/edit-venue/'.$venue_id.'" class="btn btn-sm btn-success" title="Edit '.$venue_name.'"><i class="fa fa-pencil"></i>Edit Venue</a></td>
						<td>'.$button.'</td>
						<td><a href="'.site_url().'soccer-management/delete-venue/'.$venue_id.'" class="btn btn-sm btn-danger" onclick="return confirm(\'Do you really want to delete '.$venue_name.'?\');" title="Delete '.$venue_name.'"><i class="fa fa-trash"> Delete</i></a></td>
					</tr> 
				';
			}
			
			$result .= 
			'
						  </tbody>
						</table>
			';
		}
		
		else
		{
			$result .= "There are no venue";
		}
?><section class="panel">
							<header class="panel-heading">						
								<h2 class="panel-title"><?php echo $title;?></h2>
							</header>
							<div class="panel-body">
                            	<?php
								$search = $this->session->userdata('venue_search_title2');
								
								if(!empty($search))
								{
									echo '<h6>Filtered by: '.$search.'</h6>';
									echo '<a href="'.site_url().'soccer_management/venue/close_search" class="btn btn-sm btn-info pull-left">Close search</a>';
								}
                                $success = $this->session->userdata('success_message');
		
								if(!empty($success))
								{
									echo '<div class="alert alert-success"> <strong>Success!</strong> '.$success.' </div>';
									$this->session->unset_userdata('success_message');
								}
								
								$error = $this->session->userdata('error_message');
								
								if(!empty($error))
								{
									echo '<div class="alert alert-danger"> <strong>Oh snap!</strong> '.$error.' </div>';
									$this->session->unset_userdata('error_message');
								}
								?>
                            	<div class="row" style="margin-bottom:20px;">
                                    <!--<div class="col-lg-2 col-lg-offset-8">
                                        <a href="<?php echo site_url();?>soccer-management/export-venue" class="btn btn-sm btn-success pull-right">Export</a>
                                    </div>-->
                                    <div class="col-lg-12">
                                    	<a href="<?php echo site_url();?>soccer-management/add-venue" class="btn btn-sm btn-info pull-right">Add Venue</a>
                                    </div>
                                </div>
								<div class="table-responsive">
                                	
									<?php echo $result;?>
							
                                </div>
							</div>
                            <div class="panel-footer">
                            	<?php if(isset($links)){echo $links;}?>
                            </div>
						</section>