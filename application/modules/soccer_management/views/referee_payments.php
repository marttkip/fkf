<?php
$result = '';
$league_payment_referees_result = '';
$youth_teams = $this->session->userdata('youth_teams');
if(!empty($youth_teams))
{
}
else
{
	if($payment_referees->num_rows() > 0)
	{
		$count = $page;
				
		$result .= 
		'
		<table class="table table-bordered table-striped table-condensed">
			<thead>
				<tr>
					<th>#</th>
					<th>Referee Name</a></th>
					<th>Referee Role</a></th>
					<th>Fixture Date</a></th>
					<th>Game</a></th>
					<th>Pay per Game</th>
					<th>Status</th>
					<th colspan="3">Actions</th>
				</tr>
			</thead>
			<tbody>
			  
		';
		foreach($payment_referees->result() as $referees)
		{
			$payment_button = '';
			$referee_id = $referees->referee_id;
			$referee_fname = $referees->personnel_fname;
			$referee_onames = $referees->personnel_onames;
			$referee_name = $referee_onames.' '.$referee_fname;
			
			$referee_type = $referees->referee_type_id;
			$referee_role = $this->payments_model->get_referee_role($referee_type);
			$tournament_fixture_id = $referees->tournament_fixture_id;
			
			//get ficture details
			$home_team = $this->tournament_model->get_home_team($tournament_fixture_id);
			$away_team = $this->tournament_model->get_away_team($tournament_fixture_id);
			$fixture_date = $this->payments_model->get_fixture_date($tournament_fixture_id);
			$count++;
			
			//payment_type for referee
			$payment_amount = $this->payments_model->get_payment_amount($referee_type);
			//get ref status in the referee queue
			$referee_payment_status = $this->tournament_model->get_referee_payment_status($referee_id,$tournament_fixture_id,2);
			
			$referee_payments_queue_id = $this->payments_model->get_queue_id($referee_id, $tournament_fixture_id);
			$fixture_type = 2;
			
			if($referee_payment_status == 0)
			{
				$status = '<span class="label label-default">Waiting Confirmation</span>';
			}
			elseif($referee_payment_status == 1)
			{
				$status = '<span class="label label-warning">Sent to Admin</span>';
			}
			elseif($referee_payment_status == 2)
			{
				$status = '<span class="label label-danger">Sent to Accounts</span>';
				$payment_button = '<a class="btn btn-warning" href="'.site_url().'soccer-management/pay-tornament-referee/'.$referee_id.'/'.$referee_payments_queue_id.'/'.$fixture_type.'"  title="Pay '.$referee_name.'"><i class="fa fa-money"></i> Make Payment</a>';
			}
			elseif($referee_payment_status == 3)
			{
				$status = '<span class="label label-success">Accounts Recieved</span>';
				$payment_button = '<a class="btn btn-warning" href="'.site_url().'soccer-management/pay-tornament-referee/'.$referee_id.'/'.$referee_payments_queue_id.'/'.$fixture_type.'"  title="Pay '.$referee_name.'"><i class="fa fa-money"></i> Make Payment</a>';
			}
			else
			{
				$status = '<span class="label label-default">Waiting Confirmation</span>';
				$payment_button = '<a class="btn btn-warning" href="'.site_url().'soccer-management/pay-tornament-referee/'.$referee_id.'/'.$referee_payments_queue_id.'/'.$fixture_type.'"  title="Pay '.$referee_name.'"><i class="fa fa-money"></i> Make Payment</a>';
			}
			$result.= 
			'
				<tr>
					<td>'.$count.'</td>
					<td>'.$referee_name.'</td>
					<td>'.$referee_role.'</td>
					<td>'.date('jS M Y',strtotime($fixture_date)).'</td>
					<td>'.$home_team.' VS '.$away_team.'</td>
					<td>'.number_format($payment_amount,2).'</td>
					<td>'.$status.'</td>
					<td><a class="btn btn-default" href="'.site_url().'soccer_management/tournament/view_referee_queue/'.$referee_id.'/'.$referee_payments_queue_id.'/'.$fixture_type.'/'.$tournament_fixture_id.'"  title="Pay '.$referee_name.'"><i class="fa fa-eye"></i>View</a></td>
					<td>'.$payment_button.'</td>
				</tr>
			';
		}
		$result.='
				</tbody>
			</table>';
	}
	else
	{
		$result.= "No referees have been sent to the accounts queue";
		
	}
}
if($league_payment_referees->num_rows() > 0)
{
	$league_count = 0;
	$league_payment_referees_result .= 
	'
	<table class="table table-bordered table-striped table-condensed">
		<thead>
			<tr>
				<th>#</th>
				<th>Referee Name</a></th>
				<th>Referee Role</a></th>
				<th>Fixture Date</a></th>
				<th>Game</a></th>
				<th>Pay per Game</th>
				<th>Status</th>
				<th colspan="2">Actions</th>
			</tr>
		</thead>
		<tbody>
		  
	';
	//var_dump($league_payment_referees->result());die();
	foreach($league_payment_referees->result() as $league_payments)
	{
		$league_payment_button = '';
		
		$league_referee_id = $league_payments->referee_id;
		$league_referee_fname = $league_payments->referee_fname;
		$league_referee_onames = $league_payments->referee_onames;
		$league_referee_name = $league_referee_fname.' '.$league_referee_onames;
		
		$league_referee_type = $league_payments->referee_type_id;
		$league_referee_role = $this->payments_model->get_referee_role($league_referee_type);
		$fixture_id = $league_payments->fixture_id;
		
		//get ficture details
		$league_home_team = $this->league_model->get_home_team($fixture_id);
		$league_away_team = $this->league_model->get_away_team($fixture_id);
		$league_fixture_date = $this->league_model->get_league_fixture_date($fixture_id);
		$league_count++;
		
		//payment_type for referee
		$league_payment_amount = $this->payments_model->get_payment_amount($league_referee_type);
		//get ref status in the referee queue
		$league_referee_payment_status = $this->league_model->get_league_referee_payment_status($league_referee_id,$fixture_id,1);
		
		$league_referee_payments_queue_id = $this->payments_model->get_league_queue_id($league_referee_id, $fixture_id);
		$league_fixture_type = 1;
		
		if($league_referee_payment_status == 0)
		{
			$league_status = '<span class="label label-default">Waiting Confirmation</span>';
		}
		elseif($league_referee_payment_status == 1)
		{
			$league_status = '<span class="label label-warning">Sent to Admin</span>';
		}
		elseif($league_referee_payment_status == 2)
		{
			$league_status = '<span class="label label-danger">Sent to Accounts</span>';
			$league_payment_button = '<a class="btn btn-warning" href="'.site_url().'soccer-management/pay-league-referee/'.$league_referee_id.'/'.$league_referee_payments_queue_id.'/'.$league_fixture_type.'"  title="Pay '.$league_referee_name.'"><i class="fa fa-money"></i> Make Payment</a>';
		}
		elseif($league_referee_payment_status == 3)
		{
			$league_status = '<span class="label label-success">Accounts Recieved</span>';
			$league_payment_button = '<a class="btn btn-warning" href="'.site_url().'soccer-management/pay-league-referee/'.$league_referee_id.'/'.$league_referee_payments_queue_id.'/'.$league_fixture_type.'"  title="Pay '.$league_referee_name.'"><i class="fa fa-money"></i> Make Payment</a>';
		}
		else
		{
			$league_status = '<span class="label label-default">Waiting Confirmation</span>';
			$league_payment_button = '<a class="btn btn-warning" href="'.site_url().'soccer-management/pay-league-referee/'.$league_referee_id.'/'.$league_referee_payments_queue_id.'/'.$league_fixture_type.'"  title="Pay '.$league_referee_name.'"><i class="fa fa-money"></i> Make Payment</a>';
		}
		$league_payment_referees_result.= 
		'
			<tr>
				<td>'.$league_count.'</td>
				<td>'.$league_referee_name.'</td>
				<td>'.$league_referee_role.'</td>
				<td>'.date('jS M Y',strtotime($league_fixture_date)).'</td>
				<td>'.$league_home_team.' VS '.$league_away_team.'</td>
				<td>'.number_format($league_payment_amount,2).'</td>
				<td>'.$league_status.'</td>
				<td><a class="btn btn-default" href="'.site_url().'soccer_management/league/view_referee_queue/'.$league_referee_id.'/'.$league_referee_payments_queue_id.'/'.$league_fixture_type.'/'.$fixture_id.'"  title="View '.$league_referee_name.'"><i class="fa fa-eye"></i>View</a></td>
				<td>'.$league_payment_button.'</td>
			</tr>
		';
	}
	$league_payment_referees_result.='
			</tbody>
		</table>';
}
else
{
	$league_payment_referees_result.= "No referees have been sent to the accounts queue";
}
?>

<?php
//check if youth league is accessed
$youth_teams = $this->session->userdata('youth_teams');
if(!empty($youth_teams))
{
}
else
{
	?>
	<section class="panel">
		<header class="panel-heading">						
			<h2 class="panel-title"><?php echo "Tournament Payments";?></h2>
		</header>
		<div class="panel-body">
			<?php
		   
			$success = $this->session->userdata('success_message');
	
			if(!empty($success))
			{
				echo '<div class="alert alert-success"> <strong>Success!</strong> '.$success.' </div>';
				$this->session->unset_userdata('success_message');
			}
			
			$error = $this->session->userdata('error_message');
			
			if(!empty($error))
			{
				echo '<div class="alert alert-danger"> <strong>Oh snap!</strong> '.$error.' </div>';
				$this->session->unset_userdata('error_message');
			}
			?>
			
			<div class="table-responsive">
				
				<?php echo $result;?>
		
			</div>
		</div>
		<div class="panel-footer">
			<?php if(isset($links)){echo $links;}?>
		</div>
	</section>
    <?php
}
?>
<section class="panel">
    <header class="panel-heading">
    <?php
    	$youth_teams = $this->session->userdata('youth_teams');
		if(!empty($youth_teams))
		{
			?>
            <h2 class="panel-title"><?php echo "Youth League Payments";?></h2>
            <?php
		}
		else
		{
			?>
            <h2 class="panel-title"><?php echo "League Payments";?></h2>
            <?php
		}
		?>
        
    </header>
    <div class="panel-body">
        <?php
       
        $success = $this->session->userdata('success_message');

        if(!empty($success))
        {
            echo '<div class="alert alert-success"> <strong>Success!</strong> '.$success.' </div>';
            $this->session->unset_userdata('success_message');
        }
        
        $error = $this->session->userdata('error_message');
        
        if(!empty($error))
        {
            echo '<div class="alert alert-danger"> <strong>Oh snap!</strong> '.$error.' </div>';
            $this->session->unset_userdata('error_message');
        }
        ?>
        
        <div class="table-responsive">
            
            <?php echo $league_payment_referees_result;?>
    
        </div>
    </div>
    <div class="panel-footer">
        <?php if(isset($links)){echo $links;}?>
    </div>
</section>