<?php
//seasons data
$tournament_tournament_duration_start_date = set_value('tournament_duration_start_date');
$tournament_tournament_duration_end_date = set_value('tournament_duration_end_date');
$result = '';
if($tournament_durations->num_rows() > 0)
{
	$count = 0;
	$result .= 
			'
			<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>Tournament</th>
						<th>Start Date</a></th>
						<th>End Date</a></th>
						<th>Status</a></th>
						<th colspan="6">Actions</th>
					</tr>
				</thead>
				  <tbody>
				  
			';
	foreach($tournament_durations->result() as $duration)
	{
		$tournament_duration_id = $duration->tournament_duration_id;
		$tournament_duration_start_date = $duration->tournament_duration_start_date;
		$tournament_duration_end_date = $duration->tournament_duration_end_date;
		$tournament_duration_status = $duration->tournament_duration_status;
		
		//create deactivated status display
		if($tournament_duration_status == 0)
		{
			$status = '<span class="label label-default">Deactivated</span>';
			$button = '<a class="btn btn-info" href="'.site_url().'soccer-management/activate-tournament-duration/'.$tournament_duration_id.'/'.$tournament_id.'" onclick="return confirm(\'Do you want to activate season'.$tournament_duration_id.'?\');" title="Activate season'.$tournament_duration_id.'"><i class="fa fa-thumbs-up"></i> Activate</a>';
		}
		//create activated status display
		else if($tournament_duration_status == 1)
		{
			$status = '<span class="label label-success">Active</span>';
			$button = '<a class="btn btn-default" href="'.site_url().'soccer-management/deactivate-tournament-duration/'.$tournament_duration_id.'/'.$tournament_id.'" onclick="return confirm(\'Do you want to deactivate season'.$tournament_duration_id.'?\');" title="Dectivate season'.$tournament_duration_id.'"><i class="fa fa-thumbs-down"></i>Deactivate</a>';
		}
		
		
		$count++;
		
		$result .= 
		'
			<tr>
				<td>'.$count.'</td>
				<td>'.date('jS M Y',strtotime($tournament_duration_start_date)).'</td>
				<td>'.date('jS M Y',strtotime($tournament_duration_end_date)).'</td>
				<td>'.$status.'</td>
				<td>'.$button.'</td>
				<td><a href="'.site_url().'soccer-management/edit-tournament-duration/'.$tournament_duration_id.'/'.$tournament_id.'" class="btn btn-sm btn-success" title="Edit season'.$tournament_duration_id.'"><i class="fa fa-pencil"></i> Edit Season</a></td>
				<td><a href="'.site_url().'soccer-management/delete-tournament-duration/'.$tournament_duration_id.'/'.$tournament_id.'" class="btn btn-sm btn-danger" title="Delete season'.$tournament_duration_id.'"><i class="fa fa-trash"></i> Delete Season</a></td>
				<td><a href="'.site_url().'soccer-management/add-tournament-duration-team/'.$tournament_duration_id.'/'.$tournament_id.'" class="btn btn-sm btn-warning" title="Add team for season'.$tournament_duration_id.'"><i class="fa fa-plus"></i> Add Team</a></td>
				<td><a href="'.site_url().'soccer-management/add-tournament-duration-fixture/'.$tournament_duration_id.'/'.$tournament_id.'" class="btn btn-sm btn-default" title="Add fixtures for season'.$tournament_duration_id.'"><i class="fa fa-calendar-check-o""></i> Add Fixture</a></td>
				<td><a href="'.site_url().'soccer-management/view-tournament-duration-table/'.$tournament_duration_id.'/'.$tournament_id.'" class="btn btn-sm btn-info" title="Tournament Standings for season'.$tournament_duration_id.'"><i class="fa fa-calendar-check-o""></i> Tournament Standings</a></td>
			</tr>
		';
	}
}
else
{
	$result.= 'There are no added seasons for this tournament';
}
?>
<section class="panel">
    <header class="panel-heading">						
        <h2 class="panel-title"><?php echo $title;?></h2>
    </header>
    <div class="panel-body">
    	 <?php echo form_open_multipart(''.site_url().'soccer-management/add-tournament-duration/'.$tournament_id.'', array("class" => "form-horizontal", "role" => "form"));?>
    	<div class="row">
        	<div class="col-md-6">
            	 <div class="form-group">
                    <label class="col-lg-5 control-label">Start Date: </label>
                    
                    <div class="col-lg-7">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </span>
                            <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="tournament_duration_start_date" placeholder="Start Date" value="<?php echo $tournament_tournament_duration_start_date;?>">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
            	<div class="form-group">
                    <label class="col-lg-5 control-label">End Date: </label>
                    
                    <div class="col-lg-7">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </span>
                            <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="tournament_duration_end_date" placeholder="End Date" value="<?php echo $tournament_tournament_duration_end_date;?>">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" style="margin-top:10px;">
			<div class="col-md-12">
				<div class="form-actions center-align">
					<button class="submit btn btn-primary" type="submit">
						Add season
					</button>
				</div>
			</div>
		</div>
        <?php echo form_close();?>
    </div>
</section>
<section class="panel">
    <header class="panel-heading">						
        <h2 class="panel-title"><?php echo $tournament_name;?></h2>
    </header>
    <div class="panel-body">
        <?php
        $search = $this->session->userdata('coach_search_title2');
        
        if(!empty($search))
        {
            echo '<h6>Filtered by: '.$search.'</h6>';
            echo '<a href="'.site_url().'soccer_management/coach/close_search" class="btn btn-sm btn-info pull-left">Close search</a>';
        }
        $success = $this->session->userdata('success_message');

        if(!empty($success))
        {
            echo '<div class="alert alert-success"> <strong>Success!</strong> '.$success.' </div>';
            $this->session->unset_userdata('success_message');
        }
        
        $error = $this->session->userdata('error_message');
        
        if(!empty($error))
        {
            echo '<div class="alert alert-danger"> <strong>Oh snap!</strong> '.$error.' </div>';
            $this->session->unset_userdata('error_message');
        }
        ?>
        <div class="row" style="margin-bottom:20px;">
            <div class="col-lg-12">
                <a href="<?php echo site_url();?>soccer-management/tournament" class="btn btn-sm btn-info pull-right">Back to Tournaments</a>
            </div>
        </div>
        <div class="table-responsive">
            
            <?php echo $result;?>
    
        </div>
    </div>
</section>