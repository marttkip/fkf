<?php
//tournament data
$season_team_id = set_value('team_id');
$result = '';
if($tournament_teams->num_rows() > 0)
{
	$count = 0;
	$result .= 
			'
			<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th>Team name</a></th>
						<th colspan="4">Actions</th>
					</tr>
				</thead>
				  <tbody>
				  
			';
	foreach($tournament_teams->result() as $all_teams)
	{
		$team_name = $all_teams->team_name;
		$tournament_team_id = $all_teams->tournament_team_id;
		$team_id = $all_teams->team_id;
		$count++;
		$result .=
				'
				<tr>
					<td>'.$count.'</td>
					<td>'.$team_name.'</td>
					<td><a class="btn btn-info" href="'.site_url().'soccer-management/edit-team/'.$team_id.'" class="btn btn-sm btn-success" title="Edit '.$team_name.'"><i class="fa fa-pencil"></i> Edit Team</a></td>
					<td><a class="btn btn-info" href="'.site_url().'soccer-management/remove-tournament-team/'.$team_id.'/'.$tournament_id.'/'.$tournament_duration_id.'" class="btn btn-sm btn-danger" title="Remove '.$team_name.' from tournament"><i class="fa fa-pencil"></i> Remove Team</a></td>
				</tr>
				';
	}
}
else
{
	$result .= 'There are no teams in the season for this tournament';
}
?>          
          <section class="panel">
                <header class="panel-heading">
                    <h2 class="panel-title"><?php echo $title;?></h2>
                </header>
                <div class="panel-body">
                	<div class="row" style="margin-bottom:20px;">
                    	<div class="col-lg-offset-8 col-lg-2">
                            <a href="<?php echo base_url().'soccer-management/add-tournament-duration-fixture/'.$tournament_duration_id.'/'.$tournament_id;?>" class="btn btn-success pull-right">Add Fixture</a>
                        </div>
                        <div class="col-lg-2">
                            <a href="<?php echo base_url().'soccer-management/add-tournament-duration/'.$tournament_id;?>" class="btn btn-info pull-right">Back to season</a>
                        </div>
                    </div>
                        
                    <!-- Adding Errors -->
                    <?php
						$success = $this->session->userdata('success_message');
						$error = $this->session->userdata('error_message');
						
						if(!empty($success))
						{
							echo '
								<div class="alert alert-success">'.$success.'</div>
							';
							
							$this->session->unset_userdata('success_message');
						}
						
						if(!empty($error))
						{
							echo '
								<div class="alert alert-danger">'.$error.'</div>
							';
							
							$this->session->unset_userdata('error_message');
						}
						$validation_errors = validation_errors();
						
						if(!empty($validation_errors))
						{
							echo '<div class="alert alert-danger"> Oh snap! '.$validation_errors.' </div>';
						}
                    ?>
                    
                    <?php echo form_open($this->uri->uri_string(), array("class" => "form-horizontal", "role" => "form"));?>
<div class="row">
	<div class="col-md-6">
    	<div class="form-group">
            <label class="col-lg-5 control-label">Team: </label>
            <div class="col-lg-7">
                <select class="form-control" name="team_id">
                	<option value="--Select Team--" selected>--Select Team--</option>
                	<?php
                    	if($teams->num_rows()> 0)
						{
							foreach($teams->result() as $res)
							{
								$db_team_id = $res->team_id;
								$team_name = $res->team_name;
								
								if($db_team_id == $season_team_id)
								{
									echo '<option value="'.$db_team_id.'" selected>'.$team_name.'</option>';
								}
								
								else
								{
									echo '<option value="'.$db_team_id.'">'.$team_name.'</option>';
								}
							}
						}
					?>
                </select>
            </div>
        </div>
	</div>
</div>
<div class="row" style="margin-top:10px;">
	<div class="col-md-12">
        <div class="form-actions center-align">
            <button class="submit btn btn-primary" type="submit">
                Add team
            </button>
        </div>
    </div>
</div>
                    <?php echo form_close();?>
                </div>
            </section>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Teams in <?php echo $tournament_name;?> season <?php echo $tournament_duration_id;?></h2>
    </header>
    <div class="panel-body">
    	<div class="table-responsive">
            
            <?php echo $result;?>
    
        </div>
    </div>
</section>