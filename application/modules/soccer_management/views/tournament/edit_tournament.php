<?php
//tournament data
$row = $tournament->row();

$tournament_name = $row->tournament_name;
$tournament_type_id = $row->tournament_type_id;

//repopulate data if validation errors occur
$validation_error = validation_errors();
				
if(!empty($validation_error))
{
	$tournament_name = set_value('tournament_name');
	$tournament_type_id = set_value('tournament_type_id');
}
?>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">About <?php echo $tournament_name;?></h2>
    </header>
    <div class="panel-body">
    <!-- Adding Errors -->
    <?php
    if(isset($error)){
        echo '<div class="alert alert-danger"> Oh snap! Change a few things up and try submitting again. </div>';
    }
    if(!empty($validation_errors))
    {
        echo '<div class="alert alert-danger"> Oh snap! '.$validation_errors.' </div>';
    }

    ?>
            
<?php echo form_open_multipart(''.site_url().'soccer-management/edit-tournament/'.$tournament_id.'', array("class" => "form-horizontal", "role" => "form"));?>
<div class="row">
	<div class="col-md-6">
    	<div class="form-group">
            <label class="col-lg-5 control-label">Tournament name: </label>
            
            <div class="col-lg-7">
            	<input type="text" class="form-control" name="tournament_name" placeholder="Tournament name" value="<?php echo $tournament_name;?>">
            </div>
        </div>
	</div>
    <div class="col-md-6">
    	<div class="form-group">
            <label class="col-lg-5 control-label">Tournament type: </label>
            
            <div class="col-lg-7">
            	<select class="form-control" name="tournament_type_id">
                	<?php
                    	if($tournament_types->num_rows() > 0)
						{
							echo '<option value="" selected>--Select Tournamen type--</option>';
							$tournament_type = $tournament_types->result();
							
							foreach($tournament_type as $res)
							{
								$db_tournament_type_id = $res->tournament_type_id;
								$tournament_type_name = $res->tournament_type_name;
								
								if($db_tournament_type_id== $tournament_type_id)
								{
									echo '<option value="'.$db_tournament_type_id.'" selected>'.$tournament_type_name.'</option>';
								}
								
								else
								{
									echo '<option value="'.$db_tournament_type_id.'">'.$tournament_type_name.'</option>';
								}
							}
						}
					?>
                </select>
            </div>
        </div>
	</div>
</div>
<div class="row" style="margin-top:10px;">
	<div class="col-md-12">
        <div class="form-actions center-align">
            <button class="submit btn btn-primary" type="submit">
                Edit tournament
            </button>
        </div>
    </div>
</div>
            <?php echo form_close();?>
                </div>
            </section>