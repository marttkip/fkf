<?php
$payment_amount = set_value('payment_amount');
?>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title"><?php echo $title;?></h2>
    </header>
    <div class="panel-body">
    	<?php
			$success = $this->session->userdata('success_message');
			$error = $this->session->userdata('error_message');
			
			if(!empty($success))
			{
				echo '
					<div class="alert alert-success">'.$success.'</div>
				';
				
				$this->session->unset_userdata('success_message');
			}
			
			if(!empty($error))
			{
				echo '
					<div class="alert alert-danger">'.$error.'</div>
				';
				
				$this->session->unset_userdata('error_message');
			}
			$validation_errors = validation_errors();
			
			if(!empty($validation_errors))
			{
				echo '<div class="alert alert-danger"> Oh snap! '.$validation_errors.' </div>';
			}
		?>
    	 <?php echo form_open($this->uri->uri_string(), array("class" => "form-horizontal", "role" => "form"));?>
         <div class="row">
         	<div class="col-md-6">
            	<div class="form-group">
                    <label class="col-lg-5 control-label">Amount to Pay: </label>
                    
                    <div class="col-lg-7">
                        <input type="text" class="form-control" name="payment_amount" placeholder="Amount to Pay" value="<?php echo $payment_amount;?>">
                    </div>
                </div>
            </div>
            <div class="col-md-6">
            	<div class="form-actions center-align">
                    <button class="submit btn btn-primary" type="submit">
                        Add payment
                    </button>
                </div>
            </div>
         </div>
         <?php echo form_close();?>
    </div>
</section>