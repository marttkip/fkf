<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title"><?php echo $title;?></h2>
    </header>
    <div class="panel-body">
    	<div class="row">
        	<div class="col-md-6">
            	<section class="panel">
                <header class="panel-heading">
                    <h2 class="panel-title"><?php echo $title;?> Invoices</h2>
                </header>
                <div class="panel-body">
                	<table class="table table-bordered table-striped table-condensed">
                    	<thead>
                        	<tr>
                            	<th>Fixture</th>
                                <th>Game Date</th>
                                <th>Invoice Amount</th>
                            </tr>
                        </thead>
                        <tbody>
                        	<tr>
                            	<td><?php echo $home_team.' VS '.$away_team;?></td>
                                <td><?php echo date('jS M Y',strtotime($fixture_date));?></td>
                                <td><?php echo number_format($referee_invoice,2);?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-md-6">
            	<section class="panel">
                <header class="panel-heading">
                    <h2 class="panel-title"><?php echo $title;?> Payments</h2>
                </header>
                <div class="panel-body">
                	<table class="table table-bordered table-striped table-condensed">
                    	<thead>
                        	<tr>
                            	<th>Fixture</th>
                                <th>Game Date</th>
                                <th>Payment Amount</th>
                            </tr>
                        </thead>
                        <tbody>
                        	<tr>
                            	<td><?php echo $home_team.' VS '.$away_team;?></td>
                                <td><?php echo date('jS M Y',strtotime($fixture_date));?></td>
                                <td><?php echo number_format($referee_payments,2);?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="row">
        	<div class="form-actions center-align">
                <strong><h3>The balance is : <?php echo number_format($referee_invoice - $referee_payments,3);?></h3></strong>
            </div>
        </div>
    </div>
</section>