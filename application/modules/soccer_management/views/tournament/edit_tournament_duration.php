<?php
//tournament seasons data
$row = $tournament_duration->row();

$tournament_duration_start_date = $row->tournament_duration_start_date;
$tournament_duration_end_date = $row->tournament_duration_end_date;
//repopulate data if validation errors occur
$validation_error = validation_errors();
				
if(!empty($validation_error))
{
	$tournament_duration_start_date = set_value('tournament_duration_start_date');
	$tournament_duration_end_date = set_value('tournament_duration_end_date');
}
?>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Edit Season <?php echo $tournament_duration_id;?> of <?php echo $tournament_name;?></h2>
    </header>
    <div class="panel-body">
    <!-- Adding Errors -->
    <?php
    if(isset($error)){
        echo '<div class="alert alert-danger"> Oh snap! Change a few things up and try submitting again. </div>';
    }
    if(!empty($validation_errors))
    {
        echo '<div class="alert alert-danger"> Oh snap! '.$validation_errors.' </div>';
    }

    ?>
            
		<?php echo form_open($this->uri->uri_string(), array("class" => "form-horizontal", "role" => "form"));?>
            <div class="row">
                <div class="col-md-6">
                     <div class="form-group">
                        <label class="col-lg-5 control-label">Start Date: </label>
                        
                        <div class="col-lg-7">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                                <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="tournament_duration_start_date" placeholder="Start Date" value="<?php echo $tournament_duration_start_date;?>">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-lg-5 control-label">End Date: </label>
                        
                        <div class="col-lg-7">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                                <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="tournament_duration_end_date" placeholder="End Date" value="<?php echo $tournament_duration_end_date;?>">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-top:10px;">
                <div class="col-md-12">
                    <div class="form-actions center-align">
                        <button class="submit btn btn-primary" type="submit">
                            Edit season
                        </button>
                    </div>
                </div>
            </div>
        <?php echo form_close();?>
	</div>
</section>