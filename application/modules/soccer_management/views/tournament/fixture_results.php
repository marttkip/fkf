<?php
$fixture_team_player_id = set_value('tournament_fixture_team_player_id');
$comment_fixture_description = set_value('comment_fixture_description');
$assessment_fixture_description = set_value('assessment_fixture_description');
$foul_player_id = set_value('foul_player_id');
$foul_minute = set_value('foul_minute');
$action_id= set_value('action_id');
$foul_type_id= set_value('foul_type_id');
$goul_type_id = set_value('goul_type_id');
$goal_minute = set_value('goal_minute');
$result = '';
$foul_result = '';
$comment_result = '';
$assessment_result = '';
if($fixture_fouls->num_rows() > 0)
{
	$count = 0;
	$foul_result .=
				'
				<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th>Team</th>
						<th>Number</th>
						<th>Player name</th>
						<th>Minute</th>
						<th>Reason</th>
						<th>Action</th>
					</tr>
				</thead>
				  <tbody>
				';
	foreach($fixture_fouls->result() as $fouls_committed)
	{
		$action_name = $fouls_committed->action_name;
		$foul_type_name = $fouls_committed->foul_type_name;
		$foul_team_name = $fouls_committed->team_name;
		$foul_player_number = $fouls_committed->player_number;
		$player_fname = $fouls_committed->player_fname;
		$player_onames = $fouls_committed->player_onames;
		$foul_player_name = $player_fname. ' ' .$player_onames;
		$foul_minute_time = $fouls_committed->foul_minute;
		$count++;
		$foul_result .=
					'
					<tr>
						<td>'.$count.'</td>
						<td>'.$foul_team_name.'</td>
						<td>'.$foul_player_number.'</td>
						<td>'.$foul_player_name.'</td>
						<td>'.$foul_minute_time.'</td>
						<td>'.$foul_type_name.'</td>
						<td>'.$action_name.'</td>
					</tr>
					';
		
	}
	$foul_result .='</tbody>
				</table>';
}
else
{
	$foul_result .= "No foouls committed in the match";
}
if($total_fixture_goal->num_rows() > 0)
{
	$count = 0;
	$result.= 
			'
			<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th>Team</th>
						<th>Number</th>
						<th>Player name</th>
						<th>Minute</th>
						<th>Goal Type</th>
					</tr>
				</thead>
				  <tbody>
				  
			';
	foreach($total_fixture_goal->result() as $fixture_goals)
	{
		
		$team_name = $fixture_goals->team_name;
		$player_number = $fixture_goals->player_number;
		$player_fname = $fixture_goals->player_fname;
		$player_onames = $fixture_goals->player_onames;
		$player_name = $player_fname. ' ' .$player_onames;
		$score_minute = $fixture_goals->goal_minute;
		$goal_type = $fixture_goals->goal_type_name;
		$count++;
		
		$result.=
				'
				<tr>
					<td>'.$count.'</td>
					<td>'.$team_name.'</td>
					<td>'.$player_number.'</td>
					<td>'.$player_name.'</td>
					<td>'.$score_minute.'</td>
					<td>'.$goal_type.'</td>
				</tr>
				';
	}
	$result .='</tbody>
				</table>';
}
else
{
	$result .= 'No goals scored';
}

if($all_fixture_comments->num_rows() > 0)
{
	$comment_result .= 
				'<table class="table table-bordered table-striped table-condensed">
					<thead>
						<tr>
							<th>#</th>
							<th>Comment</th>
							<th>Comment description</th>
						</tr>
					</thead>
					  <tbody>
				  ';
	$count=0;	
	foreach($all_fixture_comments->result() as $comments_of_fixture)
	{
		$comment_id = $comments_of_fixture->comment_id;
		$comment_fixture_description = $comments_of_fixture->comment_fixture_description;
		$comment_name = $comments_of_fixture->comment_name;
		$count++;
		$comment_result .= 
						'<tr>
							<td>'.$count.'</td>
							<td>'.$comment_name.'</td>
							<td>'.$comment_fixture_description.'</td>
						</tr>
						';
	}
	$comment_result .=
	'
					</tbody>
				</table>';
}
else
{
	$comment_result .= 'No comments for this fixture';
}
if($all_assessment_fixtures->num_rows() > 0)
{
	$assessment_count = 0;
	$assessment_result .=
	'<table class="table table-bordered table-striped table-condensed">
					<thead>
						<tr>
							<th>#</th>
							<th>Assessmnet</th>
							<th>Comment description</th>
							<th>Rating</th>
						</tr>
					</thead>
					  <tbody>
				  ';
	foreach($all_assessment_fixtures->result() as $assessments_of_fixture)
	{
		$assessment_id = $assessments_of_fixture->assessment_id;
		$assessment_fixture_description = $assessments_of_fixture->assessment_fixture_description;
		$assessment_name = $assessments_of_fixture->assessment_name;
		$assessment_rating = $assessments_of_fixture->assessment_fixture_rating;
		$assessment_count++;
		$assessment_result .= 
						'<tr>
							<td>'.$assessment_count.'</td>
							<td>'.$assessment_name.'</td>
							<td>'.$assessment_fixture_description.'</td>
							<td>'.$assessment_rating.'</td>
						</tr>
						';
	}
	$assessment_result .=
	'
					</tbody>
				</table>';
}
else
{
	$assessment_result .= 'No assessments added to this fixture';
}
?>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title"><?php echo $title;?></h2>
    </header>
    <div class="panel-body">
    	<div class="row">
            <div class="col-md-offset-8 col-md-2">
                    <button type="button" class="btn btn-primary btn-sm pull-right" data-toggle="modal" data-target="#add_scores">
                        View Score
                    </button>
            </div>
            <div class="col-md-2">
                    <a href="<?php echo base_url().'soccer-management/add-tournament-duration-fixture/'.$tournament_duration_id.'/'.$tournament_id;?>" class="btn btn-info pull-right">Back to fixture</a>
            </div>
        </div>
         <!-- Adding Errors -->
                    <?php
						$success = $this->session->userdata('success_message');
						$error = $this->session->userdata('error_message');
						
						if(!empty($success))
						{
							echo '
								<div class="alert alert-success">'.$success.'</div>
							';
							
							$this->session->unset_userdata('success_message');
						}
						
						if(!empty($error))
						{
							echo '
								<div class="alert alert-danger">'.$error.'</div>
							';
							
							$this->session->unset_userdata('error_message');
						}
						$validation_errors = validation_errors();
						
						if(!empty($validation_errors))
						{
							echo '<div class="alert alert-danger"> Oh snap! '.$validation_errors.' </div>';
						}
                    ?>
        <?php
			echo form_open('soccer_management/tournament/add_tournament_fixture_scores/'.$tournament_fixture_id.'/'.$tournament_duration_id.'/'.$tournament_id, array("class" => "form-horizontal", "role" => "form"));
		?>
        <div class="row">
        	<div class="col-md-6">
            	<div class="form-group">
                    <label class="col-lg-5 control-label">Player: </label>
                    <div class="col-lg-7">
                        <select class="form-control" name="tournament_fixture_team_player_id">
                            <?php
                                if($goal_scorer->num_rows()> 0)
                                {
									echo '<option value="--Select Scorer--" selected>--Select Scorer--</option>';
                                    foreach($goal_scorer->result() as $res)
                                    {
                                        $db_fixture_team_player_id = $res->fixture_team_player_id;
                                        $player_fname = $res->player_fname;
                                        $player_onames = $res->player_onames;
                                        $player_name = $player_fname. ' ' .$player_onames;
                                        
                                        if($db_fixture_team_player_id == $fixture_team_player_id)
                                        {
                                            echo '<option value="'.$db_fixture_team_player_id.'" selected>'.$player_name.'</option>';
                                        }
                                        
                                        else
                                        {
                                            echo '<option value="'.$db_fixture_team_player_id.'">'.$player_name.'</option>';
                                        }
                                    }
                                }
                            ?>
                        </select>
                    </div>
                </div>
            	<div class="form-group">
                    <label class="col-lg-5 control-label">Score Minute </label>
                    
                    <div class="col-lg-7">
                        <input type="text" class="form-control" name="goal_minute" placeholder="Minute" value="<?php echo $goal_minute;?>">
                    </div>
                </div>
            	<div class="form-group">
                    <label class="col-lg-5 control-label">Goal Type: </label>
                    <div class="col-lg-7">
                        <select class="form-control" name="goal_type_id">
                            <?php
                                if($goul_types->num_rows()> 0)
                                {
									echo '<option value="--Select Scorer--" selected>--Select Goal Type--</option>';
                                    foreach($goul_types->result() as $res)
                                    {
                                        $db_goul_type_id = $res->goal_type_id;
                                        $goul_type_name = $res->goal_type_name;
                                        
                                        if($db_goul_type_id == $goal_type_id)
                                        {
                                            echo '<option value="'.$db_goul_type_id.'" selected>'.$goul_type_name.'</option>';
                                        }
                                        
                                        else
                                        {
                                            echo '<option value="'.$db_goul_type_id.'">'.$goul_type_name.'</option>';
                                        }
                                    }
                                }
                            ?>
                        </select>
                    </div>
                </div>
				<div class="row" style="margin-top:10px;">
					<div class="col-md-12">
						<div class="form-actions center-align">
							<button class="submit btn btn-success" type="submit">
								Add scores
							</button>
						</div>
					</div>
				</div>
				<?php echo form_close();?>
			</div>

        	<div class="col-md-6">
				<?php
			echo form_open('soccer_management/tournament/add_fixture_fouls/'.$tournament_fixture_id.'/'.$tournament_duration_id.'/'.$tournament_id, array("class" => "form-horizontal", "role" => "form"));
		?>
            	<div class="form-group">
                    <label class="col-lg-5 control-label">Player: </label>
                    <div class="col-lg-7">
                        <select class="form-control" name="foul_player_id">
                            <?php
                                if($goal_scorer->num_rows()> 0)
                                {
									echo '<option value="--Select Offender--" selected>--Select Offender--</option>';
                                    foreach($goal_scorer->result() as $res)
                                    {
                                        $db_fixture_team_player_id = $res->fixture_team_player_id;
                                        $player_fname = $res->player_fname;
                                        $player_onames = $res->player_onames;
                                        $player_name = $player_fname. ' ' .$player_onames;
                                        
                                        if($db_fixture_team_player_id == $fixture_team_player_id)
                                        {
                                            echo '<option value="'.$db_fixture_team_player_id.'" selected>'.$player_name.'</option>';
                                        }
                                        
                                        else
                                        {
                                            echo '<option value="'.$db_fixture_team_player_id.'">'.$player_name.'</option>';
                                        }
                                    }
                                }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-5 control-label">Foul type: </label>
                    <div class="col-lg-7">
                        <select class="form-control" name="foul_type_id">
                            <?php
                                if($foul_types->num_rows()> 0)
                                {
									echo '<option value="--Select Offense--" selected>--Select Offense-</option>';
                                    foreach($foul_types->result() as $fouls)
                                    {
                                        $db_foul_type_id = $fouls->foul_type_id;
                                        $foul_type_name = $fouls->foul_type_name;
                                        
                                        if($db_foul_type_id == $foul_type_id)
                                        {
                                            echo '<option value="'.$db_foul_type_id.'" selected>'.$foul_type_name.'</option>';
                                        }
                                        
                                        else
                                        {
                                            echo '<option value="'.$db_foul_type_id.'">'.$foul_type_name.'</option>';
                                        }
                                    }
                                }
                            ?>
                        </select>
                    </div>
                </div>
				<div class="form-group">
                    <label class="col-lg-5 control-label">Foul Minute </label>
                    
                    <div class="col-lg-7">
                        <input type="text" class="form-control" name="foul_minute" placeholder="Minute" value="<?php echo $foul_minute;?>">
                    </div>
                </div>
            	<div class="form-group">
                    <label class="col-lg-5 control-label">Action: </label>
                    <div class="col-lg-7">
                        <select class="form-control" name="action_id">
                            <?php
                                if($action_types->num_rows()> 0)
                                {
									echo '<option value="--Select Action--" selected>--Select Action--</option>';
                                    foreach($action_types->result() as $actions)
                                    {
                                        $db_action_id = $actions->action_id;
                                        $action_name = $actions->action_name;
                                        
                                        if($db_action_id == $action_id)
                                        {
                                            echo '<option value="'.$db_action_id.'" selected>'.$action_name.'</option>';
                                        }
                                        
                                        else
                                        {
                                            echo '<option value="'.$db_action_id.'">'.$action_name.'</option>';
                                        }
                                    }
                                }
                            ?>
                        </select>
                    </div>
                </div>
				<div class="row" style="margin-top:10px;">
					<div class="col-md-12">
						<div class="form-actions center-align">
							<button class="submit btn btn-danger" type="submit">
								Add foul
							</button>
						</div>
					</div>
				</div>
				<?php echo form_close();?>
			</div>
        </div>
    </div>
        <div class="row" style="margin-bottom:20px;">
            <div class="modal fade" id="add_scores" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				<div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Scores</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-3"><label class="col-lg-12 control-label">Team</label>
                                </div>
                                <div class="col-md-3"><label class="col-lg-12 control-label">Half time score</label>
                                </div>
                                <div class="col-md-3"><label class="col-lg-12 control-label">Full time score</label>
                                </div>
                                <div class="col-md-3"><label class="col-lg-12 control-label">Penalty</label>
                                </div>
                            </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <div class="col-lg-12">
                                                <input type="text" class="form-control" readonly name="home_team"  value="Home: <?php echo $home_team;?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-lg-12">
                                                <input type="text" class="form-control" readonly name="away_team" value="Away: <?php echo $away_team;?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <div class="col-lg-12">
                                                <input type="readonly" class="form-control" name="half_time_score_home"  value="<?php //echo $half_time_score_home;?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-lg-12">
                                                <input type="readonly" class="form-control" name="half_time_score_away"  value="<?php //echo $half_time_score_away;?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <div class="col-lg-12">
                                                <input type="readonly" class="form-control" name="full_time_score_home"  value="<?php //echo $full_time_score_home;?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-lg-12">
                                                <input type="readonly" class="form-control" name="full_time_score_away"  value="<?php //echo $full_time_score_away;?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <div class="col-lg-12">
                                                <input type="readonly" class="form-control" name="home_penalty"  value="<?php //echo $home_penalty;?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-lg-12">
                                                <input type="readonly" class="form-control" name="away_penalty"  value="<?php //echo $away_penalty;?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
				</div>
			</div>
</section>
<section class="panel">
	<header class="panel-heading">
    	<h2 class="	panel-title">Add comments</h2>
    </header>
    <div class="panel-body">
    	<?php
		$add_comment_result = '';
        if($comment_type->num_rows() > 0)
        {
			
            foreach($comment_type->result() as $comments)
            {
                $comment_id = $comments->comment_id;
                $comment_name = $comments->comment_name;
                $add_comment_result .=
                form_open('soccer_management/tournament/post_fixture_comment/'.$comment_id.'/'.$tournament_fixture_id.'/'.$tournament_duration_id.'/'.$tournament_id).'
					<div class="col-md-6" style="margin-bottom: 0.5em;">
						<div class = "col-md-9">
							<div class="form-group">
								<label class="col-lg-5 control-label">'.$comment_name.' </label>
								<div class="col-lg-7">
									<textarea rows="4" cols="5" class="form-control" name="comment_fixture_description" placeholder="'.$comment_name.'" value="'.$comment_fixture_description.'"></textarea>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<button type="submit" class="btn btn-sm btn-warning" title="Add '.$comment_name.'"><i class="fa fa-plus"></i>Add Fixture Comment</button>
						</div>
                    </div>
                '.form_close();
            }
        }
		echo $add_comment_result;
        ?>
    </div>
</section>
<section class="panel">
	<header class="panel-heading">
    	<h2 class="	panel-title">Add assessments</h2>
    </header>
    <div class="panel-body">
    	<?php
		$add_assessment_result = '';
        if($assessment_type->num_rows() > 0)
        {
			
            foreach($assessment_type->result() as $assessments)
            {
                $assessment_id = $assessments->assessment_id;
                $assessment_name = $assessments->assessment_name;
				$assessment_parent  =$assessments->assessment_parent;
				$assessment_parent_name = $this->fixture_model->get_parent_name($assessment_parent);
                $add_assessment_result .=
				form_open('soccer_management/tournament/post_fixture_assignment/'.$assessment_id.'/'.$tournament_fixture_id.'/'.$tournament_duration_id.'/'.$tournament_id).'
                
                    <div class="col-md-6" style="margin-bottom: 0.5em;">
						<div class = "col-md-9">
							<div class="form-group">
								<div class="col-lg-12">
									  <input type="radio" name="assessment_name" value="1"> 1
									  <input type="radio" name="assessment_name" value="2"> 2
									  <input type="radio" name="assessment_name" value="3"> 3
									  <input type="radio" name="assessment_name" value="4"> 4
									  <input type="radio" name="assessment_name" value="5"> 5
									  <input type="radio" name="assessment_name" value="6"> 6
									  <input type="radio" name="assessment_name" value="7"> 7
									  <input type="radio" name="assessment_name" value="8"> 8
									  <input type="radio" name="assessment_name" value="9"> 9
									  <input type="radio" name="assessment_name" value="10"> 10
									<textarea rows="4" cols="5" class="form-control" name="assessment_fixture_description" placeholder="'.$assessment_name.'" value="'.$assessment_fixture_description.'"></textarea>
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<button type="submit" class="btn btn-sm btn-success" title="Add '.$assessment_name.'"><i class="fa fa-plus"></i>Add Fixture Assessment</a>
						</div>
                    </div>
                '.form_close();
            }
        }
		echo $add_assessment_result;
        ?>
    </div>
</section>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Goals</h2>
    </header>
    <div class="panel-body">
    	<div class="table-responsive">
            <?php echo $result;?>
        </div> 
    </div>
</section>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Fouls</h2>
    </header>
    <div class="panel-body">
    	<div class="table-responsive">
            <?php echo $foul_result;?>
        </div> 
    </div>
</section>  
<section class="panel">
	<header class="panel-heading">
        <h2 class="panel-title">Comments</h2>
    </header>
    <div class="panel-body">
		
    	<div class="table-responsive">
            <?php echo $comment_result;?>
        </div> 
    </div>
</section>
<section class="panel">
	<header class="panel-heading">
        <h2 class="panel-title">Assessments</h2>
    </header>
    <div class="panel-body">
		
    	<div class="table-responsive">
            <?php echo $assessment_result;?>
        </div> 
    </div>
</section> 