<?php
$row = $referee_details->row();
$referee_fname = '';
$referee_onames = '';
if(!empty($row))
{
	$referee_onames = $row->personnel_onames;
	$referee_fname = $row->personnel_fname;
}
?>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title"><?php echo $referee_fname.' ' .$referee_onames." League Fixtures";?></h2>
    </header>
     <?php
		$success = $this->session->userdata('success_message');
		$error = $this->session->userdata('error_message');
		
		if(!empty($success))
		{
			echo '
				<div class="alert alert-success">'.$success.'</div>
			';
			
			$this->session->unset_userdata('success_message');
		}
		
		if(!empty($error))
		{
			echo '
				<div class="alert alert-danger">'.$error.'</div>
			';
			
			$this->session->unset_userdata('error_message');
		}
		$validation_errors = validation_errors();
		
		if(!empty($validation_errors))
		{
			echo '<div class="alert alert-danger"> Oh snap! '.$validation_errors.' </div>';
		}
	    //Widget content>
	    $tournament_fixture_result = '';
	    //get all league fixtures assigned to you
	    if($tournament_referee_games->num_rows() > 0)
	    {
			$count = 0;
			$tournament_fixture_result .= '
					<table class="table table-bordered table-striped table-condensed">
						<thead>
							<tr>
								<th>#</th>
								<th>Fixture Teams</a></th>
								<th colspan="3">Actions</th>
							</tr>
						</thead>
						<tbody>
						';	
						
	       foreach($tournament_referee_games->result() as $tournamnet_games)
	       {
	            $torna_fixture_id = $tournamnet_games->tournament_fixture_id;
	            $torna_home_team = $this->tournament_model->get_home_team($torna_fixture_id);
				$torna_away_team = $this->tournament_model->get_away_team($torna_fixture_id); 
	            $count++;
	            $tournament_fixture_result .= '
							<tr>
								<td>'.$count.'</td>
								<td>'.$torna_home_team. ' VS '.$torna_away_team.'</td>
								<td><a class="btn btn-sm btn-success" href="'.site_url().'soccer-management/referee-view-t-players/'.$torna_fixture_id.'" title="Post results "><i class="fa fa-user"></i> View Players</a>
								</td>
								<td><a class="btn btn-sm btn-info" href="'.site_url().'soccer_management/commissioner/add_torna_fixture_result/'.$torna_fixture_id.'" title="Post results "><i class="fa fa-plus-square-o"></i> Post Results</a></td>				
				</tr>
							</tr>';
	        }
			$tournament_fixture_result .= '
						</tbody>
					</table>
					';
	       
	    }
	    else
	    {
	        $tournament_fixture_result .= 'No tournament fixtures have been assigned to you';
	    }
    ?>
    <div class="panel-body">
    	<?php echo $tournament_fixture_result;?>
    </div>
</section>