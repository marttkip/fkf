<?php
$referee_result = '';
$league_referee_count = 0;
if($league_referees->num_rows() > 0)
{
	$referee_result.='
			<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th>Ref Name</th>
						<th>Ref Type</th>
						<th>Fixture</th>
						<th>Played on</th>
						<th>Paid</th>
						<th>Paid on</th>
					</tr>
				<thead>
				<tbody>
			';
	foreach($league_referees->result() as $ref_payment)
	{
		
		$fixture_id = $ref_payment->fixture_id;	
		$referee_type_name = $ref_payment->referee_type_name;		
		$referee_fname =$ref_payment->referee_fname;
		$referee_onames =$ref_payment->referee_onames;
		$referee_name = $referee_fname.' '.$referee_onames;
		
		$fixture_referee_details = $this->reports_model->league_fixture_details($fixture_id);
		$referee_name = '';

		$amount_paid = '-';
		$paid_on = '-';
		if($fixture_referee_details->num_rows() > 0)
		{
			foreach($fixture_referee_details->result() as $details)
			{
				$fixture_date = $details->fixture_date;
				$referee_onames = $details->referee_onames;
				$referee_fname = $details->referee_fname;
				$referee_id = $details->referee_id;
				$fixture_id = $details->fixture_id;
				$referee_name = $referee_fname.' '.$referee_onames;
			
				$home_team = $this->league_model->get_home_team($fixture_id);
				$away_team = $this->league_model->get_away_team($fixture_id);

				$ref_payments = $this->reports_model->referee_league_payements($referee_id,$fixture_id);

				if($ref_payments->num_rows() > 0)
				{
					foreach ($ref_payments->result() as $key => $value) {
						# code...
						$amount_paid = $value->amount_paid;
						$paid_on = $value->paid_on;
					}
				}
				
			}
		}		
		$league_referee_count++;
		$referee_result.='
						<tr>
							<td>'.$league_referee_count.'</td>
							<td>'.$referee_name.'</td>
							<td>'.$referee_type_name.'</td>
							<td>'.$home_team.' VS '.$away_team.'</td>
							<td>'.$fixture_date.'</td>
							<td>'.$amount_paid.'</td>
							<td>'.$paid_on.'</td>
						</tr>';
		
		
	}
	$referee_result .='</tbody>
				</table>';
}
else
{
	$referee_result .= 'No referees were paid';
}


$league_matches_result = '';
$league_count = 0;
if($league_matches->num_rows() > 0)
{
	$league_matches_result.='
			<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th>League</th>
						<th>Played On</th>
						<th>Fixture</th>
						<th>Referee</th>
					</tr>
				<thead>
				<tbody>
			';
	foreach($league_matches->result() as $ref_payment)
	{
		
		$league_name = $ref_payment->league_name;
		$fixture_id = $ref_payment->fixture_id;
		$fixture_date = $ref_payment->fixture_date;
		// $paid_on = $ref_payment->paid_on;
		// $referee_fname =$ref_payment->referee_fname;
		// $referee_onames =$ref_payment->referee_onames;
		// $referee_name = $referee_fname.' '.$referee_onames;
		
		$fixture_details = $this->reports_model->league_fixture_details($fixture_id);

		// $referee_type_name = $this->reports_model->referee_type($referee_id, $tournament_fixture_id);
		$referee_name = '';
		if($fixture_details->num_rows() > 0)
		{
			foreach($fixture_details->result() as $details)
			{
				$fixture_date = $details->fixture_date;
				$referee_onames = $details->referee_onames;
				$referee_fname = $details->referee_fname;
				$referee_name = $referee_fname.' '.$referee_onames;
			
				$home_team = $this->league_model->get_home_team($fixture_id);
				$away_team = $this->league_model->get_away_team($fixture_id);
				
			}
		}		
		
		$league_count++;
		
		$league_matches_result.='
						<tr>
							<td>'.$league_count.'</td>
							<td>'.strtoupper($league_name).'</td>
							<td>'.$fixture_date.'</td>
							<td>'.$home_team.' VS '.$away_team.'</td>
							<td>'.$referee_name.'</td>
						</tr>';
		
		
	}
	$league_matches_result .='</tbody>
				</table>';
}
else
{
	$league_matches_result .= 'No league matches ';
}



$tournament_matches_result = '';
$tournament_count = 0;
if($tournament_matches->num_rows() > 0)
{
	$tournament_matches_result.='
			<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th>Tournament</th>
						<th>Played On</th>
						<th>Fixture</th>
						<th>Referee</th>
					</tr>
				<thead>
				<tbody>
			';
	foreach($tournament_matches->result() as $ref_payment)
	{
		
		$tournament_name = $ref_payment->tournament_name;
		$tournament_fixture_id = $ref_payment->tournament_fixture_id;
		$tournament_fixture_date = $ref_payment->tournament_fixture_date;
		// $paid_on = $ref_payment->paid_on;
		// $referee_fname =$ref_payment->referee_fname;
		// $referee_onames =$ref_payment->referee_onames;
		// $referee_name = $referee_fname.' '.$referee_onames;
		
		$fixture_details = $this->reports_model->tournament_fixture_details_report($tournament_fixture_id);

		// $referee_type_name = $this->reports_model->referee_type($referee_id, $tournament_fixture_id);
		$referee_name = '';
		if($fixture_details->num_rows() > 0)
		{
			foreach($fixture_details->result() as $details)
			{
				$tournament_fixture_date = $details->tournament_fixture_date;
				$referee_onames = $details->referee_onames;
				$referee_fname = $details->referee_fname;
				$referee_name = $referee_fname.' '.$referee_onames;
			
				$home_team = $this->tournament_model->get_home_team($tournament_fixture_id);
				$away_team = $this->tournament_model->get_away_team($tournament_fixture_id);
				
			}
		}		
		
		$tournament_count++;
		
		$tournament_matches_result.='
						<tr>
							<td>'.$tournament_count.'</td>
							<td>'.strtoupper($tournament_name).'</td>
							<td>'.$tournament_fixture_date.'</td>
							<td>'.$home_team.' VS '.$away_team.'</td>
							<td>'.$referee_name.'</td>
						</tr>';
		
		
	}
	$tournament_matches_result .='</tbody>
				</table>';
}
else
{
	$tournament_matches_result .= 'No tournament matches ';
}




$tournament_referee_result = '';
$league_referee_count = 0;
if($tournament_referees->num_rows() > 0)
{
	$tournament_referee_result.='
			<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th>Ref Name</th>
						<th>Ref Type</th>
						<th>Fixture</th>
						<th>Played on</th>
						<th>Paid</th>
						<th>Paid on</th>
					</tr>
				<thead>
				<tbody>
			';
	foreach($tournament_referees->result() as $ref_payment)
	{
		
		$tournament_fixture_id = $ref_payment->tournament_fixture_id;	
		$referee_type_name = $ref_payment->referee_type_name;		
		$referee_fname =$ref_payment->referee_fname;
		$referee_onames =$ref_payment->referee_onames;
		$referee_name = $referee_fname.' '.$referee_onames;
		
		$fixture_referee_details = $this->reports_model->tournament_fixture_details_report($tournament_fixture_id);
		$referee_name = '';

		$amount_paid = '-';
		$paid_on = '-';
		if($fixture_referee_details->num_rows() > 0)
		{
			foreach($fixture_referee_details->result() as $details)
			{
				$fixture_date = $details->fixture_date;
				$referee_onames = $details->referee_onames;
				$referee_fname = $details->referee_fname;
				$referee_id = $details->referee_id;
				$fixture_id = $details->fixture_id;
				$referee_name = $referee_fname.' '.$referee_onames;
			
				$home_team = $this->tournament_model->get_home_team($fixture_id);
				$away_team = $this->tournament_model->get_away_team($fixture_id);

				$ref_payments = $this->reports_model->referee_league_payements($referee_id,$tournament_fixture_id);

				if($ref_payments->num_rows() > 0)
				{
					foreach ($ref_payments->result() as $key => $value) {
						# code...
						$amount_paid = $value->amount_paid;
						$paid_on = $value->paid_on;
					}
				}
				
			}
		}		
		$league_referee_count++;
		$tournament_referee_result.='
						<tr>
							<td>'.$league_referee_count.'</td>
							<td>'.$referee_name.'</td>
							<td>'.$referee_type_name.'</td>
							<td>'.$home_team.' VS '.$away_team.'</td>
							<td>'.$fixture_date.'</td>
							<td>'.$amount_paid.'</td>
							<td>'.$paid_on.'</td>
						</tr>';
		
		
	}
	$tournament_referee_result .='</tbody>
				</table>';
}
else
{
	$tournament_referee_result .= 'No tournament referees';
}

?>
<div class="row">
	<div class="col-md-12">
		<section class="panel">
		    <header class="panel-heading">						
		        <h2 class="panel-title">Search</h2>
		    </header>
		    <div class="panel-body">
		    	<?php
	            echo form_open("soccer_management/reports/search_match_report", array("class" => "form-horizontal"));
	            ?>
	            <div class="row">
	            	<div class="col-md-12">
	            		<div class="col-md-8">
	            			<div class="form-group">
			                    <label class="col-md-4 control-label">Date of the week: </label>
			                    
			                    <div class="col-md-8">
			                        <div class="input-group">
		                                <span class="input-group-addon">
		                                    <i class="fa fa-calendar"></i>
		                                </span>
		                                <input data-format="yyyy-MM-dd" type="text" data-plugin-datepicker class="form-control" name="week_date" placeholder="Date of the week">
		                            </div>
			                    </div>
			                </div>
	            		</div>
	            		<div class="col-md-4">
	            			 <div class="center-align">
	                            <button type="submit" class="btn btn-info btn-sm">Search</button>
	                        </div>
	            		</div>
	            		
	            	</div>
	            </div>
	            <?php
	            echo form_close();
	            ?>
		    </div>
		</section>
	</div>
</div>
<div class="row">
		<div class="col-md-6">
			<section class="panel">
			    <header class="panel-heading">						
			        <h2 class="panel-title"><?php echo $title?> League Matches</h2>
			    </header>
			    <div class="panel-body">
			        <div class="table-responsive">
			            
			            <?php echo $league_matches_result;?>
			    
			        </div>
			    </div>
			</section>
		</div>
		<div class="col-md-6">
			<section class="panel">
			    <header class="panel-heading">						
			        <h2 class="panel-title"><?php echo $title?> Tournament Matches</h2>
			    </header>
			    <div class="panel-body">
			        <div class="table-responsive">
			            
			            <?php echo $tournament_matches_result;?>
			    
			        </div>
			    </div>
			</section>
		</div>
</div>
<div class="row">
	<div class="col-md-6">
		<section class="panel">
		    <header class="panel-heading">						
		        <h2 class="panel-title"><?php echo $title?> League Referee/Commisioners</h2>
		    </header>
		    <div class="panel-body">
		        <div class="table-responsive">
		            
		            <?php echo $referee_result;?>
		    
		        </div>
		    </div>
		</section>
	</div>
	<div class="col-md-6">
		<section class="panel">
		    <header class="panel-heading">						
		        <h2 class="panel-title"><?php echo $title?> Tournament Referee/Commisioners</h2>
		    </header>
		    <div class="panel-body">
		        <div class="table-responsive">
		            
		            <?php echo $tournament_referee_result;?>
		    
		        </div>
		    </div>
		</section>
	</div>
</div>
