<?php
$referee_result = '';
$count = 0;
if($tournament_referee_payments->num_rows() > 0)
{
	$referee_result.='
			<table class="table table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th>#</th>
						<th>Referee Name</th>
						<th>Refereeing Type</th>
						<th>Fixture</th>
						<th>Played on</th>
						<th>Amount Paid</th>
						<th>Paid on</th>
					</tr>
				<thead>
				<tbody>
			';
	foreach($tournament_referee_payments->result() as $ref_payment)
	{
		
		$tournament_fixture_id = $ref_payment->tournament_fixture_id;
		$referee_id = $ref_payment->referee_id;
		$amount_paid = $ref_payment->amount_paid;
		$paid_on = $ref_payment->paid_on;
		$referee_fname =$ref_payment->referee_fname;
		$referee_onames =$ref_payment->referee_onames;
		$referee_name = $referee_fname.' '.$referee_onames;
		
		$fixture_details = $this->reports_model->tournament_fixture_details($tournament_fixture_id);
		$referee_type_name = $this->reports_model->referee_type($referee_id, $tournament_fixture_id);
		if($fixture_details->num_rows() > 0)
		{
			foreach($fixture_details->result() as $details)
			{
				$fixture_date = $details->tournament_fixture_date;
				$count++;
				$home_team = $this->tournament_model->get_home_team($tournament_fixture_id);
				$away_team = $this->tournament_model->get_away_team($tournament_fixture_id);
				
			}
		}		
		
		
		$referee_result.='
						<tr>
							<td>'.$count.'</td>
							<td>'.$referee_name.'</td>
							<td>'.$referee_type_name.'</td>
							<td>'.$home_team.' VS '.$away_team.'</td>
							<td>'.$fixture_date.'</td>
							<td>'.$amount_paid.'</td>
							<td>'.$paid_on.'</td>
						</tr>';
		
		
	}
	$referee_result .='</tbody>
				</table>';
}
else
{
	$referee_result .= 'No referees were paid';
}
?>
<section class="panel">
    <header class="panel-heading">						
        <h2 class="panel-title">Referee Tournament Payments</h2>
    </header>
    <div class="panel-body">
        <div class="table-responsive">
            
            <?php echo $referee_result;?>
    
        </div>
    </div>
</section>